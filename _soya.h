#ifndef __PYX_HAVE___soya
#define __PYX_HAVE___soya
#ifdef __cplusplus
#define __PYX_EXTERN_C extern "C"
#else
#define __PYX_EXTERN_C extern
#endif

#ifndef __PYX_HAVE_API___soya

__PYX_EXTERN_C DL_IMPORT(PyObject) *MAIN_LOOP;

#endif

PyMODINIT_FUNC init_soya(void);

#endif
