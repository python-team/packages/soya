/* 0.9.7.2 on Fri Aug 29 13:18:17 2008 */

#define PY_SSIZE_T_CLEAN
#include "Python.h"
#include "structmember.h"
#ifndef PY_LONG_LONG
  #define PY_LONG_LONG LONG_LONG
#endif
#if PY_VERSION_HEX < 0x02050000
  typedef int Py_ssize_t;
  #define PY_SSIZE_T_MAX INT_MAX
  #define PY_SSIZE_T_MIN INT_MIN
  #define PyInt_FromSsize_t(z) PyInt_FromLong(z)
  #define PyInt_AsSsize_t(o)	PyInt_AsLong(o)
#endif
#ifndef WIN32
  #ifndef __stdcall
    #define __stdcall
  #endif
  #ifndef __cdecl
    #define __cdecl
  #endif
#endif
#ifdef __cplusplus
#define __PYX_EXTERN_C extern "C"
#else
#define __PYX_EXTERN_C extern
#endif
#include <math.h>
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "stdint.h"
#include "include_glew.h"


typedef struct {PyObject **p; char *s;} __Pyx_InternTabEntry; /*proto*/
typedef struct {PyObject **p; char *s; long n;} __Pyx_StringTabEntry; /*proto*/

static PyObject *__pyx_m;
static PyObject *__pyx_b;
static int __pyx_lineno;
static char *__pyx_filename;
static char **__pyx_f;

static PyObject *__Pyx_GetName(PyObject *dict, PyObject *name); /*proto*/

static int __Pyx_InternStrings(__Pyx_InternTabEntry *t); /*proto*/

static void __Pyx_AddTraceback(char *funcname); /*proto*/

/* Declarations from c_opengl */

typedef void (*__pyx_t_8c_opengl__GLUfuncptr)(void);


/* Declarations from opengl */

static int __pyx_k1;


/* Implementation of opengl */

static PyObject *__pyx_n_GL_FALSE;
static PyObject *__pyx_n_GL_TRUE;
static PyObject *__pyx_n_GL_BYTE;
static PyObject *__pyx_n_GL_UNSIGNED_BYTE;
static PyObject *__pyx_n_GL_SHORT;
static PyObject *__pyx_n_GL_UNSIGNED_SHORT;
static PyObject *__pyx_n_GL_INT;
static PyObject *__pyx_n_GL_UNSIGNED_INT;
static PyObject *__pyx_n_GL_FLOAT;
static PyObject *__pyx_n_GL_DOUBLE;
static PyObject *__pyx_n_GL_2_BYTES;
static PyObject *__pyx_n_GL_3_BYTES;
static PyObject *__pyx_n_GL_4_BYTES;
static PyObject *__pyx_n_GL_POINTS;
static PyObject *__pyx_n_GL_LINES;
static PyObject *__pyx_n_GL_LINE_LOOP;
static PyObject *__pyx_n_GL_LINE_STRIP;
static PyObject *__pyx_n_GL_TRIANGLES;
static PyObject *__pyx_n_GL_TRIANGLE_STRIP;
static PyObject *__pyx_n_GL_TRIANGLE_FAN;
static PyObject *__pyx_n_GL_QUADS;
static PyObject *__pyx_n_GL_QUAD_STRIP;
static PyObject *__pyx_n_GL_POLYGON;
static PyObject *__pyx_n_GL_VERTEX_ARRAY;
static PyObject *__pyx_n_GL_NORMAL_ARRAY;
static PyObject *__pyx_n_GL_COLOR_ARRAY;
static PyObject *__pyx_n_GL_INDEX_ARRAY;
static PyObject *__pyx_n_GL_TEXTURE_COORD_ARRAY;
static PyObject *__pyx_n_GL_EDGE_FLAG_ARRAY;
static PyObject *__pyx_n_GL_VERTEX_ARRAY_SIZE;
static PyObject *__pyx_n_GL_VERTEX_ARRAY_TYPE;
static PyObject *__pyx_n_GL_VERTEX_ARRAY_STRIDE;
static PyObject *__pyx_n_GL_NORMAL_ARRAY_TYPE;
static PyObject *__pyx_n_GL_NORMAL_ARRAY_STRIDE;
static PyObject *__pyx_n_GL_COLOR_ARRAY_SIZE;
static PyObject *__pyx_n_GL_COLOR_ARRAY_TYPE;
static PyObject *__pyx_n_GL_COLOR_ARRAY_STRIDE;
static PyObject *__pyx_n_GL_INDEX_ARRAY_TYPE;
static PyObject *__pyx_n_GL_INDEX_ARRAY_STRIDE;
static PyObject *__pyx_n_GL_TEXTURE_COORD_ARRAY_SIZE;
static PyObject *__pyx_n_GL_TEXTURE_COORD_ARRAY_TYPE;
static PyObject *__pyx_n_GL_TEXTURE_COORD_ARRAY_STRIDE;
static PyObject *__pyx_n_GL_EDGE_FLAG_ARRAY_STRIDE;
static PyObject *__pyx_n_GL_VERTEX_ARRAY_POINTER;
static PyObject *__pyx_n_GL_NORMAL_ARRAY_POINTER;
static PyObject *__pyx_n_GL_COLOR_ARRAY_POINTER;
static PyObject *__pyx_n_GL_INDEX_ARRAY_POINTER;
static PyObject *__pyx_n_GL_TEXTURE_COORD_ARRAY_POINTER;
static PyObject *__pyx_n_GL_EDGE_FLAG_ARRAY_POINTER;
static PyObject *__pyx_n_GL_V2F;
static PyObject *__pyx_n_GL_V3F;
static PyObject *__pyx_n_GL_C4UB_V2F;
static PyObject *__pyx_n_GL_C4UB_V3F;
static PyObject *__pyx_n_GL_C3F_V3F;
static PyObject *__pyx_n_GL_N3F_V3F;
static PyObject *__pyx_n_GL_C4F_N3F_V3F;
static PyObject *__pyx_n_GL_T2F_V3F;
static PyObject *__pyx_n_GL_T4F_V4F;
static PyObject *__pyx_n_GL_T2F_C4UB_V3F;
static PyObject *__pyx_n_GL_T2F_C3F_V3F;
static PyObject *__pyx_n_GL_T2F_N3F_V3F;
static PyObject *__pyx_n_GL_T2F_C4F_N3F_V3F;
static PyObject *__pyx_n_GL_T4F_C4F_N3F_V4F;
static PyObject *__pyx_n_GL_MATRIX_MODE;
static PyObject *__pyx_n_GL_MODELVIEW;
static PyObject *__pyx_n_GL_PROJECTION;
static PyObject *__pyx_n_GL_TEXTURE;
static PyObject *__pyx_n_GL_POINT_SMOOTH;
static PyObject *__pyx_n_GL_POINT_SIZE;
static PyObject *__pyx_n_GL_POINT_SIZE_GRANULARITY;
static PyObject *__pyx_n_GL_POINT_SIZE_RANGE;
static PyObject *__pyx_n_GL_LINE_SMOOTH;
static PyObject *__pyx_n_GL_LINE_STIPPLE;
static PyObject *__pyx_n_GL_LINE_STIPPLE_PATTERN;
static PyObject *__pyx_n_GL_LINE_STIPPLE_REPEAT;
static PyObject *__pyx_n_GL_LINE_WIDTH;
static PyObject *__pyx_n_GL_LINE_WIDTH_GRANULARITY;
static PyObject *__pyx_n_GL_LINE_WIDTH_RANGE;
static PyObject *__pyx_n_GL_POINT;
static PyObject *__pyx_n_GL_LINE;
static PyObject *__pyx_n_GL_FILL;
static PyObject *__pyx_n_GL_CW;
static PyObject *__pyx_n_GL_CCW;
static PyObject *__pyx_n_GL_FRONT;
static PyObject *__pyx_n_GL_BACK;
static PyObject *__pyx_n_GL_POLYGON_MODE;
static PyObject *__pyx_n_GL_POLYGON_SMOOTH;
static PyObject *__pyx_n_GL_POLYGON_STIPPLE;
static PyObject *__pyx_n_GL_EDGE_FLAG;
static PyObject *__pyx_n_GL_CULL_FACE;
static PyObject *__pyx_n_GL_CULL_FACE_MODE;
static PyObject *__pyx_n_GL_FRONT_FACE;
static PyObject *__pyx_n_GL_POLYGON_OFFSET_FACTOR;
static PyObject *__pyx_n_GL_POLYGON_OFFSET_UNITS;
static PyObject *__pyx_n_GL_POLYGON_OFFSET_POINT;
static PyObject *__pyx_n_GL_POLYGON_OFFSET_LINE;
static PyObject *__pyx_n_GL_POLYGON_OFFSET_FILL;
static PyObject *__pyx_n_GL_COMPILE;
static PyObject *__pyx_n_GL_COMPILE_AND_EXECUTE;
static PyObject *__pyx_n_GL_LIST_BASE;
static PyObject *__pyx_n_GL_LIST_INDEX;
static PyObject *__pyx_n_GL_LIST_MODE;
static PyObject *__pyx_n_GL_NEVER;
static PyObject *__pyx_n_GL_LESS;
static PyObject *__pyx_n_GL_EQUAL;
static PyObject *__pyx_n_GL_LEQUAL;
static PyObject *__pyx_n_GL_GREATER;
static PyObject *__pyx_n_GL_NOTEQUAL;
static PyObject *__pyx_n_GL_GEQUAL;
static PyObject *__pyx_n_GL_ALWAYS;
static PyObject *__pyx_n_GL_DEPTH_TEST;
static PyObject *__pyx_n_GL_DEPTH_BITS;
static PyObject *__pyx_n_GL_DEPTH_CLEAR_VALUE;
static PyObject *__pyx_n_GL_DEPTH_FUNC;
static PyObject *__pyx_n_GL_DEPTH_RANGE;
static PyObject *__pyx_n_GL_DEPTH_WRITEMASK;
static PyObject *__pyx_n_GL_DEPTH_COMPONENT;
static PyObject *__pyx_n_GL_LIGHTING;
static PyObject *__pyx_n_GL_LIGHT0;
static PyObject *__pyx_n_GL_LIGHT1;
static PyObject *__pyx_n_GL_LIGHT2;
static PyObject *__pyx_n_GL_LIGHT3;
static PyObject *__pyx_n_GL_LIGHT4;
static PyObject *__pyx_n_GL_LIGHT5;
static PyObject *__pyx_n_GL_LIGHT6;
static PyObject *__pyx_n_GL_LIGHT7;
static PyObject *__pyx_n_GL_SPOT_EXPONENT;
static PyObject *__pyx_n_GL_SPOT_CUTOFF;
static PyObject *__pyx_n_GL_CONSTANT_ATTENUATION;
static PyObject *__pyx_n_GL_LINEAR_ATTENUATION;
static PyObject *__pyx_n_GL_QUADRATIC_ATTENUATION;
static PyObject *__pyx_n_GL_AMBIENT;
static PyObject *__pyx_n_GL_DIFFUSE;
static PyObject *__pyx_n_GL_SPECULAR;
static PyObject *__pyx_n_GL_SHININESS;
static PyObject *__pyx_n_GL_EMISSION;
static PyObject *__pyx_n_GL_POSITION;
static PyObject *__pyx_n_GL_SPOT_DIRECTION;
static PyObject *__pyx_n_GL_AMBIENT_AND_DIFFUSE;
static PyObject *__pyx_n_GL_COLOR_INDEXES;
static PyObject *__pyx_n_GL_LIGHT_MODEL_TWO_SIDE;
static PyObject *__pyx_n_GL_LIGHT_MODEL_LOCAL_VIEWER;
static PyObject *__pyx_n_GL_LIGHT_MODEL_AMBIENT;
static PyObject *__pyx_n_GL_FRONT_AND_BACK;
static PyObject *__pyx_n_GL_SHADE_MODEL;
static PyObject *__pyx_n_GL_FLAT;
static PyObject *__pyx_n_GL_SMOOTH;
static PyObject *__pyx_n_GL_COLOR_MATERIAL;
static PyObject *__pyx_n_GL_COLOR_MATERIAL_FACE;
static PyObject *__pyx_n_GL_COLOR_MATERIAL_PARAMETER;
static PyObject *__pyx_n_GL_NORMALIZE;
static PyObject *__pyx_n_GL_CLIP_PLANE0;
static PyObject *__pyx_n_GL_CLIP_PLANE1;
static PyObject *__pyx_n_GL_CLIP_PLANE2;
static PyObject *__pyx_n_GL_CLIP_PLANE3;
static PyObject *__pyx_n_GL_CLIP_PLANE4;
static PyObject *__pyx_n_GL_CLIP_PLANE5;
static PyObject *__pyx_n_GL_ACCUM_RED_BITS;
static PyObject *__pyx_n_GL_ACCUM_GREEN_BITS;
static PyObject *__pyx_n_GL_ACCUM_BLUE_BITS;
static PyObject *__pyx_n_GL_ACCUM_ALPHA_BITS;
static PyObject *__pyx_n_GL_ACCUM_CLEAR_VALUE;
static PyObject *__pyx_n_GL_ACCUM;
static PyObject *__pyx_n_GL_ADD;
static PyObject *__pyx_n_GL_LOAD;
static PyObject *__pyx_n_GL_MULT;
static PyObject *__pyx_n_GL_RETURN;
static PyObject *__pyx_n_GL_ALPHA_TEST;
static PyObject *__pyx_n_GL_ALPHA_TEST_REF;
static PyObject *__pyx_n_GL_ALPHA_TEST_FUNC;
static PyObject *__pyx_n_GL_BLEND;
static PyObject *__pyx_n_GL_BLEND_SRC;
static PyObject *__pyx_n_GL_BLEND_DST;
static PyObject *__pyx_n_GL_ZERO;
static PyObject *__pyx_n_GL_ONE;
static PyObject *__pyx_n_GL_SRC_COLOR;
static PyObject *__pyx_n_GL_ONE_MINUS_SRC_COLOR;
static PyObject *__pyx_n_GL_SRC_ALPHA;
static PyObject *__pyx_n_GL_ONE_MINUS_SRC_ALPHA;
static PyObject *__pyx_n_GL_DST_ALPHA;
static PyObject *__pyx_n_GL_ONE_MINUS_DST_ALPHA;
static PyObject *__pyx_n_GL_DST_COLOR;
static PyObject *__pyx_n_GL_ONE_MINUS_DST_COLOR;
static PyObject *__pyx_n_GL_SRC_ALPHA_SATURATE;
static PyObject *__pyx_n_GL_CONSTANT_COLOR;
static PyObject *__pyx_n_GL_ONE_MINUS_CONSTANT_COLOR;
static PyObject *__pyx_n_GL_CONSTANT_ALPHA;
static PyObject *__pyx_n_GL_ONE_MINUS_CONSTANT_ALPHA;
static PyObject *__pyx_n_GL_FEEDBACK;
static PyObject *__pyx_n_GL_RENDER;
static PyObject *__pyx_n_GL_SELECT;
static PyObject *__pyx_n_GL_2D;
static PyObject *__pyx_n_GL_3D;
static PyObject *__pyx_n_GL_3D_COLOR;
static PyObject *__pyx_n_GL_3D_COLOR_TEXTURE;
static PyObject *__pyx_n_GL_4D_COLOR_TEXTURE;
static PyObject *__pyx_n_GL_POINT_TOKEN;
static PyObject *__pyx_n_GL_LINE_TOKEN;
static PyObject *__pyx_n_GL_LINE_RESET_TOKEN;
static PyObject *__pyx_n_GL_POLYGON_TOKEN;
static PyObject *__pyx_n_GL_BITMAP_TOKEN;
static PyObject *__pyx_n_GL_DRAW_PIXEL_TOKEN;
static PyObject *__pyx_n_GL_COPY_PIXEL_TOKEN;
static PyObject *__pyx_n_GL_PASS_THROUGH_TOKEN;
static PyObject *__pyx_n_GL_FEEDBACK_BUFFER_POINTER;
static PyObject *__pyx_n_GL_FEEDBACK_BUFFER_SIZE;
static PyObject *__pyx_n_GL_FEEDBACK_BUFFER_TYPE;
static PyObject *__pyx_n_GL_SELECTION_BUFFER_POINTER;
static PyObject *__pyx_n_GL_SELECTION_BUFFER_SIZE;
static PyObject *__pyx_n_GL_FOG;
static PyObject *__pyx_n_GL_FOG_MODE;
static PyObject *__pyx_n_GL_FOG_DENSITY;
static PyObject *__pyx_n_GL_FOG_COLOR;
static PyObject *__pyx_n_GL_FOG_INDEX;
static PyObject *__pyx_n_GL_FOG_START;
static PyObject *__pyx_n_GL_FOG_END;
static PyObject *__pyx_n_GL_LINEAR;
static PyObject *__pyx_n_GL_EXP;
static PyObject *__pyx_n_GL_EXP2;
static PyObject *__pyx_n_GL_LOGIC_OP;
static PyObject *__pyx_n_GL_INDEX_LOGIC_OP;
static PyObject *__pyx_n_GL_COLOR_LOGIC_OP;
static PyObject *__pyx_n_GL_LOGIC_OP_MODE;
static PyObject *__pyx_n_GL_CLEAR;
static PyObject *__pyx_n_GL_SET;
static PyObject *__pyx_n_GL_COPY;
static PyObject *__pyx_n_GL_COPY_INVERTED;
static PyObject *__pyx_n_GL_NOOP;
static PyObject *__pyx_n_GL_INVERT;
static PyObject *__pyx_n_GL_AND;
static PyObject *__pyx_n_GL_NAND;
static PyObject *__pyx_n_GL_OR;
static PyObject *__pyx_n_GL_NOR;
static PyObject *__pyx_n_GL_XOR;
static PyObject *__pyx_n_GL_EQUIV;
static PyObject *__pyx_n_GL_AND_REVERSE;
static PyObject *__pyx_n_GL_AND_INVERTED;
static PyObject *__pyx_n_GL_OR_REVERSE;
static PyObject *__pyx_n_GL_OR_INVERTED;
static PyObject *__pyx_n_GL_STENCIL_TEST;
static PyObject *__pyx_n_GL_STENCIL_WRITEMASK;
static PyObject *__pyx_n_GL_STENCIL_BITS;
static PyObject *__pyx_n_GL_STENCIL_FUNC;
static PyObject *__pyx_n_GL_STENCIL_VALUE_MASK;
static PyObject *__pyx_n_GL_STENCIL_REF;
static PyObject *__pyx_n_GL_STENCIL_FAIL;
static PyObject *__pyx_n_GL_STENCIL_PASS_DEPTH_PASS;
static PyObject *__pyx_n_GL_STENCIL_PASS_DEPTH_FAIL;
static PyObject *__pyx_n_GL_STENCIL_CLEAR_VALUE;
static PyObject *__pyx_n_GL_STENCIL_INDEX;
static PyObject *__pyx_n_GL_KEEP;
static PyObject *__pyx_n_GL_REPLACE;
static PyObject *__pyx_n_GL_INCR;
static PyObject *__pyx_n_GL_DECR;
static PyObject *__pyx_n_GL_NONE;
static PyObject *__pyx_n_GL_LEFT;
static PyObject *__pyx_n_GL_RIGHT;
static PyObject *__pyx_n_GL_FRONT_LEFT;
static PyObject *__pyx_n_GL_FRONT_RIGHT;
static PyObject *__pyx_n_GL_BACK_LEFT;
static PyObject *__pyx_n_GL_BACK_RIGHT;
static PyObject *__pyx_n_GL_AUX0;
static PyObject *__pyx_n_GL_AUX1;
static PyObject *__pyx_n_GL_AUX2;
static PyObject *__pyx_n_GL_AUX3;
static PyObject *__pyx_n_GL_COLOR_INDEX;
static PyObject *__pyx_n_GL_RED;
static PyObject *__pyx_n_GL_GREEN;
static PyObject *__pyx_n_GL_BLUE;
static PyObject *__pyx_n_GL_ALPHA;
static PyObject *__pyx_n_GL_LUMINANCE;
static PyObject *__pyx_n_GL_LUMINANCE_ALPHA;
static PyObject *__pyx_n_GL_ALPHA_BITS;
static PyObject *__pyx_n_GL_RED_BITS;
static PyObject *__pyx_n_GL_GREEN_BITS;
static PyObject *__pyx_n_GL_BLUE_BITS;
static PyObject *__pyx_n_GL_INDEX_BITS;
static PyObject *__pyx_n_GL_SUBPIXEL_BITS;
static PyObject *__pyx_n_GL_AUX_BUFFERS;
static PyObject *__pyx_n_GL_READ_BUFFER;
static PyObject *__pyx_n_GL_DRAW_BUFFER;
static PyObject *__pyx_n_GL_DOUBLEBUFFER;
static PyObject *__pyx_n_GL_STEREO;
static PyObject *__pyx_n_GL_BITMAP;
static PyObject *__pyx_n_GL_COLOR;
static PyObject *__pyx_n_GL_DEPTH;
static PyObject *__pyx_n_GL_STENCIL;
static PyObject *__pyx_n_GL_DITHER;
static PyObject *__pyx_n_GL_RGB;
static PyObject *__pyx_n_GL_RGBA;
static PyObject *__pyx_n_GL_MAX_LIST_NESTING;
static PyObject *__pyx_n_GL_MAX_ATTRIB_STACK_DEPTH;
static PyObject *__pyx_n_GL_MAX_MODELVIEW_STACK_DEPTH;
static PyObject *__pyx_n_GL_MAX_NAME_STACK_DEPTH;
static PyObject *__pyx_n_GL_MAX_PROJECTION_STACK_DEPTH;
static PyObject *__pyx_n_GL_MAX_TEXTURE_STACK_DEPTH;
static PyObject *__pyx_n_GL_MAX_EVAL_ORDER;
static PyObject *__pyx_n_GL_MAX_LIGHTS;
static PyObject *__pyx_n_GL_MAX_CLIP_PLANES;
static PyObject *__pyx_n_GL_MAX_TEXTURE_SIZE;
static PyObject *__pyx_n_GL_MAX_PIXEL_MAP_TABLE;
static PyObject *__pyx_n_GL_MAX_VIEWPORT_DIMS;
static PyObject *__pyx_n_GL_MAX_CLIENT_ATTRIB_STACK_DEPTH;
static PyObject *__pyx_n_GL_ATTRIB_STACK_DEPTH;
static PyObject *__pyx_n_GL_CLIENT_ATTRIB_STACK_DEPTH;
static PyObject *__pyx_n_GL_COLOR_CLEAR_VALUE;
static PyObject *__pyx_n_GL_COLOR_WRITEMASK;
static PyObject *__pyx_n_GL_CURRENT_INDEX;
static PyObject *__pyx_n_GL_CURRENT_COLOR;
static PyObject *__pyx_n_GL_CURRENT_NORMAL;
static PyObject *__pyx_n_GL_CURRENT_RASTER_COLOR;
static PyObject *__pyx_n_GL_CURRENT_RASTER_DISTANCE;
static PyObject *__pyx_n_GL_CURRENT_RASTER_INDEX;
static PyObject *__pyx_n_GL_CURRENT_RASTER_POSITION;
static PyObject *__pyx_n_GL_CURRENT_RASTER_TEXTURE_COORDS;
static PyObject *__pyx_n_GL_CURRENT_RASTER_POSITION_VALID;
static PyObject *__pyx_n_GL_CURRENT_TEXTURE_COORDS;
static PyObject *__pyx_n_GL_INDEX_CLEAR_VALUE;
static PyObject *__pyx_n_GL_INDEX_MODE;
static PyObject *__pyx_n_GL_INDEX_WRITEMASK;
static PyObject *__pyx_n_GL_MODELVIEW_MATRIX;
static PyObject *__pyx_n_GL_MODELVIEW_STACK_DEPTH;
static PyObject *__pyx_n_GL_NAME_STACK_DEPTH;
static PyObject *__pyx_n_GL_PROJECTION_MATRIX;
static PyObject *__pyx_n_GL_PROJECTION_STACK_DEPTH;
static PyObject *__pyx_n_GL_RENDER_MODE;
static PyObject *__pyx_n_GL_RGBA_MODE;
static PyObject *__pyx_n_GL_TEXTURE_MATRIX;
static PyObject *__pyx_n_GL_TEXTURE_STACK_DEPTH;
static PyObject *__pyx_n_GL_VIEWPORT;
static PyObject *__pyx_n_GL_AUTO_NORMAL;
static PyObject *__pyx_n_GL_MAP1_COLOR_4;
static PyObject *__pyx_n_GL_MAP1_GRID_DOMAIN;
static PyObject *__pyx_n_GL_MAP1_GRID_SEGMENTS;
static PyObject *__pyx_n_GL_MAP1_INDEX;
static PyObject *__pyx_n_GL_MAP1_NORMAL;
static PyObject *__pyx_n_GL_MAP1_TEXTURE_COORD_1;
static PyObject *__pyx_n_GL_MAP1_TEXTURE_COORD_2;
static PyObject *__pyx_n_GL_MAP1_TEXTURE_COORD_3;
static PyObject *__pyx_n_GL_MAP1_TEXTURE_COORD_4;
static PyObject *__pyx_n_GL_MAP1_VERTEX_3;
static PyObject *__pyx_n_GL_MAP1_VERTEX_4;
static PyObject *__pyx_n_GL_MAP2_COLOR_4;
static PyObject *__pyx_n_GL_MAP2_GRID_DOMAIN;
static PyObject *__pyx_n_GL_MAP2_GRID_SEGMENTS;
static PyObject *__pyx_n_GL_MAP2_INDEX;
static PyObject *__pyx_n_GL_MAP2_NORMAL;
static PyObject *__pyx_n_GL_MAP2_TEXTURE_COORD_1;
static PyObject *__pyx_n_GL_MAP2_TEXTURE_COORD_2;
static PyObject *__pyx_n_GL_MAP2_TEXTURE_COORD_3;
static PyObject *__pyx_n_GL_MAP2_TEXTURE_COORD_4;
static PyObject *__pyx_n_GL_MAP2_VERTEX_3;
static PyObject *__pyx_n_GL_MAP2_VERTEX_4;
static PyObject *__pyx_n_GL_COEFF;
static PyObject *__pyx_n_GL_DOMAIN;
static PyObject *__pyx_n_GL_ORDER;
static PyObject *__pyx_n_GL_FOG_HINT;
static PyObject *__pyx_n_GL_LINE_SMOOTH_HINT;
static PyObject *__pyx_n_GL_PERSPECTIVE_CORRECTION_HINT;
static PyObject *__pyx_n_GL_POINT_SMOOTH_HINT;
static PyObject *__pyx_n_GL_POLYGON_SMOOTH_HINT;
static PyObject *__pyx_n_GL_DONT_CARE;
static PyObject *__pyx_n_GL_FASTEST;
static PyObject *__pyx_n_GL_NICEST;
static PyObject *__pyx_n_GL_SCISSOR_TEST;
static PyObject *__pyx_n_GL_SCISSOR_BOX;
static PyObject *__pyx_n_GL_MAP_COLOR;
static PyObject *__pyx_n_GL_MAP_STENCIL;
static PyObject *__pyx_n_GL_INDEX_SHIFT;
static PyObject *__pyx_n_GL_INDEX_OFFSET;
static PyObject *__pyx_n_GL_RED_SCALE;
static PyObject *__pyx_n_GL_RED_BIAS;
static PyObject *__pyx_n_GL_GREEN_SCALE;
static PyObject *__pyx_n_GL_GREEN_BIAS;
static PyObject *__pyx_n_GL_BLUE_SCALE;
static PyObject *__pyx_n_GL_BLUE_BIAS;
static PyObject *__pyx_n_GL_ALPHA_SCALE;
static PyObject *__pyx_n_GL_ALPHA_BIAS;
static PyObject *__pyx_n_GL_DEPTH_SCALE;
static PyObject *__pyx_n_GL_DEPTH_BIAS;
static PyObject *__pyx_n_GL_PIXEL_MAP_S_TO_S_SIZE;
static PyObject *__pyx_n_GL_PIXEL_MAP_I_TO_I_SIZE;
static PyObject *__pyx_n_GL_PIXEL_MAP_I_TO_R_SIZE;
static PyObject *__pyx_n_GL_PIXEL_MAP_I_TO_G_SIZE;
static PyObject *__pyx_n_GL_PIXEL_MAP_I_TO_B_SIZE;
static PyObject *__pyx_n_GL_PIXEL_MAP_I_TO_A_SIZE;
static PyObject *__pyx_n_GL_PIXEL_MAP_R_TO_R_SIZE;
static PyObject *__pyx_n_GL_PIXEL_MAP_G_TO_G_SIZE;
static PyObject *__pyx_n_GL_PIXEL_MAP_B_TO_B_SIZE;
static PyObject *__pyx_n_GL_PIXEL_MAP_A_TO_A_SIZE;
static PyObject *__pyx_n_GL_PIXEL_MAP_S_TO_S;
static PyObject *__pyx_n_GL_PIXEL_MAP_I_TO_I;
static PyObject *__pyx_n_GL_PIXEL_MAP_I_TO_R;
static PyObject *__pyx_n_GL_PIXEL_MAP_I_TO_G;
static PyObject *__pyx_n_GL_PIXEL_MAP_I_TO_B;
static PyObject *__pyx_n_GL_PIXEL_MAP_I_TO_A;
static PyObject *__pyx_n_GL_PIXEL_MAP_R_TO_R;
static PyObject *__pyx_n_GL_PIXEL_MAP_G_TO_G;
static PyObject *__pyx_n_GL_PIXEL_MAP_B_TO_B;
static PyObject *__pyx_n_GL_PIXEL_MAP_A_TO_A;
static PyObject *__pyx_n_GL_PACK_ALIGNMENT;
static PyObject *__pyx_n_GL_PACK_LSB_FIRST;
static PyObject *__pyx_n_GL_PACK_ROW_LENGTH;
static PyObject *__pyx_n_GL_PACK_SKIP_PIXELS;
static PyObject *__pyx_n_GL_PACK_SKIP_ROWS;
static PyObject *__pyx_n_GL_PACK_SWAP_BYTES;
static PyObject *__pyx_n_GL_UNPACK_ALIGNMENT;
static PyObject *__pyx_n_GL_UNPACK_LSB_FIRST;
static PyObject *__pyx_n_GL_UNPACK_ROW_LENGTH;
static PyObject *__pyx_n_GL_UNPACK_SKIP_PIXELS;
static PyObject *__pyx_n_GL_UNPACK_SKIP_ROWS;
static PyObject *__pyx_n_GL_UNPACK_SWAP_BYTES;
static PyObject *__pyx_n_GL_ZOOM_X;
static PyObject *__pyx_n_GL_ZOOM_Y;
static PyObject *__pyx_n_GL_TEXTURE_ENV;
static PyObject *__pyx_n_GL_TEXTURE_ENV_MODE;
static PyObject *__pyx_n_GL_TEXTURE_1D;
static PyObject *__pyx_n_GL_TEXTURE_2D;
static PyObject *__pyx_n_GL_TEXTURE_WRAP_S;
static PyObject *__pyx_n_GL_TEXTURE_WRAP_T;
static PyObject *__pyx_n_GL_TEXTURE_MAG_FILTER;
static PyObject *__pyx_n_GL_TEXTURE_MIN_FILTER;
static PyObject *__pyx_n_GL_TEXTURE_ENV_COLOR;
static PyObject *__pyx_n_GL_TEXTURE_GEN_S;
static PyObject *__pyx_n_GL_TEXTURE_GEN_T;
static PyObject *__pyx_n_GL_TEXTURE_GEN_MODE;
static PyObject *__pyx_n_GL_TEXTURE_BORDER_COLOR;
static PyObject *__pyx_n_GL_TEXTURE_WIDTH;
static PyObject *__pyx_n_GL_TEXTURE_HEIGHT;
static PyObject *__pyx_n_GL_TEXTURE_BORDER;
static PyObject *__pyx_n_GL_TEXTURE_COMPONENTS;
static PyObject *__pyx_n_GL_TEXTURE_RED_SIZE;
static PyObject *__pyx_n_GL_TEXTURE_GREEN_SIZE;
static PyObject *__pyx_n_GL_TEXTURE_BLUE_SIZE;
static PyObject *__pyx_n_GL_TEXTURE_ALPHA_SIZE;
static PyObject *__pyx_n_GL_TEXTURE_LUMINANCE_SIZE;
static PyObject *__pyx_n_GL_TEXTURE_INTENSITY_SIZE;
static PyObject *__pyx_n_GL_NEAREST_MIPMAP_NEAREST;
static PyObject *__pyx_n_GL_NEAREST_MIPMAP_LINEAR;
static PyObject *__pyx_n_GL_LINEAR_MIPMAP_NEAREST;
static PyObject *__pyx_n_GL_LINEAR_MIPMAP_LINEAR;
static PyObject *__pyx_n_GL_OBJECT_LINEAR;
static PyObject *__pyx_n_GL_OBJECT_PLANE;
static PyObject *__pyx_n_GL_EYE_LINEAR;
static PyObject *__pyx_n_GL_EYE_PLANE;
static PyObject *__pyx_n_GL_SPHERE_MAP;
static PyObject *__pyx_n_GL_DECAL;
static PyObject *__pyx_n_GL_MODULATE;
static PyObject *__pyx_n_GL_NEAREST;
static PyObject *__pyx_n_GL_REPEAT;
static PyObject *__pyx_n_GL_CLAMP;
static PyObject *__pyx_n_GL_S;
static PyObject *__pyx_n_GL_T;
static PyObject *__pyx_n_GL_R;
static PyObject *__pyx_n_GL_Q;
static PyObject *__pyx_n_GL_TEXTURE_GEN_R;
static PyObject *__pyx_n_GL_TEXTURE_GEN_Q;
static PyObject *__pyx_n_GL_VENDOR;
static PyObject *__pyx_n_GL_RENDERER;
static PyObject *__pyx_n_GL_VERSION;
static PyObject *__pyx_n_GL_EXTENSIONS;
static PyObject *__pyx_n_GL_NO_ERROR;
static PyObject *__pyx_n_GL_INVALID_VALUE;
static PyObject *__pyx_n_GL_INVALID_ENUM;
static PyObject *__pyx_n_GL_INVALID_OPERATION;
static PyObject *__pyx_n_GL_STACK_OVERFLOW;
static PyObject *__pyx_n_GL_STACK_UNDERFLOW;
static PyObject *__pyx_n_GL_OUT_OF_MEMORY;
static PyObject *__pyx_n_GL_CURRENT_BIT;
static PyObject *__pyx_n_GL_POINT_BIT;
static PyObject *__pyx_n_GL_LINE_BIT;
static PyObject *__pyx_n_GL_POLYGON_BIT;
static PyObject *__pyx_n_GL_POLYGON_STIPPLE_BIT;
static PyObject *__pyx_n_GL_PIXEL_MODE_BIT;
static PyObject *__pyx_n_GL_LIGHTING_BIT;
static PyObject *__pyx_n_GL_FOG_BIT;
static PyObject *__pyx_n_GL_DEPTH_BUFFER_BIT;
static PyObject *__pyx_n_GL_ACCUM_BUFFER_BIT;
static PyObject *__pyx_n_GL_STENCIL_BUFFER_BIT;
static PyObject *__pyx_n_GL_VIEWPORT_BIT;
static PyObject *__pyx_n_GL_TRANSFORM_BIT;
static PyObject *__pyx_n_GL_ENABLE_BIT;
static PyObject *__pyx_n_GL_COLOR_BUFFER_BIT;
static PyObject *__pyx_n_GL_HINT_BIT;
static PyObject *__pyx_n_GL_EVAL_BIT;
static PyObject *__pyx_n_GL_LIST_BIT;
static PyObject *__pyx_n_GL_TEXTURE_BIT;
static PyObject *__pyx_n_GL_SCISSOR_BIT;
static PyObject *__pyx_n_GL_ALL_ATTRIB_BITS;
static PyObject *__pyx_n_GL_PROXY_TEXTURE_1D;
static PyObject *__pyx_n_GL_PROXY_TEXTURE_2D;
static PyObject *__pyx_n_GL_TEXTURE_PRIORITY;
static PyObject *__pyx_n_GL_TEXTURE_RESIDENT;
static PyObject *__pyx_n_GL_TEXTURE_BINDING_1D;
static PyObject *__pyx_n_GL_TEXTURE_BINDING_2D;
static PyObject *__pyx_n_GL_TEXTURE_INTERNAL_FORMAT;
static PyObject *__pyx_n_GL_ALPHA4;
static PyObject *__pyx_n_GL_ALPHA8;
static PyObject *__pyx_n_GL_ALPHA12;
static PyObject *__pyx_n_GL_ALPHA16;
static PyObject *__pyx_n_GL_LUMINANCE4;
static PyObject *__pyx_n_GL_LUMINANCE8;
static PyObject *__pyx_n_GL_LUMINANCE12;
static PyObject *__pyx_n_GL_LUMINANCE16;
static PyObject *__pyx_n_GL_LUMINANCE4_ALPHA4;
static PyObject *__pyx_n_GL_LUMINANCE6_ALPHA2;
static PyObject *__pyx_n_GL_LUMINANCE8_ALPHA8;
static PyObject *__pyx_n_GL_LUMINANCE12_ALPHA4;
static PyObject *__pyx_n_GL_LUMINANCE12_ALPHA12;
static PyObject *__pyx_n_GL_LUMINANCE16_ALPHA16;
static PyObject *__pyx_n_GL_INTENSITY;
static PyObject *__pyx_n_GL_INTENSITY4;
static PyObject *__pyx_n_GL_INTENSITY8;
static PyObject *__pyx_n_GL_INTENSITY12;
static PyObject *__pyx_n_GL_INTENSITY16;
static PyObject *__pyx_n_GL_R3_G3_B2;
static PyObject *__pyx_n_GL_RGB4;
static PyObject *__pyx_n_GL_RGB5;
static PyObject *__pyx_n_GL_RGB8;
static PyObject *__pyx_n_GL_RGB10;
static PyObject *__pyx_n_GL_RGB12;
static PyObject *__pyx_n_GL_RGB16;
static PyObject *__pyx_n_GL_RGBA2;
static PyObject *__pyx_n_GL_RGBA4;
static PyObject *__pyx_n_GL_RGB5_A1;
static PyObject *__pyx_n_GL_RGBA8;
static PyObject *__pyx_n_GL_RGB10_A2;
static PyObject *__pyx_n_GL_RGBA12;
static PyObject *__pyx_n_GL_RGBA16;
static PyObject *__pyx_n_GL_CLIENT_PIXEL_STORE_BIT;
static PyObject *__pyx_n_GL_CLIENT_VERTEX_ARRAY_BIT;
static PyObject *__pyx_n_GL_CLIENT_ALL_ATTRIB_BITS;
static PyObject *__pyx_n_GL_RESCALE_NORMAL;
static PyObject *__pyx_n_GL_CLAMP_TO_EDGE;
static PyObject *__pyx_n_GL_MAX_ELEMENTS_VERTICES;
static PyObject *__pyx_n_GL_MAX_ELEMENTS_INDICES;
static PyObject *__pyx_n_GL_BGR;
static PyObject *__pyx_n_GL_BGRA;
static PyObject *__pyx_n_GL_UNSIGNED_BYTE_3_3_2;
static PyObject *__pyx_n_GL_UNSIGNED_BYTE_2_3_3_REV;
static PyObject *__pyx_n_GL_UNSIGNED_SHORT_5_6_5;
static PyObject *__pyx_n_GL_UNSIGNED_SHORT_5_6_5_REV;
static PyObject *__pyx_n_GL_UNSIGNED_SHORT_4_4_4_4;
static PyObject *__pyx_n_GL_UNSIGNED_SHORT_4_4_4_4_REV;
static PyObject *__pyx_n_GL_UNSIGNED_SHORT_5_5_5_1;
static PyObject *__pyx_n_GL_UNSIGNED_SHORT_1_5_5_5_REV;
static PyObject *__pyx_n_GL_UNSIGNED_INT_8_8_8_8;
static PyObject *__pyx_n_GL_UNSIGNED_INT_8_8_8_8_REV;
static PyObject *__pyx_n_GL_UNSIGNED_INT_10_10_10_2;
static PyObject *__pyx_n_GL_UNSIGNED_INT_2_10_10_10_REV;
static PyObject *__pyx_n_GL_LIGHT_MODEL_COLOR_CONTROL;
static PyObject *__pyx_n_GL_SINGLE_COLOR;
static PyObject *__pyx_n_GL_SEPARATE_SPECULAR_COLOR;
static PyObject *__pyx_n_GL_TEXTURE_MIN_LOD;
static PyObject *__pyx_n_GL_TEXTURE_MAX_LOD;
static PyObject *__pyx_n_GL_TEXTURE_BASE_LEVEL;
static PyObject *__pyx_n_GL_TEXTURE_MAX_LEVEL;
static PyObject *__pyx_n_GL_SMOOTH_POINT_SIZE_RANGE;
static PyObject *__pyx_n_GL_SMOOTH_POINT_SIZE_GRANULARITY;
static PyObject *__pyx_n_GL_SMOOTH_LINE_WIDTH_RANGE;
static PyObject *__pyx_n_GL_SMOOTH_LINE_WIDTH_GRANULARITY;
static PyObject *__pyx_n_GL_ALIASED_POINT_SIZE_RANGE;
static PyObject *__pyx_n_GL_ALIASED_LINE_WIDTH_RANGE;
static PyObject *__pyx_n_GL_PACK_SKIP_IMAGES;
static PyObject *__pyx_n_GL_PACK_IMAGE_HEIGHT;
static PyObject *__pyx_n_GL_UNPACK_SKIP_IMAGES;
static PyObject *__pyx_n_GL_UNPACK_IMAGE_HEIGHT;
static PyObject *__pyx_n_GL_TEXTURE_3D;
static PyObject *__pyx_n_GL_PROXY_TEXTURE_3D;
static PyObject *__pyx_n_GL_TEXTURE_DEPTH;
static PyObject *__pyx_n_GL_TEXTURE_WRAP_R;
static PyObject *__pyx_n_GL_MAX_3D_TEXTURE_SIZE;
static PyObject *__pyx_n_GL_TEXTURE_BINDING_3D;
static PyObject *__pyx_n_GL_COLOR_TABLE;
static PyObject *__pyx_n_GL_POST_CONVOLUTION_COLOR_TABLE;
static PyObject *__pyx_n_GL_POST_COLOR_MATRIX_COLOR_TABLE;
static PyObject *__pyx_n_GL_PROXY_COLOR_TABLE;
static PyObject *__pyx_n_GL_PROXY_POST_CONVOLUTION_COLOR_TABLE;
static PyObject *__pyx_n_GL_PROXY_POST_COLOR_MATRIX_COLOR_TABLE;
static PyObject *__pyx_n_GL_COLOR_TABLE_SCALE;
static PyObject *__pyx_n_GL_COLOR_TABLE_BIAS;
static PyObject *__pyx_n_GL_COLOR_TABLE_FORMAT;
static PyObject *__pyx_n_GL_COLOR_TABLE_WIDTH;
static PyObject *__pyx_n_GL_COLOR_TABLE_RED_SIZE;
static PyObject *__pyx_n_GL_COLOR_TABLE_GREEN_SIZE;
static PyObject *__pyx_n_GL_COLOR_TABLE_BLUE_SIZE;
static PyObject *__pyx_n_GL_COLOR_TABLE_ALPHA_SIZE;
static PyObject *__pyx_n_GL_COLOR_TABLE_LUMINANCE_SIZE;
static PyObject *__pyx_n_GL_COLOR_TABLE_INTENSITY_SIZE;
static PyObject *__pyx_n_GL_CONVOLUTION_1D;
static PyObject *__pyx_n_GL_CONVOLUTION_2D;
static PyObject *__pyx_n_GL_SEPARABLE_2D;
static PyObject *__pyx_n_GL_CONVOLUTION_BORDER_MODE;
static PyObject *__pyx_n_GL_CONVOLUTION_FILTER_SCALE;
static PyObject *__pyx_n_GL_CONVOLUTION_FILTER_BIAS;
static PyObject *__pyx_n_GL_REDUCE;
static PyObject *__pyx_n_GL_CONVOLUTION_FORMAT;
static PyObject *__pyx_n_GL_CONVOLUTION_WIDTH;
static PyObject *__pyx_n_GL_CONVOLUTION_HEIGHT;
static PyObject *__pyx_n_GL_MAX_CONVOLUTION_WIDTH;
static PyObject *__pyx_n_GL_MAX_CONVOLUTION_HEIGHT;
static PyObject *__pyx_n_GL_POST_CONVOLUTION_RED_SCALE;
static PyObject *__pyx_n_GL_POST_CONVOLUTION_GREEN_SCALE;
static PyObject *__pyx_n_GL_POST_CONVOLUTION_BLUE_SCALE;
static PyObject *__pyx_n_GL_POST_CONVOLUTION_ALPHA_SCALE;
static PyObject *__pyx_n_GL_POST_CONVOLUTION_RED_BIAS;
static PyObject *__pyx_n_GL_POST_CONVOLUTION_GREEN_BIAS;
static PyObject *__pyx_n_GL_POST_CONVOLUTION_BLUE_BIAS;
static PyObject *__pyx_n_GL_POST_CONVOLUTION_ALPHA_BIAS;
static PyObject *__pyx_n_GL_CONSTANT_BORDER;
static PyObject *__pyx_n_GL_REPLICATE_BORDER;
static PyObject *__pyx_n_GL_CONVOLUTION_BORDER_COLOR;
static PyObject *__pyx_n_GL_COLOR_MATRIX;
static PyObject *__pyx_n_GL_COLOR_MATRIX_STACK_DEPTH;
static PyObject *__pyx_n_GL_MAX_COLOR_MATRIX_STACK_DEPTH;
static PyObject *__pyx_n_GL_POST_COLOR_MATRIX_RED_SCALE;
static PyObject *__pyx_n_GL_POST_COLOR_MATRIX_GREEN_SCALE;
static PyObject *__pyx_n_GL_POST_COLOR_MATRIX_BLUE_SCALE;
static PyObject *__pyx_n_GL_POST_COLOR_MATRIX_ALPHA_SCALE;
static PyObject *__pyx_n_GL_POST_COLOR_MATRIX_RED_BIAS;
static PyObject *__pyx_n_GL_POST_COLOR_MATRIX_GREEN_BIAS;
static PyObject *__pyx_n_GL_POST_COLOR_MATRIX_BLUE_BIAS;
static PyObject *__pyx_n_GL_POST_COLOR_MATRIX_ALPHA_BIAS;
static PyObject *__pyx_n_GL_HISTOGRAM;
static PyObject *__pyx_n_GL_PROXY_HISTOGRAM;
static PyObject *__pyx_n_GL_HISTOGRAM_WIDTH;
static PyObject *__pyx_n_GL_HISTOGRAM_FORMAT;
static PyObject *__pyx_n_GL_HISTOGRAM_RED_SIZE;
static PyObject *__pyx_n_GL_HISTOGRAM_GREEN_SIZE;
static PyObject *__pyx_n_GL_HISTOGRAM_BLUE_SIZE;
static PyObject *__pyx_n_GL_HISTOGRAM_ALPHA_SIZE;
static PyObject *__pyx_n_GL_HISTOGRAM_LUMINANCE_SIZE;
static PyObject *__pyx_n_GL_HISTOGRAM_SINK;
static PyObject *__pyx_n_GL_MINMAX;
static PyObject *__pyx_n_GL_MINMAX_FORMAT;
static PyObject *__pyx_n_GL_MINMAX_SINK;
static PyObject *__pyx_n_GL_TABLE_TOO_LARGE;
static PyObject *__pyx_n_GL_BLEND_EQUATION;
static PyObject *__pyx_n_GL_MIN;
static PyObject *__pyx_n_GL_MAX;
static PyObject *__pyx_n_GL_FUNC_ADD;
static PyObject *__pyx_n_GL_FUNC_SUBTRACT;
static PyObject *__pyx_n_GL_FUNC_REVERSE_SUBTRACT;
static PyObject *__pyx_n_GL_BLEND_COLOR;
static PyObject *__pyx_n_GL_TEXTURE0;
static PyObject *__pyx_n_GL_TEXTURE1;
static PyObject *__pyx_n_GL_TEXTURE2;
static PyObject *__pyx_n_GL_TEXTURE3;
static PyObject *__pyx_n_GL_TEXTURE4;
static PyObject *__pyx_n_GL_TEXTURE5;
static PyObject *__pyx_n_GL_TEXTURE6;
static PyObject *__pyx_n_GL_TEXTURE7;
static PyObject *__pyx_n_GL_TEXTURE8;
static PyObject *__pyx_n_GL_TEXTURE9;
static PyObject *__pyx_n_GL_TEXTURE10;
static PyObject *__pyx_n_GL_TEXTURE11;
static PyObject *__pyx_n_GL_TEXTURE12;
static PyObject *__pyx_n_GL_TEXTURE13;
static PyObject *__pyx_n_GL_TEXTURE14;
static PyObject *__pyx_n_GL_TEXTURE15;
static PyObject *__pyx_n_GL_TEXTURE16;
static PyObject *__pyx_n_GL_TEXTURE17;
static PyObject *__pyx_n_GL_TEXTURE18;
static PyObject *__pyx_n_GL_TEXTURE19;
static PyObject *__pyx_n_GL_TEXTURE20;
static PyObject *__pyx_n_GL_TEXTURE21;
static PyObject *__pyx_n_GL_TEXTURE22;
static PyObject *__pyx_n_GL_TEXTURE23;
static PyObject *__pyx_n_GL_TEXTURE24;
static PyObject *__pyx_n_GL_TEXTURE25;
static PyObject *__pyx_n_GL_TEXTURE26;
static PyObject *__pyx_n_GL_TEXTURE27;
static PyObject *__pyx_n_GL_TEXTURE28;
static PyObject *__pyx_n_GL_TEXTURE29;
static PyObject *__pyx_n_GL_TEXTURE30;
static PyObject *__pyx_n_GL_TEXTURE31;
static PyObject *__pyx_n_GL_ACTIVE_TEXTURE;
static PyObject *__pyx_n_GL_CLIENT_ACTIVE_TEXTURE;
static PyObject *__pyx_n_GL_MAX_TEXTURE_UNITS;
static PyObject *__pyx_n_GL_NORMAL_MAP;
static PyObject *__pyx_n_GL_REFLECTION_MAP;
static PyObject *__pyx_n_GL_TEXTURE_CUBE_MAP;
static PyObject *__pyx_n_GL_TEXTURE_BINDING_CUBE_MAP;
static PyObject *__pyx_n_GL_TEXTURE_CUBE_MAP_POSITIVE_X;
static PyObject *__pyx_n_GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
static PyObject *__pyx_n_GL_TEXTURE_CUBE_MAP_POSITIVE_Y;
static PyObject *__pyx_n_GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
static PyObject *__pyx_n_GL_TEXTURE_CUBE_MAP_POSITIVE_Z;
static PyObject *__pyx_n_GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;
static PyObject *__pyx_n_GL_PROXY_TEXTURE_CUBE_MAP;
static PyObject *__pyx_n_GL_MAX_CUBE_MAP_TEXTURE_SIZE;
static PyObject *__pyx_n_GL_COMPRESSED_ALPHA;
static PyObject *__pyx_n_GL_COMPRESSED_LUMINANCE;
static PyObject *__pyx_n_GL_COMPRESSED_LUMINANCE_ALPHA;
static PyObject *__pyx_n_GL_COMPRESSED_INTENSITY;
static PyObject *__pyx_n_GL_COMPRESSED_RGB;
static PyObject *__pyx_n_GL_COMPRESSED_RGBA;
static PyObject *__pyx_n_GL_TEXTURE_COMPRESSION_HINT;
static PyObject *__pyx_n_GL_TEXTURE_COMPRESSED_IMAGE_SIZE;
static PyObject *__pyx_n_GL_TEXTURE_COMPRESSED;
static PyObject *__pyx_n_GL_NUM_COMPRESSED_TEXTURE_FORMATS;
static PyObject *__pyx_n_GL_COMPRESSED_TEXTURE_FORMATS;
static PyObject *__pyx_n_GL_MULTISAMPLE;
static PyObject *__pyx_n_GL_SAMPLE_ALPHA_TO_COVERAGE;
static PyObject *__pyx_n_GL_SAMPLE_ALPHA_TO_ONE;
static PyObject *__pyx_n_GL_SAMPLE_COVERAGE;
static PyObject *__pyx_n_GL_SAMPLE_BUFFERS;
static PyObject *__pyx_n_GL_SAMPLES;
static PyObject *__pyx_n_GL_SAMPLE_COVERAGE_VALUE;
static PyObject *__pyx_n_GL_SAMPLE_COVERAGE_INVERT;
static PyObject *__pyx_n_GL_MULTISAMPLE_BIT;
static PyObject *__pyx_n_GL_TRANSPOSE_MODELVIEW_MATRIX;
static PyObject *__pyx_n_GL_TRANSPOSE_PROJECTION_MATRIX;
static PyObject *__pyx_n_GL_TRANSPOSE_TEXTURE_MATRIX;
static PyObject *__pyx_n_GL_TRANSPOSE_COLOR_MATRIX;
static PyObject *__pyx_n_GL_COMBINE;
static PyObject *__pyx_n_GL_COMBINE_RGB;
static PyObject *__pyx_n_GL_COMBINE_ALPHA;
static PyObject *__pyx_n_GL_SOURCE0_RGB;
static PyObject *__pyx_n_GL_SOURCE1_RGB;
static PyObject *__pyx_n_GL_SOURCE2_RGB;
static PyObject *__pyx_n_GL_SOURCE0_ALPHA;
static PyObject *__pyx_n_GL_SOURCE1_ALPHA;
static PyObject *__pyx_n_GL_SOURCE2_ALPHA;
static PyObject *__pyx_n_GL_OPERAND0_RGB;
static PyObject *__pyx_n_GL_OPERAND1_RGB;
static PyObject *__pyx_n_GL_OPERAND2_RGB;
static PyObject *__pyx_n_GL_OPERAND0_ALPHA;
static PyObject *__pyx_n_GL_OPERAND1_ALPHA;
static PyObject *__pyx_n_GL_OPERAND2_ALPHA;
static PyObject *__pyx_n_GL_RGB_SCALE;
static PyObject *__pyx_n_GL_ADD_SIGNED;
static PyObject *__pyx_n_GL_INTERPOLATE;
static PyObject *__pyx_n_GL_SUBTRACT;
static PyObject *__pyx_n_GL_CONSTANT;
static PyObject *__pyx_n_GL_PRIMARY_COLOR;
static PyObject *__pyx_n_GL_PREVIOUS;
static PyObject *__pyx_n_GL_DOT3_RGB;
static PyObject *__pyx_n_GL_DOT3_RGBA;
static PyObject *__pyx_n_GL_CLAMP_TO_BORDER;

static PyObject *__pyx_f_6opengl_glClearColor(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glClearColor(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  float __pyx_v_arg1;
  float __pyx_v_arg2;
  float __pyx_v_arg3;
  float __pyx_v_arg4;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"arg1","arg2","arg3","arg4",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "ffff", __pyx_argnames, &__pyx_v_arg1, &__pyx_v_arg2, &__pyx_v_arg3, &__pyx_v_arg4)) return 0;
  glClearColor(__pyx_v_arg1,__pyx_v_arg2,__pyx_v_arg3,__pyx_v_arg4);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glPointSize(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glPointSize(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  float __pyx_v_arg;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"arg",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "f", __pyx_argnames, &__pyx_v_arg)) return 0;
  glPointSize(__pyx_v_arg);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glLineWidth(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glLineWidth(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  float __pyx_v_arg;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"arg",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "f", __pyx_argnames, &__pyx_v_arg)) return 0;
  glLineWidth(__pyx_v_arg);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glEnable(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glEnable(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  int __pyx_v_arg;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"arg",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "i", __pyx_argnames, &__pyx_v_arg)) return 0;
  glEnable(__pyx_v_arg);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glDisable(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glDisable(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  int __pyx_v_arg;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"arg",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "i", __pyx_argnames, &__pyx_v_arg)) return 0;
  glDisable(__pyx_v_arg);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glLightModeli(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glLightModeli(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  int __pyx_v_arg1;
  int __pyx_v_arg2;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"arg1","arg2",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "ii", __pyx_argnames, &__pyx_v_arg1, &__pyx_v_arg2)) return 0;
  glLightModeli(__pyx_v_arg1,__pyx_v_arg2);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glBegin(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glBegin(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  int __pyx_v_arg;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"arg",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "i", __pyx_argnames, &__pyx_v_arg)) return 0;
  glBegin(__pyx_v_arg);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glEnd(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glEnd(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "", __pyx_argnames)) return 0;
  glEnd();

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glVertex2i(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glVertex2i(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  int __pyx_v_x;
  int __pyx_v_y;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"x","y",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "ii", __pyx_argnames, &__pyx_v_x, &__pyx_v_y)) return 0;
  glVertex2i(__pyx_v_x,__pyx_v_y);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glVertex2f(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glVertex2f(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  float __pyx_v_x;
  float __pyx_v_y;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"x","y",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "ff", __pyx_argnames, &__pyx_v_x, &__pyx_v_y)) return 0;
  glVertex2f(__pyx_v_x,__pyx_v_y);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glVertex3f(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glVertex3f(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  float __pyx_v_x;
  float __pyx_v_y;
  float __pyx_v_z;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"x","y","z",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "fff", __pyx_argnames, &__pyx_v_x, &__pyx_v_y, &__pyx_v_z)) return 0;
  glVertex3f(__pyx_v_x,__pyx_v_y,__pyx_v_z);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glNormal3f(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glNormal3f(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  float __pyx_v_x;
  float __pyx_v_y;
  float __pyx_v_z;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"x","y","z",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "fff", __pyx_argnames, &__pyx_v_x, &__pyx_v_y, &__pyx_v_z)) return 0;
  glNormal3f(__pyx_v_x,__pyx_v_y,__pyx_v_z);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glColor4f(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glColor4f(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  float __pyx_v_r;
  float __pyx_v_g;
  float __pyx_v_b;
  float __pyx_v_a;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"r","g","b","a",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "ffff", __pyx_argnames, &__pyx_v_r, &__pyx_v_g, &__pyx_v_b, &__pyx_v_a)) return 0;
  glColor4f(__pyx_v_r,__pyx_v_g,__pyx_v_b,__pyx_v_a);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glTexCoord2f(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glTexCoord2f(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  float __pyx_v_u;
  float __pyx_v_v;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"u","v",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "ff", __pyx_argnames, &__pyx_v_u, &__pyx_v_v)) return 0;
  glTexCoord2f(__pyx_v_u,__pyx_v_v);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glDeleteList(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glDeleteList(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  int __pyx_v_id;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"id",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "i", __pyx_argnames, &__pyx_v_id)) return 0;
  glDeleteLists(__pyx_v_id,1);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glGenList(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glGenList(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  PyObject *__pyx_r;
  PyObject *__pyx_1 = 0;
  static char *__pyx_argnames[] = {0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "", __pyx_argnames)) return 0;
  __pyx_1 = PyLong_FromUnsignedLong(glGenLists(1)); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 815; goto __pyx_L1;}
  __pyx_r = __pyx_1;
  __pyx_1 = 0;
  goto __pyx_L0;

  __pyx_r = Py_None; Py_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1:;
  Py_XDECREF(__pyx_1);
  __Pyx_AddTraceback("opengl.glGenList");
  __pyx_r = 0;
  __pyx_L0:;
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glGenLists(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glGenLists(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  int __pyx_v_nb;
  PyObject *__pyx_r;
  PyObject *__pyx_1 = 0;
  static char *__pyx_argnames[] = {"nb",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "i", __pyx_argnames, &__pyx_v_nb)) return 0;
  __pyx_1 = PyLong_FromUnsignedLong(glGenLists(__pyx_v_nb)); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 818; goto __pyx_L1;}
  __pyx_r = __pyx_1;
  __pyx_1 = 0;
  goto __pyx_L0;

  __pyx_r = Py_None; Py_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1:;
  Py_XDECREF(__pyx_1);
  __Pyx_AddTraceback("opengl.glGenLists");
  __pyx_r = 0;
  __pyx_L0:;
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glNewList(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glNewList(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  int __pyx_v_list_id;
  int __pyx_v_mode;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"list_id","mode",0};
  __pyx_v_mode = __pyx_k1;
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "i|i", __pyx_argnames, &__pyx_v_list_id, &__pyx_v_mode)) return 0;
  glNewList(__pyx_v_list_id,__pyx_v_mode);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glEndList(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glEndList(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "", __pyx_argnames)) return 0;
  glEndList();

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glCallList(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glCallList(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  int __pyx_v_list_id;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"list_id",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "i", __pyx_argnames, &__pyx_v_list_id)) return 0;
  glCallList(__pyx_v_list_id);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glRotatef(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glRotatef(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  float __pyx_v_angle;
  float __pyx_v_x;
  float __pyx_v_y;
  float __pyx_v_z;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"angle","x","y","z",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "ffff", __pyx_argnames, &__pyx_v_angle, &__pyx_v_x, &__pyx_v_y, &__pyx_v_z)) return 0;
  glRotatef(__pyx_v_angle,__pyx_v_x,__pyx_v_y,__pyx_v_z);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glPushMatrix(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glPushMatrix(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "", __pyx_argnames)) return 0;
  glPushMatrix();

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glPopMatrix(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glPopMatrix(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "", __pyx_argnames)) return 0;
  glPopMatrix();

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glTranslatef(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glTranslatef(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  float __pyx_v_x;
  float __pyx_v_y;
  float __pyx_v_z;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"x","y","z",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "fff", __pyx_argnames, &__pyx_v_x, &__pyx_v_y, &__pyx_v_z)) return 0;
  glTranslatef(__pyx_v_x,__pyx_v_y,__pyx_v_z);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glTranslated(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glTranslated(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  float __pyx_v_x;
  float __pyx_v_y;
  float __pyx_v_z;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"x","y","z",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "fff", __pyx_argnames, &__pyx_v_x, &__pyx_v_y, &__pyx_v_z)) return 0;
  glTranslated(__pyx_v_x,__pyx_v_y,__pyx_v_z);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glScalef(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glScalef(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  float __pyx_v_x;
  float __pyx_v_y;
  float __pyx_v_z;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"x","y","z",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "fff", __pyx_argnames, &__pyx_v_x, &__pyx_v_y, &__pyx_v_z)) return 0;
  glScalef(__pyx_v_x,__pyx_v_y,__pyx_v_z);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glGenTextures(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glGenTextures(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  int __pyx_v_n;
  GLuint __pyx_v_ret;
  PyObject *__pyx_r;
  PyObject *__pyx_1 = 0;
  static char *__pyx_argnames[] = {"n",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "i", __pyx_argnames, &__pyx_v_n)) return 0;

  /* "/home/jiba/src/soya/opengl.pyx":849 */
  glGenTextures(__pyx_v_n,(&__pyx_v_ret));

  /* "/home/jiba/src/soya/opengl.pyx":850 */
  __pyx_1 = PyLong_FromUnsignedLong(__pyx_v_ret); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 850; goto __pyx_L1;}
  __pyx_r = __pyx_1;
  __pyx_1 = 0;
  goto __pyx_L0;

  __pyx_r = Py_None; Py_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1:;
  Py_XDECREF(__pyx_1);
  __Pyx_AddTraceback("opengl.glGenTextures");
  __pyx_r = 0;
  __pyx_L0:;
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glBindTexture(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glBindTexture(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  int __pyx_v_target;
  unsigned int __pyx_v_texture;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"target","texture",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "iI", __pyx_argnames, &__pyx_v_target, &__pyx_v_texture)) return 0;
  glBindTexture(__pyx_v_target,__pyx_v_texture);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glPixelStorei(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glPixelStorei(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  int __pyx_v_pname;
  int __pyx_v_param;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"pname","param",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "ii", __pyx_argnames, &__pyx_v_pname, &__pyx_v_param)) return 0;
  glPixelStorei(__pyx_v_pname,__pyx_v_param);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glLoadIdentity(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glLoadIdentity(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "", __pyx_argnames)) return 0;
  glLoadIdentity();

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glAccum(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glAccum(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  int __pyx_v_value;
  float __pyx_v_coef;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"value","coef",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "if", __pyx_argnames, &__pyx_v_value, &__pyx_v_coef)) return 0;
  glAccum(__pyx_v_value,__pyx_v_coef);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glMatrixMode(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glMatrixMode(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  int __pyx_v_mode;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"mode",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "i", __pyx_argnames, &__pyx_v_mode)) return 0;
  glMatrixMode(__pyx_v_mode);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_n_c_opengl;
static PyObject *__pyx_n_glMultMatrix;

static PyObject *__pyx_f_6opengl_glMultMatrix(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glMultMatrix(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  GLenum __pyx_v_mode;
  PyObject *__pyx_r;
  PyObject *__pyx_1 = 0;
  PyObject *__pyx_2 = 0;
  PyObject *__pyx_3 = 0;
  static char *__pyx_argnames[] = {"mode",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "I", __pyx_argnames, &__pyx_v_mode)) return 0;
  __pyx_1 = __Pyx_GetName(__pyx_m, __pyx_n_c_opengl); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 868; goto __pyx_L1;}
  __pyx_2 = PyObject_GetAttr(__pyx_1, __pyx_n_glMultMatrix); if (!__pyx_2) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 868; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;
  __pyx_1 = PyLong_FromUnsignedLong(__pyx_v_mode); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 868; goto __pyx_L1;}
  __pyx_3 = PyTuple_New(1); if (!__pyx_3) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 868; goto __pyx_L1;}
  PyTuple_SET_ITEM(__pyx_3, 0, __pyx_1);
  __pyx_1 = 0;
  __pyx_1 = PyObject_CallObject(__pyx_2, __pyx_3); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 868; goto __pyx_L1;}
  Py_DECREF(__pyx_2); __pyx_2 = 0;
  Py_DECREF(__pyx_3); __pyx_3 = 0;
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  __pyx_r = Py_None; Py_INCREF(Py_None);
  goto __pyx_L0;
  __pyx_L1:;
  Py_XDECREF(__pyx_1);
  Py_XDECREF(__pyx_2);
  Py_XDECREF(__pyx_3);
  __Pyx_AddTraceback("opengl.glMultMatrix");
  __pyx_r = 0;
  __pyx_L0:;
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glTexParameteri(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glTexParameteri(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  int __pyx_v_target;
  int __pyx_v_param;
  int __pyx_v_value;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"target","param","value",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "iii", __pyx_argnames, &__pyx_v_target, &__pyx_v_param, &__pyx_v_value)) return 0;
  glTexParameteri(__pyx_v_target,__pyx_v_param,__pyx_v_value);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glPolygonOffset(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glPolygonOffset(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  float __pyx_v_factor;
  float __pyx_v_units;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"factor","units",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "ff", __pyx_argnames, &__pyx_v_factor, &__pyx_v_units)) return 0;
  glPolygonOffset(__pyx_v_factor,__pyx_v_units);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glOrtho(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glOrtho(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  float __pyx_v_x1;
  float __pyx_v_x2;
  float __pyx_v_y1;
  float __pyx_v_y2;
  float __pyx_v_z1;
  float __pyx_v_z2;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"x1","x2","y1","y2","z1","z2",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "ffffff", __pyx_argnames, &__pyx_v_x1, &__pyx_v_x2, &__pyx_v_y1, &__pyx_v_y2, &__pyx_v_z1, &__pyx_v_z2)) return 0;
  glOrtho(__pyx_v_x1,__pyx_v_x2,__pyx_v_y1,__pyx_v_y2,__pyx_v_z1,__pyx_v_z2);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glPushAttrib(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glPushAttrib(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  int __pyx_v_arg;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"arg",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "i", __pyx_argnames, &__pyx_v_arg)) return 0;
  glPushAttrib(__pyx_v_arg);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glPopAttrib(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glPopAttrib(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "", __pyx_argnames)) return 0;
  glPopAttrib();

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static PyObject *__pyx_f_6opengl_glViewport(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds); /*proto*/
static PyObject *__pyx_f_6opengl_glViewport(PyObject *__pyx_self, PyObject *__pyx_args, PyObject *__pyx_kwds) {
  int __pyx_v_x;
  int __pyx_v_y;
  int __pyx_v_width;
  int __pyx_v_height;
  PyObject *__pyx_r;
  static char *__pyx_argnames[] = {"x","y","width","height",0};
  if (!PyArg_ParseTupleAndKeywords(__pyx_args, __pyx_kwds, "iiii", __pyx_argnames, &__pyx_v_x, &__pyx_v_y, &__pyx_v_width, &__pyx_v_height)) return 0;
  glViewport(__pyx_v_x,__pyx_v_y,__pyx_v_width,__pyx_v_height);

  __pyx_r = Py_None; Py_INCREF(Py_None);
  return __pyx_r;
}

static __Pyx_InternTabEntry __pyx_intern_tab[] = {
  {&__pyx_n_GL_2D, "GL_2D"},
  {&__pyx_n_GL_2_BYTES, "GL_2_BYTES"},
  {&__pyx_n_GL_3D, "GL_3D"},
  {&__pyx_n_GL_3D_COLOR, "GL_3D_COLOR"},
  {&__pyx_n_GL_3D_COLOR_TEXTURE, "GL_3D_COLOR_TEXTURE"},
  {&__pyx_n_GL_3_BYTES, "GL_3_BYTES"},
  {&__pyx_n_GL_4D_COLOR_TEXTURE, "GL_4D_COLOR_TEXTURE"},
  {&__pyx_n_GL_4_BYTES, "GL_4_BYTES"},
  {&__pyx_n_GL_ACCUM, "GL_ACCUM"},
  {&__pyx_n_GL_ACCUM_ALPHA_BITS, "GL_ACCUM_ALPHA_BITS"},
  {&__pyx_n_GL_ACCUM_BLUE_BITS, "GL_ACCUM_BLUE_BITS"},
  {&__pyx_n_GL_ACCUM_BUFFER_BIT, "GL_ACCUM_BUFFER_BIT"},
  {&__pyx_n_GL_ACCUM_CLEAR_VALUE, "GL_ACCUM_CLEAR_VALUE"},
  {&__pyx_n_GL_ACCUM_GREEN_BITS, "GL_ACCUM_GREEN_BITS"},
  {&__pyx_n_GL_ACCUM_RED_BITS, "GL_ACCUM_RED_BITS"},
  {&__pyx_n_GL_ACTIVE_TEXTURE, "GL_ACTIVE_TEXTURE"},
  {&__pyx_n_GL_ADD, "GL_ADD"},
  {&__pyx_n_GL_ADD_SIGNED, "GL_ADD_SIGNED"},
  {&__pyx_n_GL_ALIASED_LINE_WIDTH_RANGE, "GL_ALIASED_LINE_WIDTH_RANGE"},
  {&__pyx_n_GL_ALIASED_POINT_SIZE_RANGE, "GL_ALIASED_POINT_SIZE_RANGE"},
  {&__pyx_n_GL_ALL_ATTRIB_BITS, "GL_ALL_ATTRIB_BITS"},
  {&__pyx_n_GL_ALPHA, "GL_ALPHA"},
  {&__pyx_n_GL_ALPHA12, "GL_ALPHA12"},
  {&__pyx_n_GL_ALPHA16, "GL_ALPHA16"},
  {&__pyx_n_GL_ALPHA4, "GL_ALPHA4"},
  {&__pyx_n_GL_ALPHA8, "GL_ALPHA8"},
  {&__pyx_n_GL_ALPHA_BIAS, "GL_ALPHA_BIAS"},
  {&__pyx_n_GL_ALPHA_BITS, "GL_ALPHA_BITS"},
  {&__pyx_n_GL_ALPHA_SCALE, "GL_ALPHA_SCALE"},
  {&__pyx_n_GL_ALPHA_TEST, "GL_ALPHA_TEST"},
  {&__pyx_n_GL_ALPHA_TEST_FUNC, "GL_ALPHA_TEST_FUNC"},
  {&__pyx_n_GL_ALPHA_TEST_REF, "GL_ALPHA_TEST_REF"},
  {&__pyx_n_GL_ALWAYS, "GL_ALWAYS"},
  {&__pyx_n_GL_AMBIENT, "GL_AMBIENT"},
  {&__pyx_n_GL_AMBIENT_AND_DIFFUSE, "GL_AMBIENT_AND_DIFFUSE"},
  {&__pyx_n_GL_AND, "GL_AND"},
  {&__pyx_n_GL_AND_INVERTED, "GL_AND_INVERTED"},
  {&__pyx_n_GL_AND_REVERSE, "GL_AND_REVERSE"},
  {&__pyx_n_GL_ATTRIB_STACK_DEPTH, "GL_ATTRIB_STACK_DEPTH"},
  {&__pyx_n_GL_AUTO_NORMAL, "GL_AUTO_NORMAL"},
  {&__pyx_n_GL_AUX0, "GL_AUX0"},
  {&__pyx_n_GL_AUX1, "GL_AUX1"},
  {&__pyx_n_GL_AUX2, "GL_AUX2"},
  {&__pyx_n_GL_AUX3, "GL_AUX3"},
  {&__pyx_n_GL_AUX_BUFFERS, "GL_AUX_BUFFERS"},
  {&__pyx_n_GL_BACK, "GL_BACK"},
  {&__pyx_n_GL_BACK_LEFT, "GL_BACK_LEFT"},
  {&__pyx_n_GL_BACK_RIGHT, "GL_BACK_RIGHT"},
  {&__pyx_n_GL_BGR, "GL_BGR"},
  {&__pyx_n_GL_BGRA, "GL_BGRA"},
  {&__pyx_n_GL_BITMAP, "GL_BITMAP"},
  {&__pyx_n_GL_BITMAP_TOKEN, "GL_BITMAP_TOKEN"},
  {&__pyx_n_GL_BLEND, "GL_BLEND"},
  {&__pyx_n_GL_BLEND_COLOR, "GL_BLEND_COLOR"},
  {&__pyx_n_GL_BLEND_DST, "GL_BLEND_DST"},
  {&__pyx_n_GL_BLEND_EQUATION, "GL_BLEND_EQUATION"},
  {&__pyx_n_GL_BLEND_SRC, "GL_BLEND_SRC"},
  {&__pyx_n_GL_BLUE, "GL_BLUE"},
  {&__pyx_n_GL_BLUE_BIAS, "GL_BLUE_BIAS"},
  {&__pyx_n_GL_BLUE_BITS, "GL_BLUE_BITS"},
  {&__pyx_n_GL_BLUE_SCALE, "GL_BLUE_SCALE"},
  {&__pyx_n_GL_BYTE, "GL_BYTE"},
  {&__pyx_n_GL_C3F_V3F, "GL_C3F_V3F"},
  {&__pyx_n_GL_C4F_N3F_V3F, "GL_C4F_N3F_V3F"},
  {&__pyx_n_GL_C4UB_V2F, "GL_C4UB_V2F"},
  {&__pyx_n_GL_C4UB_V3F, "GL_C4UB_V3F"},
  {&__pyx_n_GL_CCW, "GL_CCW"},
  {&__pyx_n_GL_CLAMP, "GL_CLAMP"},
  {&__pyx_n_GL_CLAMP_TO_BORDER, "GL_CLAMP_TO_BORDER"},
  {&__pyx_n_GL_CLAMP_TO_EDGE, "GL_CLAMP_TO_EDGE"},
  {&__pyx_n_GL_CLEAR, "GL_CLEAR"},
  {&__pyx_n_GL_CLIENT_ACTIVE_TEXTURE, "GL_CLIENT_ACTIVE_TEXTURE"},
  {&__pyx_n_GL_CLIENT_ALL_ATTRIB_BITS, "GL_CLIENT_ALL_ATTRIB_BITS"},
  {&__pyx_n_GL_CLIENT_ATTRIB_STACK_DEPTH, "GL_CLIENT_ATTRIB_STACK_DEPTH"},
  {&__pyx_n_GL_CLIENT_PIXEL_STORE_BIT, "GL_CLIENT_PIXEL_STORE_BIT"},
  {&__pyx_n_GL_CLIENT_VERTEX_ARRAY_BIT, "GL_CLIENT_VERTEX_ARRAY_BIT"},
  {&__pyx_n_GL_CLIP_PLANE0, "GL_CLIP_PLANE0"},
  {&__pyx_n_GL_CLIP_PLANE1, "GL_CLIP_PLANE1"},
  {&__pyx_n_GL_CLIP_PLANE2, "GL_CLIP_PLANE2"},
  {&__pyx_n_GL_CLIP_PLANE3, "GL_CLIP_PLANE3"},
  {&__pyx_n_GL_CLIP_PLANE4, "GL_CLIP_PLANE4"},
  {&__pyx_n_GL_CLIP_PLANE5, "GL_CLIP_PLANE5"},
  {&__pyx_n_GL_COEFF, "GL_COEFF"},
  {&__pyx_n_GL_COLOR, "GL_COLOR"},
  {&__pyx_n_GL_COLOR_ARRAY, "GL_COLOR_ARRAY"},
  {&__pyx_n_GL_COLOR_ARRAY_POINTER, "GL_COLOR_ARRAY_POINTER"},
  {&__pyx_n_GL_COLOR_ARRAY_SIZE, "GL_COLOR_ARRAY_SIZE"},
  {&__pyx_n_GL_COLOR_ARRAY_STRIDE, "GL_COLOR_ARRAY_STRIDE"},
  {&__pyx_n_GL_COLOR_ARRAY_TYPE, "GL_COLOR_ARRAY_TYPE"},
  {&__pyx_n_GL_COLOR_BUFFER_BIT, "GL_COLOR_BUFFER_BIT"},
  {&__pyx_n_GL_COLOR_CLEAR_VALUE, "GL_COLOR_CLEAR_VALUE"},
  {&__pyx_n_GL_COLOR_INDEX, "GL_COLOR_INDEX"},
  {&__pyx_n_GL_COLOR_INDEXES, "GL_COLOR_INDEXES"},
  {&__pyx_n_GL_COLOR_LOGIC_OP, "GL_COLOR_LOGIC_OP"},
  {&__pyx_n_GL_COLOR_MATERIAL, "GL_COLOR_MATERIAL"},
  {&__pyx_n_GL_COLOR_MATERIAL_FACE, "GL_COLOR_MATERIAL_FACE"},
  {&__pyx_n_GL_COLOR_MATERIAL_PARAMETER, "GL_COLOR_MATERIAL_PARAMETER"},
  {&__pyx_n_GL_COLOR_MATRIX, "GL_COLOR_MATRIX"},
  {&__pyx_n_GL_COLOR_MATRIX_STACK_DEPTH, "GL_COLOR_MATRIX_STACK_DEPTH"},
  {&__pyx_n_GL_COLOR_TABLE, "GL_COLOR_TABLE"},
  {&__pyx_n_GL_COLOR_TABLE_ALPHA_SIZE, "GL_COLOR_TABLE_ALPHA_SIZE"},
  {&__pyx_n_GL_COLOR_TABLE_BIAS, "GL_COLOR_TABLE_BIAS"},
  {&__pyx_n_GL_COLOR_TABLE_BLUE_SIZE, "GL_COLOR_TABLE_BLUE_SIZE"},
  {&__pyx_n_GL_COLOR_TABLE_FORMAT, "GL_COLOR_TABLE_FORMAT"},
  {&__pyx_n_GL_COLOR_TABLE_GREEN_SIZE, "GL_COLOR_TABLE_GREEN_SIZE"},
  {&__pyx_n_GL_COLOR_TABLE_INTENSITY_SIZE, "GL_COLOR_TABLE_INTENSITY_SIZE"},
  {&__pyx_n_GL_COLOR_TABLE_LUMINANCE_SIZE, "GL_COLOR_TABLE_LUMINANCE_SIZE"},
  {&__pyx_n_GL_COLOR_TABLE_RED_SIZE, "GL_COLOR_TABLE_RED_SIZE"},
  {&__pyx_n_GL_COLOR_TABLE_SCALE, "GL_COLOR_TABLE_SCALE"},
  {&__pyx_n_GL_COLOR_TABLE_WIDTH, "GL_COLOR_TABLE_WIDTH"},
  {&__pyx_n_GL_COLOR_WRITEMASK, "GL_COLOR_WRITEMASK"},
  {&__pyx_n_GL_COMBINE, "GL_COMBINE"},
  {&__pyx_n_GL_COMBINE_ALPHA, "GL_COMBINE_ALPHA"},
  {&__pyx_n_GL_COMBINE_RGB, "GL_COMBINE_RGB"},
  {&__pyx_n_GL_COMPILE, "GL_COMPILE"},
  {&__pyx_n_GL_COMPILE_AND_EXECUTE, "GL_COMPILE_AND_EXECUTE"},
  {&__pyx_n_GL_COMPRESSED_ALPHA, "GL_COMPRESSED_ALPHA"},
  {&__pyx_n_GL_COMPRESSED_INTENSITY, "GL_COMPRESSED_INTENSITY"},
  {&__pyx_n_GL_COMPRESSED_LUMINANCE, "GL_COMPRESSED_LUMINANCE"},
  {&__pyx_n_GL_COMPRESSED_LUMINANCE_ALPHA, "GL_COMPRESSED_LUMINANCE_ALPHA"},
  {&__pyx_n_GL_COMPRESSED_RGB, "GL_COMPRESSED_RGB"},
  {&__pyx_n_GL_COMPRESSED_RGBA, "GL_COMPRESSED_RGBA"},
  {&__pyx_n_GL_COMPRESSED_TEXTURE_FORMATS, "GL_COMPRESSED_TEXTURE_FORMATS"},
  {&__pyx_n_GL_CONSTANT, "GL_CONSTANT"},
  {&__pyx_n_GL_CONSTANT_ALPHA, "GL_CONSTANT_ALPHA"},
  {&__pyx_n_GL_CONSTANT_ATTENUATION, "GL_CONSTANT_ATTENUATION"},
  {&__pyx_n_GL_CONSTANT_BORDER, "GL_CONSTANT_BORDER"},
  {&__pyx_n_GL_CONSTANT_COLOR, "GL_CONSTANT_COLOR"},
  {&__pyx_n_GL_CONVOLUTION_1D, "GL_CONVOLUTION_1D"},
  {&__pyx_n_GL_CONVOLUTION_2D, "GL_CONVOLUTION_2D"},
  {&__pyx_n_GL_CONVOLUTION_BORDER_COLOR, "GL_CONVOLUTION_BORDER_COLOR"},
  {&__pyx_n_GL_CONVOLUTION_BORDER_MODE, "GL_CONVOLUTION_BORDER_MODE"},
  {&__pyx_n_GL_CONVOLUTION_FILTER_BIAS, "GL_CONVOLUTION_FILTER_BIAS"},
  {&__pyx_n_GL_CONVOLUTION_FILTER_SCALE, "GL_CONVOLUTION_FILTER_SCALE"},
  {&__pyx_n_GL_CONVOLUTION_FORMAT, "GL_CONVOLUTION_FORMAT"},
  {&__pyx_n_GL_CONVOLUTION_HEIGHT, "GL_CONVOLUTION_HEIGHT"},
  {&__pyx_n_GL_CONVOLUTION_WIDTH, "GL_CONVOLUTION_WIDTH"},
  {&__pyx_n_GL_COPY, "GL_COPY"},
  {&__pyx_n_GL_COPY_INVERTED, "GL_COPY_INVERTED"},
  {&__pyx_n_GL_COPY_PIXEL_TOKEN, "GL_COPY_PIXEL_TOKEN"},
  {&__pyx_n_GL_CULL_FACE, "GL_CULL_FACE"},
  {&__pyx_n_GL_CULL_FACE_MODE, "GL_CULL_FACE_MODE"},
  {&__pyx_n_GL_CURRENT_BIT, "GL_CURRENT_BIT"},
  {&__pyx_n_GL_CURRENT_COLOR, "GL_CURRENT_COLOR"},
  {&__pyx_n_GL_CURRENT_INDEX, "GL_CURRENT_INDEX"},
  {&__pyx_n_GL_CURRENT_NORMAL, "GL_CURRENT_NORMAL"},
  {&__pyx_n_GL_CURRENT_RASTER_COLOR, "GL_CURRENT_RASTER_COLOR"},
  {&__pyx_n_GL_CURRENT_RASTER_DISTANCE, "GL_CURRENT_RASTER_DISTANCE"},
  {&__pyx_n_GL_CURRENT_RASTER_INDEX, "GL_CURRENT_RASTER_INDEX"},
  {&__pyx_n_GL_CURRENT_RASTER_POSITION, "GL_CURRENT_RASTER_POSITION"},
  {&__pyx_n_GL_CURRENT_RASTER_POSITION_VALID, "GL_CURRENT_RASTER_POSITION_VALID"},
  {&__pyx_n_GL_CURRENT_RASTER_TEXTURE_COORDS, "GL_CURRENT_RASTER_TEXTURE_COORDS"},
  {&__pyx_n_GL_CURRENT_TEXTURE_COORDS, "GL_CURRENT_TEXTURE_COORDS"},
  {&__pyx_n_GL_CW, "GL_CW"},
  {&__pyx_n_GL_DECAL, "GL_DECAL"},
  {&__pyx_n_GL_DECR, "GL_DECR"},
  {&__pyx_n_GL_DEPTH, "GL_DEPTH"},
  {&__pyx_n_GL_DEPTH_BIAS, "GL_DEPTH_BIAS"},
  {&__pyx_n_GL_DEPTH_BITS, "GL_DEPTH_BITS"},
  {&__pyx_n_GL_DEPTH_BUFFER_BIT, "GL_DEPTH_BUFFER_BIT"},
  {&__pyx_n_GL_DEPTH_CLEAR_VALUE, "GL_DEPTH_CLEAR_VALUE"},
  {&__pyx_n_GL_DEPTH_COMPONENT, "GL_DEPTH_COMPONENT"},
  {&__pyx_n_GL_DEPTH_FUNC, "GL_DEPTH_FUNC"},
  {&__pyx_n_GL_DEPTH_RANGE, "GL_DEPTH_RANGE"},
  {&__pyx_n_GL_DEPTH_SCALE, "GL_DEPTH_SCALE"},
  {&__pyx_n_GL_DEPTH_TEST, "GL_DEPTH_TEST"},
  {&__pyx_n_GL_DEPTH_WRITEMASK, "GL_DEPTH_WRITEMASK"},
  {&__pyx_n_GL_DIFFUSE, "GL_DIFFUSE"},
  {&__pyx_n_GL_DITHER, "GL_DITHER"},
  {&__pyx_n_GL_DOMAIN, "GL_DOMAIN"},
  {&__pyx_n_GL_DONT_CARE, "GL_DONT_CARE"},
  {&__pyx_n_GL_DOT3_RGB, "GL_DOT3_RGB"},
  {&__pyx_n_GL_DOT3_RGBA, "GL_DOT3_RGBA"},
  {&__pyx_n_GL_DOUBLE, "GL_DOUBLE"},
  {&__pyx_n_GL_DOUBLEBUFFER, "GL_DOUBLEBUFFER"},
  {&__pyx_n_GL_DRAW_BUFFER, "GL_DRAW_BUFFER"},
  {&__pyx_n_GL_DRAW_PIXEL_TOKEN, "GL_DRAW_PIXEL_TOKEN"},
  {&__pyx_n_GL_DST_ALPHA, "GL_DST_ALPHA"},
  {&__pyx_n_GL_DST_COLOR, "GL_DST_COLOR"},
  {&__pyx_n_GL_EDGE_FLAG, "GL_EDGE_FLAG"},
  {&__pyx_n_GL_EDGE_FLAG_ARRAY, "GL_EDGE_FLAG_ARRAY"},
  {&__pyx_n_GL_EDGE_FLAG_ARRAY_POINTER, "GL_EDGE_FLAG_ARRAY_POINTER"},
  {&__pyx_n_GL_EDGE_FLAG_ARRAY_STRIDE, "GL_EDGE_FLAG_ARRAY_STRIDE"},
  {&__pyx_n_GL_EMISSION, "GL_EMISSION"},
  {&__pyx_n_GL_ENABLE_BIT, "GL_ENABLE_BIT"},
  {&__pyx_n_GL_EQUAL, "GL_EQUAL"},
  {&__pyx_n_GL_EQUIV, "GL_EQUIV"},
  {&__pyx_n_GL_EVAL_BIT, "GL_EVAL_BIT"},
  {&__pyx_n_GL_EXP, "GL_EXP"},
  {&__pyx_n_GL_EXP2, "GL_EXP2"},
  {&__pyx_n_GL_EXTENSIONS, "GL_EXTENSIONS"},
  {&__pyx_n_GL_EYE_LINEAR, "GL_EYE_LINEAR"},
  {&__pyx_n_GL_EYE_PLANE, "GL_EYE_PLANE"},
  {&__pyx_n_GL_FALSE, "GL_FALSE"},
  {&__pyx_n_GL_FASTEST, "GL_FASTEST"},
  {&__pyx_n_GL_FEEDBACK, "GL_FEEDBACK"},
  {&__pyx_n_GL_FEEDBACK_BUFFER_POINTER, "GL_FEEDBACK_BUFFER_POINTER"},
  {&__pyx_n_GL_FEEDBACK_BUFFER_SIZE, "GL_FEEDBACK_BUFFER_SIZE"},
  {&__pyx_n_GL_FEEDBACK_BUFFER_TYPE, "GL_FEEDBACK_BUFFER_TYPE"},
  {&__pyx_n_GL_FILL, "GL_FILL"},
  {&__pyx_n_GL_FLAT, "GL_FLAT"},
  {&__pyx_n_GL_FLOAT, "GL_FLOAT"},
  {&__pyx_n_GL_FOG, "GL_FOG"},
  {&__pyx_n_GL_FOG_BIT, "GL_FOG_BIT"},
  {&__pyx_n_GL_FOG_COLOR, "GL_FOG_COLOR"},
  {&__pyx_n_GL_FOG_DENSITY, "GL_FOG_DENSITY"},
  {&__pyx_n_GL_FOG_END, "GL_FOG_END"},
  {&__pyx_n_GL_FOG_HINT, "GL_FOG_HINT"},
  {&__pyx_n_GL_FOG_INDEX, "GL_FOG_INDEX"},
  {&__pyx_n_GL_FOG_MODE, "GL_FOG_MODE"},
  {&__pyx_n_GL_FOG_START, "GL_FOG_START"},
  {&__pyx_n_GL_FRONT, "GL_FRONT"},
  {&__pyx_n_GL_FRONT_AND_BACK, "GL_FRONT_AND_BACK"},
  {&__pyx_n_GL_FRONT_FACE, "GL_FRONT_FACE"},
  {&__pyx_n_GL_FRONT_LEFT, "GL_FRONT_LEFT"},
  {&__pyx_n_GL_FRONT_RIGHT, "GL_FRONT_RIGHT"},
  {&__pyx_n_GL_FUNC_ADD, "GL_FUNC_ADD"},
  {&__pyx_n_GL_FUNC_REVERSE_SUBTRACT, "GL_FUNC_REVERSE_SUBTRACT"},
  {&__pyx_n_GL_FUNC_SUBTRACT, "GL_FUNC_SUBTRACT"},
  {&__pyx_n_GL_GEQUAL, "GL_GEQUAL"},
  {&__pyx_n_GL_GREATER, "GL_GREATER"},
  {&__pyx_n_GL_GREEN, "GL_GREEN"},
  {&__pyx_n_GL_GREEN_BIAS, "GL_GREEN_BIAS"},
  {&__pyx_n_GL_GREEN_BITS, "GL_GREEN_BITS"},
  {&__pyx_n_GL_GREEN_SCALE, "GL_GREEN_SCALE"},
  {&__pyx_n_GL_HINT_BIT, "GL_HINT_BIT"},
  {&__pyx_n_GL_HISTOGRAM, "GL_HISTOGRAM"},
  {&__pyx_n_GL_HISTOGRAM_ALPHA_SIZE, "GL_HISTOGRAM_ALPHA_SIZE"},
  {&__pyx_n_GL_HISTOGRAM_BLUE_SIZE, "GL_HISTOGRAM_BLUE_SIZE"},
  {&__pyx_n_GL_HISTOGRAM_FORMAT, "GL_HISTOGRAM_FORMAT"},
  {&__pyx_n_GL_HISTOGRAM_GREEN_SIZE, "GL_HISTOGRAM_GREEN_SIZE"},
  {&__pyx_n_GL_HISTOGRAM_LUMINANCE_SIZE, "GL_HISTOGRAM_LUMINANCE_SIZE"},
  {&__pyx_n_GL_HISTOGRAM_RED_SIZE, "GL_HISTOGRAM_RED_SIZE"},
  {&__pyx_n_GL_HISTOGRAM_SINK, "GL_HISTOGRAM_SINK"},
  {&__pyx_n_GL_HISTOGRAM_WIDTH, "GL_HISTOGRAM_WIDTH"},
  {&__pyx_n_GL_INCR, "GL_INCR"},
  {&__pyx_n_GL_INDEX_ARRAY, "GL_INDEX_ARRAY"},
  {&__pyx_n_GL_INDEX_ARRAY_POINTER, "GL_INDEX_ARRAY_POINTER"},
  {&__pyx_n_GL_INDEX_ARRAY_STRIDE, "GL_INDEX_ARRAY_STRIDE"},
  {&__pyx_n_GL_INDEX_ARRAY_TYPE, "GL_INDEX_ARRAY_TYPE"},
  {&__pyx_n_GL_INDEX_BITS, "GL_INDEX_BITS"},
  {&__pyx_n_GL_INDEX_CLEAR_VALUE, "GL_INDEX_CLEAR_VALUE"},
  {&__pyx_n_GL_INDEX_LOGIC_OP, "GL_INDEX_LOGIC_OP"},
  {&__pyx_n_GL_INDEX_MODE, "GL_INDEX_MODE"},
  {&__pyx_n_GL_INDEX_OFFSET, "GL_INDEX_OFFSET"},
  {&__pyx_n_GL_INDEX_SHIFT, "GL_INDEX_SHIFT"},
  {&__pyx_n_GL_INDEX_WRITEMASK, "GL_INDEX_WRITEMASK"},
  {&__pyx_n_GL_INT, "GL_INT"},
  {&__pyx_n_GL_INTENSITY, "GL_INTENSITY"},
  {&__pyx_n_GL_INTENSITY12, "GL_INTENSITY12"},
  {&__pyx_n_GL_INTENSITY16, "GL_INTENSITY16"},
  {&__pyx_n_GL_INTENSITY4, "GL_INTENSITY4"},
  {&__pyx_n_GL_INTENSITY8, "GL_INTENSITY8"},
  {&__pyx_n_GL_INTERPOLATE, "GL_INTERPOLATE"},
  {&__pyx_n_GL_INVALID_ENUM, "GL_INVALID_ENUM"},
  {&__pyx_n_GL_INVALID_OPERATION, "GL_INVALID_OPERATION"},
  {&__pyx_n_GL_INVALID_VALUE, "GL_INVALID_VALUE"},
  {&__pyx_n_GL_INVERT, "GL_INVERT"},
  {&__pyx_n_GL_KEEP, "GL_KEEP"},
  {&__pyx_n_GL_LEFT, "GL_LEFT"},
  {&__pyx_n_GL_LEQUAL, "GL_LEQUAL"},
  {&__pyx_n_GL_LESS, "GL_LESS"},
  {&__pyx_n_GL_LIGHT0, "GL_LIGHT0"},
  {&__pyx_n_GL_LIGHT1, "GL_LIGHT1"},
  {&__pyx_n_GL_LIGHT2, "GL_LIGHT2"},
  {&__pyx_n_GL_LIGHT3, "GL_LIGHT3"},
  {&__pyx_n_GL_LIGHT4, "GL_LIGHT4"},
  {&__pyx_n_GL_LIGHT5, "GL_LIGHT5"},
  {&__pyx_n_GL_LIGHT6, "GL_LIGHT6"},
  {&__pyx_n_GL_LIGHT7, "GL_LIGHT7"},
  {&__pyx_n_GL_LIGHTING, "GL_LIGHTING"},
  {&__pyx_n_GL_LIGHTING_BIT, "GL_LIGHTING_BIT"},
  {&__pyx_n_GL_LIGHT_MODEL_AMBIENT, "GL_LIGHT_MODEL_AMBIENT"},
  {&__pyx_n_GL_LIGHT_MODEL_COLOR_CONTROL, "GL_LIGHT_MODEL_COLOR_CONTROL"},
  {&__pyx_n_GL_LIGHT_MODEL_LOCAL_VIEWER, "GL_LIGHT_MODEL_LOCAL_VIEWER"},
  {&__pyx_n_GL_LIGHT_MODEL_TWO_SIDE, "GL_LIGHT_MODEL_TWO_SIDE"},
  {&__pyx_n_GL_LINE, "GL_LINE"},
  {&__pyx_n_GL_LINEAR, "GL_LINEAR"},
  {&__pyx_n_GL_LINEAR_ATTENUATION, "GL_LINEAR_ATTENUATION"},
  {&__pyx_n_GL_LINEAR_MIPMAP_LINEAR, "GL_LINEAR_MIPMAP_LINEAR"},
  {&__pyx_n_GL_LINEAR_MIPMAP_NEAREST, "GL_LINEAR_MIPMAP_NEAREST"},
  {&__pyx_n_GL_LINES, "GL_LINES"},
  {&__pyx_n_GL_LINE_BIT, "GL_LINE_BIT"},
  {&__pyx_n_GL_LINE_LOOP, "GL_LINE_LOOP"},
  {&__pyx_n_GL_LINE_RESET_TOKEN, "GL_LINE_RESET_TOKEN"},
  {&__pyx_n_GL_LINE_SMOOTH, "GL_LINE_SMOOTH"},
  {&__pyx_n_GL_LINE_SMOOTH_HINT, "GL_LINE_SMOOTH_HINT"},
  {&__pyx_n_GL_LINE_STIPPLE, "GL_LINE_STIPPLE"},
  {&__pyx_n_GL_LINE_STIPPLE_PATTERN, "GL_LINE_STIPPLE_PATTERN"},
  {&__pyx_n_GL_LINE_STIPPLE_REPEAT, "GL_LINE_STIPPLE_REPEAT"},
  {&__pyx_n_GL_LINE_STRIP, "GL_LINE_STRIP"},
  {&__pyx_n_GL_LINE_TOKEN, "GL_LINE_TOKEN"},
  {&__pyx_n_GL_LINE_WIDTH, "GL_LINE_WIDTH"},
  {&__pyx_n_GL_LINE_WIDTH_GRANULARITY, "GL_LINE_WIDTH_GRANULARITY"},
  {&__pyx_n_GL_LINE_WIDTH_RANGE, "GL_LINE_WIDTH_RANGE"},
  {&__pyx_n_GL_LIST_BASE, "GL_LIST_BASE"},
  {&__pyx_n_GL_LIST_BIT, "GL_LIST_BIT"},
  {&__pyx_n_GL_LIST_INDEX, "GL_LIST_INDEX"},
  {&__pyx_n_GL_LIST_MODE, "GL_LIST_MODE"},
  {&__pyx_n_GL_LOAD, "GL_LOAD"},
  {&__pyx_n_GL_LOGIC_OP, "GL_LOGIC_OP"},
  {&__pyx_n_GL_LOGIC_OP_MODE, "GL_LOGIC_OP_MODE"},
  {&__pyx_n_GL_LUMINANCE, "GL_LUMINANCE"},
  {&__pyx_n_GL_LUMINANCE12, "GL_LUMINANCE12"},
  {&__pyx_n_GL_LUMINANCE12_ALPHA12, "GL_LUMINANCE12_ALPHA12"},
  {&__pyx_n_GL_LUMINANCE12_ALPHA4, "GL_LUMINANCE12_ALPHA4"},
  {&__pyx_n_GL_LUMINANCE16, "GL_LUMINANCE16"},
  {&__pyx_n_GL_LUMINANCE16_ALPHA16, "GL_LUMINANCE16_ALPHA16"},
  {&__pyx_n_GL_LUMINANCE4, "GL_LUMINANCE4"},
  {&__pyx_n_GL_LUMINANCE4_ALPHA4, "GL_LUMINANCE4_ALPHA4"},
  {&__pyx_n_GL_LUMINANCE6_ALPHA2, "GL_LUMINANCE6_ALPHA2"},
  {&__pyx_n_GL_LUMINANCE8, "GL_LUMINANCE8"},
  {&__pyx_n_GL_LUMINANCE8_ALPHA8, "GL_LUMINANCE8_ALPHA8"},
  {&__pyx_n_GL_LUMINANCE_ALPHA, "GL_LUMINANCE_ALPHA"},
  {&__pyx_n_GL_MAP1_COLOR_4, "GL_MAP1_COLOR_4"},
  {&__pyx_n_GL_MAP1_GRID_DOMAIN, "GL_MAP1_GRID_DOMAIN"},
  {&__pyx_n_GL_MAP1_GRID_SEGMENTS, "GL_MAP1_GRID_SEGMENTS"},
  {&__pyx_n_GL_MAP1_INDEX, "GL_MAP1_INDEX"},
  {&__pyx_n_GL_MAP1_NORMAL, "GL_MAP1_NORMAL"},
  {&__pyx_n_GL_MAP1_TEXTURE_COORD_1, "GL_MAP1_TEXTURE_COORD_1"},
  {&__pyx_n_GL_MAP1_TEXTURE_COORD_2, "GL_MAP1_TEXTURE_COORD_2"},
  {&__pyx_n_GL_MAP1_TEXTURE_COORD_3, "GL_MAP1_TEXTURE_COORD_3"},
  {&__pyx_n_GL_MAP1_TEXTURE_COORD_4, "GL_MAP1_TEXTURE_COORD_4"},
  {&__pyx_n_GL_MAP1_VERTEX_3, "GL_MAP1_VERTEX_3"},
  {&__pyx_n_GL_MAP1_VERTEX_4, "GL_MAP1_VERTEX_4"},
  {&__pyx_n_GL_MAP2_COLOR_4, "GL_MAP2_COLOR_4"},
  {&__pyx_n_GL_MAP2_GRID_DOMAIN, "GL_MAP2_GRID_DOMAIN"},
  {&__pyx_n_GL_MAP2_GRID_SEGMENTS, "GL_MAP2_GRID_SEGMENTS"},
  {&__pyx_n_GL_MAP2_INDEX, "GL_MAP2_INDEX"},
  {&__pyx_n_GL_MAP2_NORMAL, "GL_MAP2_NORMAL"},
  {&__pyx_n_GL_MAP2_TEXTURE_COORD_1, "GL_MAP2_TEXTURE_COORD_1"},
  {&__pyx_n_GL_MAP2_TEXTURE_COORD_2, "GL_MAP2_TEXTURE_COORD_2"},
  {&__pyx_n_GL_MAP2_TEXTURE_COORD_3, "GL_MAP2_TEXTURE_COORD_3"},
  {&__pyx_n_GL_MAP2_TEXTURE_COORD_4, "GL_MAP2_TEXTURE_COORD_4"},
  {&__pyx_n_GL_MAP2_VERTEX_3, "GL_MAP2_VERTEX_3"},
  {&__pyx_n_GL_MAP2_VERTEX_4, "GL_MAP2_VERTEX_4"},
  {&__pyx_n_GL_MAP_COLOR, "GL_MAP_COLOR"},
  {&__pyx_n_GL_MAP_STENCIL, "GL_MAP_STENCIL"},
  {&__pyx_n_GL_MATRIX_MODE, "GL_MATRIX_MODE"},
  {&__pyx_n_GL_MAX, "GL_MAX"},
  {&__pyx_n_GL_MAX_3D_TEXTURE_SIZE, "GL_MAX_3D_TEXTURE_SIZE"},
  {&__pyx_n_GL_MAX_ATTRIB_STACK_DEPTH, "GL_MAX_ATTRIB_STACK_DEPTH"},
  {&__pyx_n_GL_MAX_CLIENT_ATTRIB_STACK_DEPTH, "GL_MAX_CLIENT_ATTRIB_STACK_DEPTH"},
  {&__pyx_n_GL_MAX_CLIP_PLANES, "GL_MAX_CLIP_PLANES"},
  {&__pyx_n_GL_MAX_COLOR_MATRIX_STACK_DEPTH, "GL_MAX_COLOR_MATRIX_STACK_DEPTH"},
  {&__pyx_n_GL_MAX_CONVOLUTION_HEIGHT, "GL_MAX_CONVOLUTION_HEIGHT"},
  {&__pyx_n_GL_MAX_CONVOLUTION_WIDTH, "GL_MAX_CONVOLUTION_WIDTH"},
  {&__pyx_n_GL_MAX_CUBE_MAP_TEXTURE_SIZE, "GL_MAX_CUBE_MAP_TEXTURE_SIZE"},
  {&__pyx_n_GL_MAX_ELEMENTS_INDICES, "GL_MAX_ELEMENTS_INDICES"},
  {&__pyx_n_GL_MAX_ELEMENTS_VERTICES, "GL_MAX_ELEMENTS_VERTICES"},
  {&__pyx_n_GL_MAX_EVAL_ORDER, "GL_MAX_EVAL_ORDER"},
  {&__pyx_n_GL_MAX_LIGHTS, "GL_MAX_LIGHTS"},
  {&__pyx_n_GL_MAX_LIST_NESTING, "GL_MAX_LIST_NESTING"},
  {&__pyx_n_GL_MAX_MODELVIEW_STACK_DEPTH, "GL_MAX_MODELVIEW_STACK_DEPTH"},
  {&__pyx_n_GL_MAX_NAME_STACK_DEPTH, "GL_MAX_NAME_STACK_DEPTH"},
  {&__pyx_n_GL_MAX_PIXEL_MAP_TABLE, "GL_MAX_PIXEL_MAP_TABLE"},
  {&__pyx_n_GL_MAX_PROJECTION_STACK_DEPTH, "GL_MAX_PROJECTION_STACK_DEPTH"},
  {&__pyx_n_GL_MAX_TEXTURE_SIZE, "GL_MAX_TEXTURE_SIZE"},
  {&__pyx_n_GL_MAX_TEXTURE_STACK_DEPTH, "GL_MAX_TEXTURE_STACK_DEPTH"},
  {&__pyx_n_GL_MAX_TEXTURE_UNITS, "GL_MAX_TEXTURE_UNITS"},
  {&__pyx_n_GL_MAX_VIEWPORT_DIMS, "GL_MAX_VIEWPORT_DIMS"},
  {&__pyx_n_GL_MIN, "GL_MIN"},
  {&__pyx_n_GL_MINMAX, "GL_MINMAX"},
  {&__pyx_n_GL_MINMAX_FORMAT, "GL_MINMAX_FORMAT"},
  {&__pyx_n_GL_MINMAX_SINK, "GL_MINMAX_SINK"},
  {&__pyx_n_GL_MODELVIEW, "GL_MODELVIEW"},
  {&__pyx_n_GL_MODELVIEW_MATRIX, "GL_MODELVIEW_MATRIX"},
  {&__pyx_n_GL_MODELVIEW_STACK_DEPTH, "GL_MODELVIEW_STACK_DEPTH"},
  {&__pyx_n_GL_MODULATE, "GL_MODULATE"},
  {&__pyx_n_GL_MULT, "GL_MULT"},
  {&__pyx_n_GL_MULTISAMPLE, "GL_MULTISAMPLE"},
  {&__pyx_n_GL_MULTISAMPLE_BIT, "GL_MULTISAMPLE_BIT"},
  {&__pyx_n_GL_N3F_V3F, "GL_N3F_V3F"},
  {&__pyx_n_GL_NAME_STACK_DEPTH, "GL_NAME_STACK_DEPTH"},
  {&__pyx_n_GL_NAND, "GL_NAND"},
  {&__pyx_n_GL_NEAREST, "GL_NEAREST"},
  {&__pyx_n_GL_NEAREST_MIPMAP_LINEAR, "GL_NEAREST_MIPMAP_LINEAR"},
  {&__pyx_n_GL_NEAREST_MIPMAP_NEAREST, "GL_NEAREST_MIPMAP_NEAREST"},
  {&__pyx_n_GL_NEVER, "GL_NEVER"},
  {&__pyx_n_GL_NICEST, "GL_NICEST"},
  {&__pyx_n_GL_NONE, "GL_NONE"},
  {&__pyx_n_GL_NOOP, "GL_NOOP"},
  {&__pyx_n_GL_NOR, "GL_NOR"},
  {&__pyx_n_GL_NORMALIZE, "GL_NORMALIZE"},
  {&__pyx_n_GL_NORMAL_ARRAY, "GL_NORMAL_ARRAY"},
  {&__pyx_n_GL_NORMAL_ARRAY_POINTER, "GL_NORMAL_ARRAY_POINTER"},
  {&__pyx_n_GL_NORMAL_ARRAY_STRIDE, "GL_NORMAL_ARRAY_STRIDE"},
  {&__pyx_n_GL_NORMAL_ARRAY_TYPE, "GL_NORMAL_ARRAY_TYPE"},
  {&__pyx_n_GL_NORMAL_MAP, "GL_NORMAL_MAP"},
  {&__pyx_n_GL_NOTEQUAL, "GL_NOTEQUAL"},
  {&__pyx_n_GL_NO_ERROR, "GL_NO_ERROR"},
  {&__pyx_n_GL_NUM_COMPRESSED_TEXTURE_FORMATS, "GL_NUM_COMPRESSED_TEXTURE_FORMATS"},
  {&__pyx_n_GL_OBJECT_LINEAR, "GL_OBJECT_LINEAR"},
  {&__pyx_n_GL_OBJECT_PLANE, "GL_OBJECT_PLANE"},
  {&__pyx_n_GL_ONE, "GL_ONE"},
  {&__pyx_n_GL_ONE_MINUS_CONSTANT_ALPHA, "GL_ONE_MINUS_CONSTANT_ALPHA"},
  {&__pyx_n_GL_ONE_MINUS_CONSTANT_COLOR, "GL_ONE_MINUS_CONSTANT_COLOR"},
  {&__pyx_n_GL_ONE_MINUS_DST_ALPHA, "GL_ONE_MINUS_DST_ALPHA"},
  {&__pyx_n_GL_ONE_MINUS_DST_COLOR, "GL_ONE_MINUS_DST_COLOR"},
  {&__pyx_n_GL_ONE_MINUS_SRC_ALPHA, "GL_ONE_MINUS_SRC_ALPHA"},
  {&__pyx_n_GL_ONE_MINUS_SRC_COLOR, "GL_ONE_MINUS_SRC_COLOR"},
  {&__pyx_n_GL_OPERAND0_ALPHA, "GL_OPERAND0_ALPHA"},
  {&__pyx_n_GL_OPERAND0_RGB, "GL_OPERAND0_RGB"},
  {&__pyx_n_GL_OPERAND1_ALPHA, "GL_OPERAND1_ALPHA"},
  {&__pyx_n_GL_OPERAND1_RGB, "GL_OPERAND1_RGB"},
  {&__pyx_n_GL_OPERAND2_ALPHA, "GL_OPERAND2_ALPHA"},
  {&__pyx_n_GL_OPERAND2_RGB, "GL_OPERAND2_RGB"},
  {&__pyx_n_GL_OR, "GL_OR"},
  {&__pyx_n_GL_ORDER, "GL_ORDER"},
  {&__pyx_n_GL_OR_INVERTED, "GL_OR_INVERTED"},
  {&__pyx_n_GL_OR_REVERSE, "GL_OR_REVERSE"},
  {&__pyx_n_GL_OUT_OF_MEMORY, "GL_OUT_OF_MEMORY"},
  {&__pyx_n_GL_PACK_ALIGNMENT, "GL_PACK_ALIGNMENT"},
  {&__pyx_n_GL_PACK_IMAGE_HEIGHT, "GL_PACK_IMAGE_HEIGHT"},
  {&__pyx_n_GL_PACK_LSB_FIRST, "GL_PACK_LSB_FIRST"},
  {&__pyx_n_GL_PACK_ROW_LENGTH, "GL_PACK_ROW_LENGTH"},
  {&__pyx_n_GL_PACK_SKIP_IMAGES, "GL_PACK_SKIP_IMAGES"},
  {&__pyx_n_GL_PACK_SKIP_PIXELS, "GL_PACK_SKIP_PIXELS"},
  {&__pyx_n_GL_PACK_SKIP_ROWS, "GL_PACK_SKIP_ROWS"},
  {&__pyx_n_GL_PACK_SWAP_BYTES, "GL_PACK_SWAP_BYTES"},
  {&__pyx_n_GL_PASS_THROUGH_TOKEN, "GL_PASS_THROUGH_TOKEN"},
  {&__pyx_n_GL_PERSPECTIVE_CORRECTION_HINT, "GL_PERSPECTIVE_CORRECTION_HINT"},
  {&__pyx_n_GL_PIXEL_MAP_A_TO_A, "GL_PIXEL_MAP_A_TO_A"},
  {&__pyx_n_GL_PIXEL_MAP_A_TO_A_SIZE, "GL_PIXEL_MAP_A_TO_A_SIZE"},
  {&__pyx_n_GL_PIXEL_MAP_B_TO_B, "GL_PIXEL_MAP_B_TO_B"},
  {&__pyx_n_GL_PIXEL_MAP_B_TO_B_SIZE, "GL_PIXEL_MAP_B_TO_B_SIZE"},
  {&__pyx_n_GL_PIXEL_MAP_G_TO_G, "GL_PIXEL_MAP_G_TO_G"},
  {&__pyx_n_GL_PIXEL_MAP_G_TO_G_SIZE, "GL_PIXEL_MAP_G_TO_G_SIZE"},
  {&__pyx_n_GL_PIXEL_MAP_I_TO_A, "GL_PIXEL_MAP_I_TO_A"},
  {&__pyx_n_GL_PIXEL_MAP_I_TO_A_SIZE, "GL_PIXEL_MAP_I_TO_A_SIZE"},
  {&__pyx_n_GL_PIXEL_MAP_I_TO_B, "GL_PIXEL_MAP_I_TO_B"},
  {&__pyx_n_GL_PIXEL_MAP_I_TO_B_SIZE, "GL_PIXEL_MAP_I_TO_B_SIZE"},
  {&__pyx_n_GL_PIXEL_MAP_I_TO_G, "GL_PIXEL_MAP_I_TO_G"},
  {&__pyx_n_GL_PIXEL_MAP_I_TO_G_SIZE, "GL_PIXEL_MAP_I_TO_G_SIZE"},
  {&__pyx_n_GL_PIXEL_MAP_I_TO_I, "GL_PIXEL_MAP_I_TO_I"},
  {&__pyx_n_GL_PIXEL_MAP_I_TO_I_SIZE, "GL_PIXEL_MAP_I_TO_I_SIZE"},
  {&__pyx_n_GL_PIXEL_MAP_I_TO_R, "GL_PIXEL_MAP_I_TO_R"},
  {&__pyx_n_GL_PIXEL_MAP_I_TO_R_SIZE, "GL_PIXEL_MAP_I_TO_R_SIZE"},
  {&__pyx_n_GL_PIXEL_MAP_R_TO_R, "GL_PIXEL_MAP_R_TO_R"},
  {&__pyx_n_GL_PIXEL_MAP_R_TO_R_SIZE, "GL_PIXEL_MAP_R_TO_R_SIZE"},
  {&__pyx_n_GL_PIXEL_MAP_S_TO_S, "GL_PIXEL_MAP_S_TO_S"},
  {&__pyx_n_GL_PIXEL_MAP_S_TO_S_SIZE, "GL_PIXEL_MAP_S_TO_S_SIZE"},
  {&__pyx_n_GL_PIXEL_MODE_BIT, "GL_PIXEL_MODE_BIT"},
  {&__pyx_n_GL_POINT, "GL_POINT"},
  {&__pyx_n_GL_POINTS, "GL_POINTS"},
  {&__pyx_n_GL_POINT_BIT, "GL_POINT_BIT"},
  {&__pyx_n_GL_POINT_SIZE, "GL_POINT_SIZE"},
  {&__pyx_n_GL_POINT_SIZE_GRANULARITY, "GL_POINT_SIZE_GRANULARITY"},
  {&__pyx_n_GL_POINT_SIZE_RANGE, "GL_POINT_SIZE_RANGE"},
  {&__pyx_n_GL_POINT_SMOOTH, "GL_POINT_SMOOTH"},
  {&__pyx_n_GL_POINT_SMOOTH_HINT, "GL_POINT_SMOOTH_HINT"},
  {&__pyx_n_GL_POINT_TOKEN, "GL_POINT_TOKEN"},
  {&__pyx_n_GL_POLYGON, "GL_POLYGON"},
  {&__pyx_n_GL_POLYGON_BIT, "GL_POLYGON_BIT"},
  {&__pyx_n_GL_POLYGON_MODE, "GL_POLYGON_MODE"},
  {&__pyx_n_GL_POLYGON_OFFSET_FACTOR, "GL_POLYGON_OFFSET_FACTOR"},
  {&__pyx_n_GL_POLYGON_OFFSET_FILL, "GL_POLYGON_OFFSET_FILL"},
  {&__pyx_n_GL_POLYGON_OFFSET_LINE, "GL_POLYGON_OFFSET_LINE"},
  {&__pyx_n_GL_POLYGON_OFFSET_POINT, "GL_POLYGON_OFFSET_POINT"},
  {&__pyx_n_GL_POLYGON_OFFSET_UNITS, "GL_POLYGON_OFFSET_UNITS"},
  {&__pyx_n_GL_POLYGON_SMOOTH, "GL_POLYGON_SMOOTH"},
  {&__pyx_n_GL_POLYGON_SMOOTH_HINT, "GL_POLYGON_SMOOTH_HINT"},
  {&__pyx_n_GL_POLYGON_STIPPLE, "GL_POLYGON_STIPPLE"},
  {&__pyx_n_GL_POLYGON_STIPPLE_BIT, "GL_POLYGON_STIPPLE_BIT"},
  {&__pyx_n_GL_POLYGON_TOKEN, "GL_POLYGON_TOKEN"},
  {&__pyx_n_GL_POSITION, "GL_POSITION"},
  {&__pyx_n_GL_POST_COLOR_MATRIX_ALPHA_BIAS, "GL_POST_COLOR_MATRIX_ALPHA_BIAS"},
  {&__pyx_n_GL_POST_COLOR_MATRIX_ALPHA_SCALE, "GL_POST_COLOR_MATRIX_ALPHA_SCALE"},
  {&__pyx_n_GL_POST_COLOR_MATRIX_BLUE_BIAS, "GL_POST_COLOR_MATRIX_BLUE_BIAS"},
  {&__pyx_n_GL_POST_COLOR_MATRIX_BLUE_SCALE, "GL_POST_COLOR_MATRIX_BLUE_SCALE"},
  {&__pyx_n_GL_POST_COLOR_MATRIX_COLOR_TABLE, "GL_POST_COLOR_MATRIX_COLOR_TABLE"},
  {&__pyx_n_GL_POST_COLOR_MATRIX_GREEN_BIAS, "GL_POST_COLOR_MATRIX_GREEN_BIAS"},
  {&__pyx_n_GL_POST_COLOR_MATRIX_GREEN_SCALE, "GL_POST_COLOR_MATRIX_GREEN_SCALE"},
  {&__pyx_n_GL_POST_COLOR_MATRIX_RED_BIAS, "GL_POST_COLOR_MATRIX_RED_BIAS"},
  {&__pyx_n_GL_POST_COLOR_MATRIX_RED_SCALE, "GL_POST_COLOR_MATRIX_RED_SCALE"},
  {&__pyx_n_GL_POST_CONVOLUTION_ALPHA_BIAS, "GL_POST_CONVOLUTION_ALPHA_BIAS"},
  {&__pyx_n_GL_POST_CONVOLUTION_ALPHA_SCALE, "GL_POST_CONVOLUTION_ALPHA_SCALE"},
  {&__pyx_n_GL_POST_CONVOLUTION_BLUE_BIAS, "GL_POST_CONVOLUTION_BLUE_BIAS"},
  {&__pyx_n_GL_POST_CONVOLUTION_BLUE_SCALE, "GL_POST_CONVOLUTION_BLUE_SCALE"},
  {&__pyx_n_GL_POST_CONVOLUTION_COLOR_TABLE, "GL_POST_CONVOLUTION_COLOR_TABLE"},
  {&__pyx_n_GL_POST_CONVOLUTION_GREEN_BIAS, "GL_POST_CONVOLUTION_GREEN_BIAS"},
  {&__pyx_n_GL_POST_CONVOLUTION_GREEN_SCALE, "GL_POST_CONVOLUTION_GREEN_SCALE"},
  {&__pyx_n_GL_POST_CONVOLUTION_RED_BIAS, "GL_POST_CONVOLUTION_RED_BIAS"},
  {&__pyx_n_GL_POST_CONVOLUTION_RED_SCALE, "GL_POST_CONVOLUTION_RED_SCALE"},
  {&__pyx_n_GL_PREVIOUS, "GL_PREVIOUS"},
  {&__pyx_n_GL_PRIMARY_COLOR, "GL_PRIMARY_COLOR"},
  {&__pyx_n_GL_PROJECTION, "GL_PROJECTION"},
  {&__pyx_n_GL_PROJECTION_MATRIX, "GL_PROJECTION_MATRIX"},
  {&__pyx_n_GL_PROJECTION_STACK_DEPTH, "GL_PROJECTION_STACK_DEPTH"},
  {&__pyx_n_GL_PROXY_COLOR_TABLE, "GL_PROXY_COLOR_TABLE"},
  {&__pyx_n_GL_PROXY_HISTOGRAM, "GL_PROXY_HISTOGRAM"},
  {&__pyx_n_GL_PROXY_POST_COLOR_MATRIX_COLOR_TABLE, "GL_PROXY_POST_COLOR_MATRIX_COLOR_TABLE"},
  {&__pyx_n_GL_PROXY_POST_CONVOLUTION_COLOR_TABLE, "GL_PROXY_POST_CONVOLUTION_COLOR_TABLE"},
  {&__pyx_n_GL_PROXY_TEXTURE_1D, "GL_PROXY_TEXTURE_1D"},
  {&__pyx_n_GL_PROXY_TEXTURE_2D, "GL_PROXY_TEXTURE_2D"},
  {&__pyx_n_GL_PROXY_TEXTURE_3D, "GL_PROXY_TEXTURE_3D"},
  {&__pyx_n_GL_PROXY_TEXTURE_CUBE_MAP, "GL_PROXY_TEXTURE_CUBE_MAP"},
  {&__pyx_n_GL_Q, "GL_Q"},
  {&__pyx_n_GL_QUADRATIC_ATTENUATION, "GL_QUADRATIC_ATTENUATION"},
  {&__pyx_n_GL_QUADS, "GL_QUADS"},
  {&__pyx_n_GL_QUAD_STRIP, "GL_QUAD_STRIP"},
  {&__pyx_n_GL_R, "GL_R"},
  {&__pyx_n_GL_R3_G3_B2, "GL_R3_G3_B2"},
  {&__pyx_n_GL_READ_BUFFER, "GL_READ_BUFFER"},
  {&__pyx_n_GL_RED, "GL_RED"},
  {&__pyx_n_GL_REDUCE, "GL_REDUCE"},
  {&__pyx_n_GL_RED_BIAS, "GL_RED_BIAS"},
  {&__pyx_n_GL_RED_BITS, "GL_RED_BITS"},
  {&__pyx_n_GL_RED_SCALE, "GL_RED_SCALE"},
  {&__pyx_n_GL_REFLECTION_MAP, "GL_REFLECTION_MAP"},
  {&__pyx_n_GL_RENDER, "GL_RENDER"},
  {&__pyx_n_GL_RENDERER, "GL_RENDERER"},
  {&__pyx_n_GL_RENDER_MODE, "GL_RENDER_MODE"},
  {&__pyx_n_GL_REPEAT, "GL_REPEAT"},
  {&__pyx_n_GL_REPLACE, "GL_REPLACE"},
  {&__pyx_n_GL_REPLICATE_BORDER, "GL_REPLICATE_BORDER"},
  {&__pyx_n_GL_RESCALE_NORMAL, "GL_RESCALE_NORMAL"},
  {&__pyx_n_GL_RETURN, "GL_RETURN"},
  {&__pyx_n_GL_RGB, "GL_RGB"},
  {&__pyx_n_GL_RGB10, "GL_RGB10"},
  {&__pyx_n_GL_RGB10_A2, "GL_RGB10_A2"},
  {&__pyx_n_GL_RGB12, "GL_RGB12"},
  {&__pyx_n_GL_RGB16, "GL_RGB16"},
  {&__pyx_n_GL_RGB4, "GL_RGB4"},
  {&__pyx_n_GL_RGB5, "GL_RGB5"},
  {&__pyx_n_GL_RGB5_A1, "GL_RGB5_A1"},
  {&__pyx_n_GL_RGB8, "GL_RGB8"},
  {&__pyx_n_GL_RGBA, "GL_RGBA"},
  {&__pyx_n_GL_RGBA12, "GL_RGBA12"},
  {&__pyx_n_GL_RGBA16, "GL_RGBA16"},
  {&__pyx_n_GL_RGBA2, "GL_RGBA2"},
  {&__pyx_n_GL_RGBA4, "GL_RGBA4"},
  {&__pyx_n_GL_RGBA8, "GL_RGBA8"},
  {&__pyx_n_GL_RGBA_MODE, "GL_RGBA_MODE"},
  {&__pyx_n_GL_RGB_SCALE, "GL_RGB_SCALE"},
  {&__pyx_n_GL_RIGHT, "GL_RIGHT"},
  {&__pyx_n_GL_S, "GL_S"},
  {&__pyx_n_GL_SAMPLES, "GL_SAMPLES"},
  {&__pyx_n_GL_SAMPLE_ALPHA_TO_COVERAGE, "GL_SAMPLE_ALPHA_TO_COVERAGE"},
  {&__pyx_n_GL_SAMPLE_ALPHA_TO_ONE, "GL_SAMPLE_ALPHA_TO_ONE"},
  {&__pyx_n_GL_SAMPLE_BUFFERS, "GL_SAMPLE_BUFFERS"},
  {&__pyx_n_GL_SAMPLE_COVERAGE, "GL_SAMPLE_COVERAGE"},
  {&__pyx_n_GL_SAMPLE_COVERAGE_INVERT, "GL_SAMPLE_COVERAGE_INVERT"},
  {&__pyx_n_GL_SAMPLE_COVERAGE_VALUE, "GL_SAMPLE_COVERAGE_VALUE"},
  {&__pyx_n_GL_SCISSOR_BIT, "GL_SCISSOR_BIT"},
  {&__pyx_n_GL_SCISSOR_BOX, "GL_SCISSOR_BOX"},
  {&__pyx_n_GL_SCISSOR_TEST, "GL_SCISSOR_TEST"},
  {&__pyx_n_GL_SELECT, "GL_SELECT"},
  {&__pyx_n_GL_SELECTION_BUFFER_POINTER, "GL_SELECTION_BUFFER_POINTER"},
  {&__pyx_n_GL_SELECTION_BUFFER_SIZE, "GL_SELECTION_BUFFER_SIZE"},
  {&__pyx_n_GL_SEPARABLE_2D, "GL_SEPARABLE_2D"},
  {&__pyx_n_GL_SEPARATE_SPECULAR_COLOR, "GL_SEPARATE_SPECULAR_COLOR"},
  {&__pyx_n_GL_SET, "GL_SET"},
  {&__pyx_n_GL_SHADE_MODEL, "GL_SHADE_MODEL"},
  {&__pyx_n_GL_SHININESS, "GL_SHININESS"},
  {&__pyx_n_GL_SHORT, "GL_SHORT"},
  {&__pyx_n_GL_SINGLE_COLOR, "GL_SINGLE_COLOR"},
  {&__pyx_n_GL_SMOOTH, "GL_SMOOTH"},
  {&__pyx_n_GL_SMOOTH_LINE_WIDTH_GRANULARITY, "GL_SMOOTH_LINE_WIDTH_GRANULARITY"},
  {&__pyx_n_GL_SMOOTH_LINE_WIDTH_RANGE, "GL_SMOOTH_LINE_WIDTH_RANGE"},
  {&__pyx_n_GL_SMOOTH_POINT_SIZE_GRANULARITY, "GL_SMOOTH_POINT_SIZE_GRANULARITY"},
  {&__pyx_n_GL_SMOOTH_POINT_SIZE_RANGE, "GL_SMOOTH_POINT_SIZE_RANGE"},
  {&__pyx_n_GL_SOURCE0_ALPHA, "GL_SOURCE0_ALPHA"},
  {&__pyx_n_GL_SOURCE0_RGB, "GL_SOURCE0_RGB"},
  {&__pyx_n_GL_SOURCE1_ALPHA, "GL_SOURCE1_ALPHA"},
  {&__pyx_n_GL_SOURCE1_RGB, "GL_SOURCE1_RGB"},
  {&__pyx_n_GL_SOURCE2_ALPHA, "GL_SOURCE2_ALPHA"},
  {&__pyx_n_GL_SOURCE2_RGB, "GL_SOURCE2_RGB"},
  {&__pyx_n_GL_SPECULAR, "GL_SPECULAR"},
  {&__pyx_n_GL_SPHERE_MAP, "GL_SPHERE_MAP"},
  {&__pyx_n_GL_SPOT_CUTOFF, "GL_SPOT_CUTOFF"},
  {&__pyx_n_GL_SPOT_DIRECTION, "GL_SPOT_DIRECTION"},
  {&__pyx_n_GL_SPOT_EXPONENT, "GL_SPOT_EXPONENT"},
  {&__pyx_n_GL_SRC_ALPHA, "GL_SRC_ALPHA"},
  {&__pyx_n_GL_SRC_ALPHA_SATURATE, "GL_SRC_ALPHA_SATURATE"},
  {&__pyx_n_GL_SRC_COLOR, "GL_SRC_COLOR"},
  {&__pyx_n_GL_STACK_OVERFLOW, "GL_STACK_OVERFLOW"},
  {&__pyx_n_GL_STACK_UNDERFLOW, "GL_STACK_UNDERFLOW"},
  {&__pyx_n_GL_STENCIL, "GL_STENCIL"},
  {&__pyx_n_GL_STENCIL_BITS, "GL_STENCIL_BITS"},
  {&__pyx_n_GL_STENCIL_BUFFER_BIT, "GL_STENCIL_BUFFER_BIT"},
  {&__pyx_n_GL_STENCIL_CLEAR_VALUE, "GL_STENCIL_CLEAR_VALUE"},
  {&__pyx_n_GL_STENCIL_FAIL, "GL_STENCIL_FAIL"},
  {&__pyx_n_GL_STENCIL_FUNC, "GL_STENCIL_FUNC"},
  {&__pyx_n_GL_STENCIL_INDEX, "GL_STENCIL_INDEX"},
  {&__pyx_n_GL_STENCIL_PASS_DEPTH_FAIL, "GL_STENCIL_PASS_DEPTH_FAIL"},
  {&__pyx_n_GL_STENCIL_PASS_DEPTH_PASS, "GL_STENCIL_PASS_DEPTH_PASS"},
  {&__pyx_n_GL_STENCIL_REF, "GL_STENCIL_REF"},
  {&__pyx_n_GL_STENCIL_TEST, "GL_STENCIL_TEST"},
  {&__pyx_n_GL_STENCIL_VALUE_MASK, "GL_STENCIL_VALUE_MASK"},
  {&__pyx_n_GL_STENCIL_WRITEMASK, "GL_STENCIL_WRITEMASK"},
  {&__pyx_n_GL_STEREO, "GL_STEREO"},
  {&__pyx_n_GL_SUBPIXEL_BITS, "GL_SUBPIXEL_BITS"},
  {&__pyx_n_GL_SUBTRACT, "GL_SUBTRACT"},
  {&__pyx_n_GL_T, "GL_T"},
  {&__pyx_n_GL_T2F_C3F_V3F, "GL_T2F_C3F_V3F"},
  {&__pyx_n_GL_T2F_C4F_N3F_V3F, "GL_T2F_C4F_N3F_V3F"},
  {&__pyx_n_GL_T2F_C4UB_V3F, "GL_T2F_C4UB_V3F"},
  {&__pyx_n_GL_T2F_N3F_V3F, "GL_T2F_N3F_V3F"},
  {&__pyx_n_GL_T2F_V3F, "GL_T2F_V3F"},
  {&__pyx_n_GL_T4F_C4F_N3F_V4F, "GL_T4F_C4F_N3F_V4F"},
  {&__pyx_n_GL_T4F_V4F, "GL_T4F_V4F"},
  {&__pyx_n_GL_TABLE_TOO_LARGE, "GL_TABLE_TOO_LARGE"},
  {&__pyx_n_GL_TEXTURE, "GL_TEXTURE"},
  {&__pyx_n_GL_TEXTURE0, "GL_TEXTURE0"},
  {&__pyx_n_GL_TEXTURE1, "GL_TEXTURE1"},
  {&__pyx_n_GL_TEXTURE10, "GL_TEXTURE10"},
  {&__pyx_n_GL_TEXTURE11, "GL_TEXTURE11"},
  {&__pyx_n_GL_TEXTURE12, "GL_TEXTURE12"},
  {&__pyx_n_GL_TEXTURE13, "GL_TEXTURE13"},
  {&__pyx_n_GL_TEXTURE14, "GL_TEXTURE14"},
  {&__pyx_n_GL_TEXTURE15, "GL_TEXTURE15"},
  {&__pyx_n_GL_TEXTURE16, "GL_TEXTURE16"},
  {&__pyx_n_GL_TEXTURE17, "GL_TEXTURE17"},
  {&__pyx_n_GL_TEXTURE18, "GL_TEXTURE18"},
  {&__pyx_n_GL_TEXTURE19, "GL_TEXTURE19"},
  {&__pyx_n_GL_TEXTURE2, "GL_TEXTURE2"},
  {&__pyx_n_GL_TEXTURE20, "GL_TEXTURE20"},
  {&__pyx_n_GL_TEXTURE21, "GL_TEXTURE21"},
  {&__pyx_n_GL_TEXTURE22, "GL_TEXTURE22"},
  {&__pyx_n_GL_TEXTURE23, "GL_TEXTURE23"},
  {&__pyx_n_GL_TEXTURE24, "GL_TEXTURE24"},
  {&__pyx_n_GL_TEXTURE25, "GL_TEXTURE25"},
  {&__pyx_n_GL_TEXTURE26, "GL_TEXTURE26"},
  {&__pyx_n_GL_TEXTURE27, "GL_TEXTURE27"},
  {&__pyx_n_GL_TEXTURE28, "GL_TEXTURE28"},
  {&__pyx_n_GL_TEXTURE29, "GL_TEXTURE29"},
  {&__pyx_n_GL_TEXTURE3, "GL_TEXTURE3"},
  {&__pyx_n_GL_TEXTURE30, "GL_TEXTURE30"},
  {&__pyx_n_GL_TEXTURE31, "GL_TEXTURE31"},
  {&__pyx_n_GL_TEXTURE4, "GL_TEXTURE4"},
  {&__pyx_n_GL_TEXTURE5, "GL_TEXTURE5"},
  {&__pyx_n_GL_TEXTURE6, "GL_TEXTURE6"},
  {&__pyx_n_GL_TEXTURE7, "GL_TEXTURE7"},
  {&__pyx_n_GL_TEXTURE8, "GL_TEXTURE8"},
  {&__pyx_n_GL_TEXTURE9, "GL_TEXTURE9"},
  {&__pyx_n_GL_TEXTURE_1D, "GL_TEXTURE_1D"},
  {&__pyx_n_GL_TEXTURE_2D, "GL_TEXTURE_2D"},
  {&__pyx_n_GL_TEXTURE_3D, "GL_TEXTURE_3D"},
  {&__pyx_n_GL_TEXTURE_ALPHA_SIZE, "GL_TEXTURE_ALPHA_SIZE"},
  {&__pyx_n_GL_TEXTURE_BASE_LEVEL, "GL_TEXTURE_BASE_LEVEL"},
  {&__pyx_n_GL_TEXTURE_BINDING_1D, "GL_TEXTURE_BINDING_1D"},
  {&__pyx_n_GL_TEXTURE_BINDING_2D, "GL_TEXTURE_BINDING_2D"},
  {&__pyx_n_GL_TEXTURE_BINDING_3D, "GL_TEXTURE_BINDING_3D"},
  {&__pyx_n_GL_TEXTURE_BINDING_CUBE_MAP, "GL_TEXTURE_BINDING_CUBE_MAP"},
  {&__pyx_n_GL_TEXTURE_BIT, "GL_TEXTURE_BIT"},
  {&__pyx_n_GL_TEXTURE_BLUE_SIZE, "GL_TEXTURE_BLUE_SIZE"},
  {&__pyx_n_GL_TEXTURE_BORDER, "GL_TEXTURE_BORDER"},
  {&__pyx_n_GL_TEXTURE_BORDER_COLOR, "GL_TEXTURE_BORDER_COLOR"},
  {&__pyx_n_GL_TEXTURE_COMPONENTS, "GL_TEXTURE_COMPONENTS"},
  {&__pyx_n_GL_TEXTURE_COMPRESSED, "GL_TEXTURE_COMPRESSED"},
  {&__pyx_n_GL_TEXTURE_COMPRESSED_IMAGE_SIZE, "GL_TEXTURE_COMPRESSED_IMAGE_SIZE"},
  {&__pyx_n_GL_TEXTURE_COMPRESSION_HINT, "GL_TEXTURE_COMPRESSION_HINT"},
  {&__pyx_n_GL_TEXTURE_COORD_ARRAY, "GL_TEXTURE_COORD_ARRAY"},
  {&__pyx_n_GL_TEXTURE_COORD_ARRAY_POINTER, "GL_TEXTURE_COORD_ARRAY_POINTER"},
  {&__pyx_n_GL_TEXTURE_COORD_ARRAY_SIZE, "GL_TEXTURE_COORD_ARRAY_SIZE"},
  {&__pyx_n_GL_TEXTURE_COORD_ARRAY_STRIDE, "GL_TEXTURE_COORD_ARRAY_STRIDE"},
  {&__pyx_n_GL_TEXTURE_COORD_ARRAY_TYPE, "GL_TEXTURE_COORD_ARRAY_TYPE"},
  {&__pyx_n_GL_TEXTURE_CUBE_MAP, "GL_TEXTURE_CUBE_MAP"},
  {&__pyx_n_GL_TEXTURE_CUBE_MAP_NEGATIVE_X, "GL_TEXTURE_CUBE_MAP_NEGATIVE_X"},
  {&__pyx_n_GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, "GL_TEXTURE_CUBE_MAP_NEGATIVE_Y"},
  {&__pyx_n_GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, "GL_TEXTURE_CUBE_MAP_NEGATIVE_Z"},
  {&__pyx_n_GL_TEXTURE_CUBE_MAP_POSITIVE_X, "GL_TEXTURE_CUBE_MAP_POSITIVE_X"},
  {&__pyx_n_GL_TEXTURE_CUBE_MAP_POSITIVE_Y, "GL_TEXTURE_CUBE_MAP_POSITIVE_Y"},
  {&__pyx_n_GL_TEXTURE_CUBE_MAP_POSITIVE_Z, "GL_TEXTURE_CUBE_MAP_POSITIVE_Z"},
  {&__pyx_n_GL_TEXTURE_DEPTH, "GL_TEXTURE_DEPTH"},
  {&__pyx_n_GL_TEXTURE_ENV, "GL_TEXTURE_ENV"},
  {&__pyx_n_GL_TEXTURE_ENV_COLOR, "GL_TEXTURE_ENV_COLOR"},
  {&__pyx_n_GL_TEXTURE_ENV_MODE, "GL_TEXTURE_ENV_MODE"},
  {&__pyx_n_GL_TEXTURE_GEN_MODE, "GL_TEXTURE_GEN_MODE"},
  {&__pyx_n_GL_TEXTURE_GEN_Q, "GL_TEXTURE_GEN_Q"},
  {&__pyx_n_GL_TEXTURE_GEN_R, "GL_TEXTURE_GEN_R"},
  {&__pyx_n_GL_TEXTURE_GEN_S, "GL_TEXTURE_GEN_S"},
  {&__pyx_n_GL_TEXTURE_GEN_T, "GL_TEXTURE_GEN_T"},
  {&__pyx_n_GL_TEXTURE_GREEN_SIZE, "GL_TEXTURE_GREEN_SIZE"},
  {&__pyx_n_GL_TEXTURE_HEIGHT, "GL_TEXTURE_HEIGHT"},
  {&__pyx_n_GL_TEXTURE_INTENSITY_SIZE, "GL_TEXTURE_INTENSITY_SIZE"},
  {&__pyx_n_GL_TEXTURE_INTERNAL_FORMAT, "GL_TEXTURE_INTERNAL_FORMAT"},
  {&__pyx_n_GL_TEXTURE_LUMINANCE_SIZE, "GL_TEXTURE_LUMINANCE_SIZE"},
  {&__pyx_n_GL_TEXTURE_MAG_FILTER, "GL_TEXTURE_MAG_FILTER"},
  {&__pyx_n_GL_TEXTURE_MATRIX, "GL_TEXTURE_MATRIX"},
  {&__pyx_n_GL_TEXTURE_MAX_LEVEL, "GL_TEXTURE_MAX_LEVEL"},
  {&__pyx_n_GL_TEXTURE_MAX_LOD, "GL_TEXTURE_MAX_LOD"},
  {&__pyx_n_GL_TEXTURE_MIN_FILTER, "GL_TEXTURE_MIN_FILTER"},
  {&__pyx_n_GL_TEXTURE_MIN_LOD, "GL_TEXTURE_MIN_LOD"},
  {&__pyx_n_GL_TEXTURE_PRIORITY, "GL_TEXTURE_PRIORITY"},
  {&__pyx_n_GL_TEXTURE_RED_SIZE, "GL_TEXTURE_RED_SIZE"},
  {&__pyx_n_GL_TEXTURE_RESIDENT, "GL_TEXTURE_RESIDENT"},
  {&__pyx_n_GL_TEXTURE_STACK_DEPTH, "GL_TEXTURE_STACK_DEPTH"},
  {&__pyx_n_GL_TEXTURE_WIDTH, "GL_TEXTURE_WIDTH"},
  {&__pyx_n_GL_TEXTURE_WRAP_R, "GL_TEXTURE_WRAP_R"},
  {&__pyx_n_GL_TEXTURE_WRAP_S, "GL_TEXTURE_WRAP_S"},
  {&__pyx_n_GL_TEXTURE_WRAP_T, "GL_TEXTURE_WRAP_T"},
  {&__pyx_n_GL_TRANSFORM_BIT, "GL_TRANSFORM_BIT"},
  {&__pyx_n_GL_TRANSPOSE_COLOR_MATRIX, "GL_TRANSPOSE_COLOR_MATRIX"},
  {&__pyx_n_GL_TRANSPOSE_MODELVIEW_MATRIX, "GL_TRANSPOSE_MODELVIEW_MATRIX"},
  {&__pyx_n_GL_TRANSPOSE_PROJECTION_MATRIX, "GL_TRANSPOSE_PROJECTION_MATRIX"},
  {&__pyx_n_GL_TRANSPOSE_TEXTURE_MATRIX, "GL_TRANSPOSE_TEXTURE_MATRIX"},
  {&__pyx_n_GL_TRIANGLES, "GL_TRIANGLES"},
  {&__pyx_n_GL_TRIANGLE_FAN, "GL_TRIANGLE_FAN"},
  {&__pyx_n_GL_TRIANGLE_STRIP, "GL_TRIANGLE_STRIP"},
  {&__pyx_n_GL_TRUE, "GL_TRUE"},
  {&__pyx_n_GL_UNPACK_ALIGNMENT, "GL_UNPACK_ALIGNMENT"},
  {&__pyx_n_GL_UNPACK_IMAGE_HEIGHT, "GL_UNPACK_IMAGE_HEIGHT"},
  {&__pyx_n_GL_UNPACK_LSB_FIRST, "GL_UNPACK_LSB_FIRST"},
  {&__pyx_n_GL_UNPACK_ROW_LENGTH, "GL_UNPACK_ROW_LENGTH"},
  {&__pyx_n_GL_UNPACK_SKIP_IMAGES, "GL_UNPACK_SKIP_IMAGES"},
  {&__pyx_n_GL_UNPACK_SKIP_PIXELS, "GL_UNPACK_SKIP_PIXELS"},
  {&__pyx_n_GL_UNPACK_SKIP_ROWS, "GL_UNPACK_SKIP_ROWS"},
  {&__pyx_n_GL_UNPACK_SWAP_BYTES, "GL_UNPACK_SWAP_BYTES"},
  {&__pyx_n_GL_UNSIGNED_BYTE, "GL_UNSIGNED_BYTE"},
  {&__pyx_n_GL_UNSIGNED_BYTE_2_3_3_REV, "GL_UNSIGNED_BYTE_2_3_3_REV"},
  {&__pyx_n_GL_UNSIGNED_BYTE_3_3_2, "GL_UNSIGNED_BYTE_3_3_2"},
  {&__pyx_n_GL_UNSIGNED_INT, "GL_UNSIGNED_INT"},
  {&__pyx_n_GL_UNSIGNED_INT_10_10_10_2, "GL_UNSIGNED_INT_10_10_10_2"},
  {&__pyx_n_GL_UNSIGNED_INT_2_10_10_10_REV, "GL_UNSIGNED_INT_2_10_10_10_REV"},
  {&__pyx_n_GL_UNSIGNED_INT_8_8_8_8, "GL_UNSIGNED_INT_8_8_8_8"},
  {&__pyx_n_GL_UNSIGNED_INT_8_8_8_8_REV, "GL_UNSIGNED_INT_8_8_8_8_REV"},
  {&__pyx_n_GL_UNSIGNED_SHORT, "GL_UNSIGNED_SHORT"},
  {&__pyx_n_GL_UNSIGNED_SHORT_1_5_5_5_REV, "GL_UNSIGNED_SHORT_1_5_5_5_REV"},
  {&__pyx_n_GL_UNSIGNED_SHORT_4_4_4_4, "GL_UNSIGNED_SHORT_4_4_4_4"},
  {&__pyx_n_GL_UNSIGNED_SHORT_4_4_4_4_REV, "GL_UNSIGNED_SHORT_4_4_4_4_REV"},
  {&__pyx_n_GL_UNSIGNED_SHORT_5_5_5_1, "GL_UNSIGNED_SHORT_5_5_5_1"},
  {&__pyx_n_GL_UNSIGNED_SHORT_5_6_5, "GL_UNSIGNED_SHORT_5_6_5"},
  {&__pyx_n_GL_UNSIGNED_SHORT_5_6_5_REV, "GL_UNSIGNED_SHORT_5_6_5_REV"},
  {&__pyx_n_GL_V2F, "GL_V2F"},
  {&__pyx_n_GL_V3F, "GL_V3F"},
  {&__pyx_n_GL_VENDOR, "GL_VENDOR"},
  {&__pyx_n_GL_VERSION, "GL_VERSION"},
  {&__pyx_n_GL_VERTEX_ARRAY, "GL_VERTEX_ARRAY"},
  {&__pyx_n_GL_VERTEX_ARRAY_POINTER, "GL_VERTEX_ARRAY_POINTER"},
  {&__pyx_n_GL_VERTEX_ARRAY_SIZE, "GL_VERTEX_ARRAY_SIZE"},
  {&__pyx_n_GL_VERTEX_ARRAY_STRIDE, "GL_VERTEX_ARRAY_STRIDE"},
  {&__pyx_n_GL_VERTEX_ARRAY_TYPE, "GL_VERTEX_ARRAY_TYPE"},
  {&__pyx_n_GL_VIEWPORT, "GL_VIEWPORT"},
  {&__pyx_n_GL_VIEWPORT_BIT, "GL_VIEWPORT_BIT"},
  {&__pyx_n_GL_XOR, "GL_XOR"},
  {&__pyx_n_GL_ZERO, "GL_ZERO"},
  {&__pyx_n_GL_ZOOM_X, "GL_ZOOM_X"},
  {&__pyx_n_GL_ZOOM_Y, "GL_ZOOM_Y"},
  {&__pyx_n_c_opengl, "c_opengl"},
  {&__pyx_n_glMultMatrix, "glMultMatrix"},
  {0, 0}
};

static struct PyMethodDef __pyx_methods[] = {
  {"glClearColor", (PyCFunction)__pyx_f_6opengl_glClearColor, METH_VARARGS|METH_KEYWORDS, 0},
  {"glPointSize", (PyCFunction)__pyx_f_6opengl_glPointSize, METH_VARARGS|METH_KEYWORDS, 0},
  {"glLineWidth", (PyCFunction)__pyx_f_6opengl_glLineWidth, METH_VARARGS|METH_KEYWORDS, 0},
  {"glEnable", (PyCFunction)__pyx_f_6opengl_glEnable, METH_VARARGS|METH_KEYWORDS, 0},
  {"glDisable", (PyCFunction)__pyx_f_6opengl_glDisable, METH_VARARGS|METH_KEYWORDS, 0},
  {"glLightModeli", (PyCFunction)__pyx_f_6opengl_glLightModeli, METH_VARARGS|METH_KEYWORDS, 0},
  {"glBegin", (PyCFunction)__pyx_f_6opengl_glBegin, METH_VARARGS|METH_KEYWORDS, 0},
  {"glEnd", (PyCFunction)__pyx_f_6opengl_glEnd, METH_VARARGS|METH_KEYWORDS, 0},
  {"glVertex2i", (PyCFunction)__pyx_f_6opengl_glVertex2i, METH_VARARGS|METH_KEYWORDS, 0},
  {"glVertex2f", (PyCFunction)__pyx_f_6opengl_glVertex2f, METH_VARARGS|METH_KEYWORDS, 0},
  {"glVertex3f", (PyCFunction)__pyx_f_6opengl_glVertex3f, METH_VARARGS|METH_KEYWORDS, 0},
  {"glNormal3f", (PyCFunction)__pyx_f_6opengl_glNormal3f, METH_VARARGS|METH_KEYWORDS, 0},
  {"glColor4f", (PyCFunction)__pyx_f_6opengl_glColor4f, METH_VARARGS|METH_KEYWORDS, 0},
  {"glTexCoord2f", (PyCFunction)__pyx_f_6opengl_glTexCoord2f, METH_VARARGS|METH_KEYWORDS, 0},
  {"glDeleteList", (PyCFunction)__pyx_f_6opengl_glDeleteList, METH_VARARGS|METH_KEYWORDS, 0},
  {"glGenList", (PyCFunction)__pyx_f_6opengl_glGenList, METH_VARARGS|METH_KEYWORDS, 0},
  {"glGenLists", (PyCFunction)__pyx_f_6opengl_glGenLists, METH_VARARGS|METH_KEYWORDS, 0},
  {"glNewList", (PyCFunction)__pyx_f_6opengl_glNewList, METH_VARARGS|METH_KEYWORDS, 0},
  {"glEndList", (PyCFunction)__pyx_f_6opengl_glEndList, METH_VARARGS|METH_KEYWORDS, 0},
  {"glCallList", (PyCFunction)__pyx_f_6opengl_glCallList, METH_VARARGS|METH_KEYWORDS, 0},
  {"glRotatef", (PyCFunction)__pyx_f_6opengl_glRotatef, METH_VARARGS|METH_KEYWORDS, 0},
  {"glPushMatrix", (PyCFunction)__pyx_f_6opengl_glPushMatrix, METH_VARARGS|METH_KEYWORDS, 0},
  {"glPopMatrix", (PyCFunction)__pyx_f_6opengl_glPopMatrix, METH_VARARGS|METH_KEYWORDS, 0},
  {"glTranslatef", (PyCFunction)__pyx_f_6opengl_glTranslatef, METH_VARARGS|METH_KEYWORDS, 0},
  {"glTranslated", (PyCFunction)__pyx_f_6opengl_glTranslated, METH_VARARGS|METH_KEYWORDS, 0},
  {"glScalef", (PyCFunction)__pyx_f_6opengl_glScalef, METH_VARARGS|METH_KEYWORDS, 0},
  {"glGenTextures", (PyCFunction)__pyx_f_6opengl_glGenTextures, METH_VARARGS|METH_KEYWORDS, 0},
  {"glBindTexture", (PyCFunction)__pyx_f_6opengl_glBindTexture, METH_VARARGS|METH_KEYWORDS, 0},
  {"glPixelStorei", (PyCFunction)__pyx_f_6opengl_glPixelStorei, METH_VARARGS|METH_KEYWORDS, 0},
  {"glLoadIdentity", (PyCFunction)__pyx_f_6opengl_glLoadIdentity, METH_VARARGS|METH_KEYWORDS, 0},
  {"glAccum", (PyCFunction)__pyx_f_6opengl_glAccum, METH_VARARGS|METH_KEYWORDS, 0},
  {"glMatrixMode", (PyCFunction)__pyx_f_6opengl_glMatrixMode, METH_VARARGS|METH_KEYWORDS, 0},
  {"glMultMatrix", (PyCFunction)__pyx_f_6opengl_glMultMatrix, METH_VARARGS|METH_KEYWORDS, 0},
  {"glTexParameteri", (PyCFunction)__pyx_f_6opengl_glTexParameteri, METH_VARARGS|METH_KEYWORDS, 0},
  {"glPolygonOffset", (PyCFunction)__pyx_f_6opengl_glPolygonOffset, METH_VARARGS|METH_KEYWORDS, 0},
  {"glOrtho", (PyCFunction)__pyx_f_6opengl_glOrtho, METH_VARARGS|METH_KEYWORDS, 0},
  {"glPushAttrib", (PyCFunction)__pyx_f_6opengl_glPushAttrib, METH_VARARGS|METH_KEYWORDS, 0},
  {"glPopAttrib", (PyCFunction)__pyx_f_6opengl_glPopAttrib, METH_VARARGS|METH_KEYWORDS, 0},
  {"glViewport", (PyCFunction)__pyx_f_6opengl_glViewport, METH_VARARGS|METH_KEYWORDS, 0},
  {0, 0, 0, 0}
};

static void __pyx_init_filenames(void); /*proto*/

PyMODINIT_FUNC initopengl(void); /*proto*/
PyMODINIT_FUNC initopengl(void) {
  PyObject *__pyx_1 = 0;
  int __pyx_2;
  __pyx_init_filenames();
  __pyx_m = Py_InitModule4("opengl", __pyx_methods, 0, 0, PYTHON_API_VERSION);
  if (!__pyx_m) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 21; goto __pyx_L1;};
  Py_INCREF(__pyx_m);
  __pyx_b = PyImport_AddModule("__builtin__");
  if (!__pyx_b) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 21; goto __pyx_L1;};
  if (PyObject_SetAttrString(__pyx_m, "__builtins__", __pyx_b) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 21; goto __pyx_L1;};
  if (__Pyx_InternStrings(__pyx_intern_tab) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 21; goto __pyx_L1;};

  /* "/home/jiba/src/soya/opengl.pyx":27 */
  __pyx_1 = PyInt_FromLong(GL_FALSE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 27; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FALSE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 27; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":28 */
  __pyx_1 = PyInt_FromLong(GL_TRUE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 28; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TRUE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 28; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":29 */
  __pyx_1 = PyInt_FromLong(GL_BYTE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 29; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_BYTE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 29; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":30 */
  __pyx_1 = PyInt_FromLong(GL_UNSIGNED_BYTE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 30; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNSIGNED_BYTE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 30; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":31 */
  __pyx_1 = PyInt_FromLong(GL_SHORT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 31; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SHORT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 31; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":32 */
  __pyx_1 = PyInt_FromLong(GL_UNSIGNED_SHORT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 32; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNSIGNED_SHORT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 32; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":33 */
  __pyx_1 = PyInt_FromLong(GL_INT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 33; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 33; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":34 */
  __pyx_1 = PyInt_FromLong(GL_UNSIGNED_INT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 34; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNSIGNED_INT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 34; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":35 */
  __pyx_1 = PyInt_FromLong(GL_FLOAT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 35; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FLOAT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 35; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":36 */
  __pyx_1 = PyInt_FromLong(GL_DOUBLE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 36; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DOUBLE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 36; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":37 */
  __pyx_1 = PyInt_FromLong(GL_2_BYTES); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 37; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_2_BYTES, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 37; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":38 */
  __pyx_1 = PyInt_FromLong(GL_3_BYTES); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 38; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_3_BYTES, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 38; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":39 */
  __pyx_1 = PyInt_FromLong(GL_4_BYTES); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 39; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_4_BYTES, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 39; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":40 */
  __pyx_1 = PyInt_FromLong(GL_POINTS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 40; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POINTS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 40; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":41 */
  __pyx_1 = PyInt_FromLong(GL_LINES); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 41; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINES, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 41; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":42 */
  __pyx_1 = PyInt_FromLong(GL_LINE_LOOP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 42; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINE_LOOP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 42; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":43 */
  __pyx_1 = PyInt_FromLong(GL_LINE_STRIP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 43; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINE_STRIP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 43; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":44 */
  __pyx_1 = PyInt_FromLong(GL_TRIANGLES); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 44; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TRIANGLES, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 44; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":45 */
  __pyx_1 = PyInt_FromLong(GL_TRIANGLE_STRIP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 45; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TRIANGLE_STRIP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 45; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":46 */
  __pyx_1 = PyInt_FromLong(GL_TRIANGLE_FAN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 46; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TRIANGLE_FAN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 46; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":47 */
  __pyx_1 = PyInt_FromLong(GL_QUADS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 47; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_QUADS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 47; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":48 */
  __pyx_1 = PyInt_FromLong(GL_QUAD_STRIP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 48; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_QUAD_STRIP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 48; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":49 */
  __pyx_1 = PyInt_FromLong(GL_POLYGON); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 49; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POLYGON, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 49; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":50 */
  __pyx_1 = PyInt_FromLong(GL_VERTEX_ARRAY); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 50; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_VERTEX_ARRAY, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 50; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":51 */
  __pyx_1 = PyInt_FromLong(GL_NORMAL_ARRAY); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 51; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NORMAL_ARRAY, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 51; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":52 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_ARRAY); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 52; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_ARRAY, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 52; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":53 */
  __pyx_1 = PyInt_FromLong(GL_INDEX_ARRAY); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 53; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INDEX_ARRAY, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 53; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":54 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_COORD_ARRAY); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 54; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_COORD_ARRAY, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 54; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":55 */
  __pyx_1 = PyInt_FromLong(GL_EDGE_FLAG_ARRAY); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 55; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_EDGE_FLAG_ARRAY, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 55; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":56 */
  __pyx_1 = PyInt_FromLong(GL_VERTEX_ARRAY_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 56; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_VERTEX_ARRAY_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 56; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":57 */
  __pyx_1 = PyInt_FromLong(GL_VERTEX_ARRAY_TYPE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 57; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_VERTEX_ARRAY_TYPE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 57; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":58 */
  __pyx_1 = PyInt_FromLong(GL_VERTEX_ARRAY_STRIDE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 58; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_VERTEX_ARRAY_STRIDE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 58; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":59 */
  __pyx_1 = PyInt_FromLong(GL_NORMAL_ARRAY_TYPE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 59; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NORMAL_ARRAY_TYPE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 59; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":60 */
  __pyx_1 = PyInt_FromLong(GL_NORMAL_ARRAY_STRIDE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 60; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NORMAL_ARRAY_STRIDE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 60; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":61 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_ARRAY_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 61; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_ARRAY_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 61; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":62 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_ARRAY_TYPE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 62; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_ARRAY_TYPE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 62; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":63 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_ARRAY_STRIDE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 63; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_ARRAY_STRIDE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 63; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":64 */
  __pyx_1 = PyInt_FromLong(GL_INDEX_ARRAY_TYPE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 64; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INDEX_ARRAY_TYPE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 64; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":65 */
  __pyx_1 = PyInt_FromLong(GL_INDEX_ARRAY_STRIDE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 65; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INDEX_ARRAY_STRIDE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 65; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":66 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_COORD_ARRAY_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 66; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_COORD_ARRAY_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 66; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":67 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_COORD_ARRAY_TYPE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 67; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_COORD_ARRAY_TYPE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 67; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":68 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_COORD_ARRAY_STRIDE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 68; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_COORD_ARRAY_STRIDE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 68; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":69 */
  __pyx_1 = PyInt_FromLong(GL_EDGE_FLAG_ARRAY_STRIDE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 69; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_EDGE_FLAG_ARRAY_STRIDE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 69; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":70 */
  __pyx_1 = PyInt_FromLong(GL_VERTEX_ARRAY_POINTER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 70; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_VERTEX_ARRAY_POINTER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 70; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":71 */
  __pyx_1 = PyInt_FromLong(GL_NORMAL_ARRAY_POINTER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 71; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NORMAL_ARRAY_POINTER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 71; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":72 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_ARRAY_POINTER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 72; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_ARRAY_POINTER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 72; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":73 */
  __pyx_1 = PyInt_FromLong(GL_INDEX_ARRAY_POINTER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 73; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INDEX_ARRAY_POINTER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 73; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":74 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_COORD_ARRAY_POINTER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 74; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_COORD_ARRAY_POINTER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 74; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":75 */
  __pyx_1 = PyInt_FromLong(GL_EDGE_FLAG_ARRAY_POINTER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 75; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_EDGE_FLAG_ARRAY_POINTER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 75; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":76 */
  __pyx_1 = PyInt_FromLong(GL_V2F); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 76; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_V2F, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 76; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":77 */
  __pyx_1 = PyInt_FromLong(GL_V3F); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 77; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_V3F, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 77; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":78 */
  __pyx_1 = PyInt_FromLong(GL_C4UB_V2F); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 78; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_C4UB_V2F, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 78; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":79 */
  __pyx_1 = PyInt_FromLong(GL_C4UB_V3F); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 79; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_C4UB_V3F, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 79; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":80 */
  __pyx_1 = PyInt_FromLong(GL_C3F_V3F); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 80; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_C3F_V3F, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 80; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":81 */
  __pyx_1 = PyInt_FromLong(GL_N3F_V3F); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 81; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_N3F_V3F, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 81; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":82 */
  __pyx_1 = PyInt_FromLong(GL_C4F_N3F_V3F); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 82; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_C4F_N3F_V3F, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 82; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":83 */
  __pyx_1 = PyInt_FromLong(GL_T2F_V3F); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 83; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_T2F_V3F, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 83; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":84 */
  __pyx_1 = PyInt_FromLong(GL_T4F_V4F); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 84; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_T4F_V4F, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 84; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":85 */
  __pyx_1 = PyInt_FromLong(GL_T2F_C4UB_V3F); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 85; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_T2F_C4UB_V3F, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 85; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":86 */
  __pyx_1 = PyInt_FromLong(GL_T2F_C3F_V3F); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 86; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_T2F_C3F_V3F, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 86; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":87 */
  __pyx_1 = PyInt_FromLong(GL_T2F_N3F_V3F); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 87; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_T2F_N3F_V3F, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 87; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":88 */
  __pyx_1 = PyInt_FromLong(GL_T2F_C4F_N3F_V3F); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 88; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_T2F_C4F_N3F_V3F, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 88; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":89 */
  __pyx_1 = PyInt_FromLong(GL_T4F_C4F_N3F_V4F); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 89; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_T4F_C4F_N3F_V4F, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 89; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":90 */
  __pyx_1 = PyInt_FromLong(GL_MATRIX_MODE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 90; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MATRIX_MODE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 90; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":91 */
  __pyx_1 = PyInt_FromLong(GL_MODELVIEW); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 91; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MODELVIEW, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 91; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":92 */
  __pyx_1 = PyInt_FromLong(GL_PROJECTION); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 92; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PROJECTION, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 92; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":93 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 93; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 93; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":94 */
  __pyx_1 = PyInt_FromLong(GL_POINT_SMOOTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 94; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POINT_SMOOTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 94; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":95 */
  __pyx_1 = PyInt_FromLong(GL_POINT_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 95; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POINT_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 95; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":96 */
  __pyx_1 = PyInt_FromLong(GL_POINT_SIZE_GRANULARITY); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 96; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POINT_SIZE_GRANULARITY, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 96; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":97 */
  __pyx_1 = PyInt_FromLong(GL_POINT_SIZE_RANGE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 97; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POINT_SIZE_RANGE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 97; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":98 */
  __pyx_1 = PyInt_FromLong(GL_LINE_SMOOTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 98; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINE_SMOOTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 98; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":99 */
  __pyx_1 = PyInt_FromLong(GL_LINE_STIPPLE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 99; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINE_STIPPLE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 99; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":100 */
  __pyx_1 = PyInt_FromLong(GL_LINE_STIPPLE_PATTERN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 100; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINE_STIPPLE_PATTERN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 100; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":101 */
  __pyx_1 = PyInt_FromLong(GL_LINE_STIPPLE_REPEAT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 101; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINE_STIPPLE_REPEAT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 101; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":102 */
  __pyx_1 = PyInt_FromLong(GL_LINE_WIDTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 102; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINE_WIDTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 102; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":103 */
  __pyx_1 = PyInt_FromLong(GL_LINE_WIDTH_GRANULARITY); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 103; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINE_WIDTH_GRANULARITY, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 103; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":104 */
  __pyx_1 = PyInt_FromLong(GL_LINE_WIDTH_RANGE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 104; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINE_WIDTH_RANGE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 104; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":105 */
  __pyx_1 = PyInt_FromLong(GL_POINT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 105; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POINT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 105; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":106 */
  __pyx_1 = PyInt_FromLong(GL_LINE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 106; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 106; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":107 */
  __pyx_1 = PyInt_FromLong(GL_FILL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 107; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FILL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 107; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":108 */
  __pyx_1 = PyInt_FromLong(GL_CW); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 108; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CW, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 108; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":109 */
  __pyx_1 = PyInt_FromLong(GL_CCW); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 109; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CCW, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 109; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":110 */
  __pyx_1 = PyInt_FromLong(GL_FRONT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 110; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FRONT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 110; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":111 */
  __pyx_1 = PyInt_FromLong(GL_BACK); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 111; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_BACK, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 111; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":112 */
  __pyx_1 = PyInt_FromLong(GL_POLYGON_MODE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 112; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POLYGON_MODE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 112; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":113 */
  __pyx_1 = PyInt_FromLong(GL_POLYGON_SMOOTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 113; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POLYGON_SMOOTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 113; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":114 */
  __pyx_1 = PyInt_FromLong(GL_POLYGON_STIPPLE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 114; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POLYGON_STIPPLE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 114; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":115 */
  __pyx_1 = PyInt_FromLong(GL_EDGE_FLAG); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 115; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_EDGE_FLAG, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 115; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":116 */
  __pyx_1 = PyInt_FromLong(GL_CULL_FACE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 116; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CULL_FACE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 116; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":117 */
  __pyx_1 = PyInt_FromLong(GL_CULL_FACE_MODE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 117; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CULL_FACE_MODE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 117; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":118 */
  __pyx_1 = PyInt_FromLong(GL_FRONT_FACE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 118; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FRONT_FACE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 118; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":119 */
  __pyx_1 = PyInt_FromLong(GL_POLYGON_OFFSET_FACTOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 119; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POLYGON_OFFSET_FACTOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 119; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":120 */
  __pyx_1 = PyInt_FromLong(GL_POLYGON_OFFSET_UNITS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 120; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POLYGON_OFFSET_UNITS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 120; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":121 */
  __pyx_1 = PyInt_FromLong(GL_POLYGON_OFFSET_POINT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 121; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POLYGON_OFFSET_POINT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 121; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":122 */
  __pyx_1 = PyInt_FromLong(GL_POLYGON_OFFSET_LINE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 122; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POLYGON_OFFSET_LINE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 122; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":123 */
  __pyx_1 = PyInt_FromLong(GL_POLYGON_OFFSET_FILL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 123; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POLYGON_OFFSET_FILL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 123; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":124 */
  __pyx_1 = PyInt_FromLong(GL_COMPILE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 124; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COMPILE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 124; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":125 */
  __pyx_1 = PyInt_FromLong(GL_COMPILE_AND_EXECUTE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 125; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COMPILE_AND_EXECUTE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 125; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":126 */
  __pyx_1 = PyInt_FromLong(GL_LIST_BASE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 126; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LIST_BASE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 126; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":127 */
  __pyx_1 = PyInt_FromLong(GL_LIST_INDEX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 127; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LIST_INDEX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 127; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":128 */
  __pyx_1 = PyInt_FromLong(GL_LIST_MODE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 128; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LIST_MODE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 128; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":129 */
  __pyx_1 = PyInt_FromLong(GL_NEVER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 129; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NEVER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 129; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":130 */
  __pyx_1 = PyInt_FromLong(GL_LESS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 130; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LESS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 130; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":131 */
  __pyx_1 = PyInt_FromLong(GL_EQUAL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 131; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_EQUAL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 131; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":132 */
  __pyx_1 = PyInt_FromLong(GL_LEQUAL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 132; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LEQUAL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 132; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":133 */
  __pyx_1 = PyInt_FromLong(GL_GREATER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 133; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_GREATER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 133; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":134 */
  __pyx_1 = PyInt_FromLong(GL_NOTEQUAL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 134; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NOTEQUAL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 134; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":135 */
  __pyx_1 = PyInt_FromLong(GL_GEQUAL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 135; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_GEQUAL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 135; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":136 */
  __pyx_1 = PyInt_FromLong(GL_ALWAYS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 136; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ALWAYS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 136; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":137 */
  __pyx_1 = PyInt_FromLong(GL_DEPTH_TEST); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 137; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DEPTH_TEST, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 137; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":138 */
  __pyx_1 = PyInt_FromLong(GL_DEPTH_BITS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 138; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DEPTH_BITS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 138; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":139 */
  __pyx_1 = PyInt_FromLong(GL_DEPTH_CLEAR_VALUE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 139; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DEPTH_CLEAR_VALUE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 139; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":140 */
  __pyx_1 = PyInt_FromLong(GL_DEPTH_FUNC); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 140; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DEPTH_FUNC, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 140; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":141 */
  __pyx_1 = PyInt_FromLong(GL_DEPTH_RANGE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 141; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DEPTH_RANGE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 141; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":142 */
  __pyx_1 = PyInt_FromLong(GL_DEPTH_WRITEMASK); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 142; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DEPTH_WRITEMASK, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 142; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":143 */
  __pyx_1 = PyInt_FromLong(GL_DEPTH_COMPONENT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 143; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DEPTH_COMPONENT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 143; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":144 */
  __pyx_1 = PyInt_FromLong(GL_LIGHTING); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 144; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LIGHTING, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 144; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":145 */
  __pyx_1 = PyInt_FromLong(GL_LIGHT0); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 145; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LIGHT0, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 145; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":146 */
  __pyx_1 = PyInt_FromLong(GL_LIGHT1); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 146; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LIGHT1, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 146; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":147 */
  __pyx_1 = PyInt_FromLong(GL_LIGHT2); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 147; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LIGHT2, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 147; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":148 */
  __pyx_1 = PyInt_FromLong(GL_LIGHT3); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 148; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LIGHT3, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 148; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":149 */
  __pyx_1 = PyInt_FromLong(GL_LIGHT4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 149; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LIGHT4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 149; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":150 */
  __pyx_1 = PyInt_FromLong(GL_LIGHT5); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 150; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LIGHT5, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 150; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":151 */
  __pyx_1 = PyInt_FromLong(GL_LIGHT6); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 151; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LIGHT6, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 151; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":152 */
  __pyx_1 = PyInt_FromLong(GL_LIGHT7); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 152; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LIGHT7, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 152; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":153 */
  __pyx_1 = PyInt_FromLong(GL_SPOT_EXPONENT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 153; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SPOT_EXPONENT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 153; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":154 */
  __pyx_1 = PyInt_FromLong(GL_SPOT_CUTOFF); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 154; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SPOT_CUTOFF, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 154; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":155 */
  __pyx_1 = PyInt_FromLong(GL_CONSTANT_ATTENUATION); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 155; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CONSTANT_ATTENUATION, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 155; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":156 */
  __pyx_1 = PyInt_FromLong(GL_LINEAR_ATTENUATION); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 156; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINEAR_ATTENUATION, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 156; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":157 */
  __pyx_1 = PyInt_FromLong(GL_QUADRATIC_ATTENUATION); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 157; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_QUADRATIC_ATTENUATION, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 157; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":158 */
  __pyx_1 = PyInt_FromLong(GL_AMBIENT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 158; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_AMBIENT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 158; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":159 */
  __pyx_1 = PyInt_FromLong(GL_DIFFUSE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 159; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DIFFUSE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 159; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":160 */
  __pyx_1 = PyInt_FromLong(GL_SPECULAR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 160; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SPECULAR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 160; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":161 */
  __pyx_1 = PyInt_FromLong(GL_SHININESS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 161; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SHININESS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 161; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":162 */
  __pyx_1 = PyInt_FromLong(GL_EMISSION); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 162; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_EMISSION, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 162; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":163 */
  __pyx_1 = PyInt_FromLong(GL_POSITION); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 163; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POSITION, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 163; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":164 */
  __pyx_1 = PyInt_FromLong(GL_SPOT_DIRECTION); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 164; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SPOT_DIRECTION, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 164; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":165 */
  __pyx_1 = PyInt_FromLong(GL_AMBIENT_AND_DIFFUSE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 165; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_AMBIENT_AND_DIFFUSE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 165; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":166 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_INDEXES); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 166; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_INDEXES, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 166; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":167 */
  __pyx_1 = PyInt_FromLong(GL_LIGHT_MODEL_TWO_SIDE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 167; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LIGHT_MODEL_TWO_SIDE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 167; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":168 */
  __pyx_1 = PyInt_FromLong(GL_LIGHT_MODEL_LOCAL_VIEWER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 168; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LIGHT_MODEL_LOCAL_VIEWER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 168; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":169 */
  __pyx_1 = PyInt_FromLong(GL_LIGHT_MODEL_AMBIENT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 169; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LIGHT_MODEL_AMBIENT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 169; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":170 */
  __pyx_1 = PyInt_FromLong(GL_FRONT_AND_BACK); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 170; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FRONT_AND_BACK, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 170; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":171 */
  __pyx_1 = PyInt_FromLong(GL_SHADE_MODEL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 171; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SHADE_MODEL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 171; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":172 */
  __pyx_1 = PyInt_FromLong(GL_FLAT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 172; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FLAT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 172; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":173 */
  __pyx_1 = PyInt_FromLong(GL_SMOOTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 173; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SMOOTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 173; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":174 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_MATERIAL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 174; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_MATERIAL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 174; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":175 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_MATERIAL_FACE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 175; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_MATERIAL_FACE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 175; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":176 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_MATERIAL_PARAMETER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 176; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_MATERIAL_PARAMETER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 176; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":177 */
  __pyx_1 = PyInt_FromLong(GL_NORMALIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 177; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NORMALIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 177; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":178 */
  __pyx_1 = PyInt_FromLong(GL_CLIP_PLANE0); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 178; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CLIP_PLANE0, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 178; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":179 */
  __pyx_1 = PyInt_FromLong(GL_CLIP_PLANE1); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 179; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CLIP_PLANE1, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 179; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":180 */
  __pyx_1 = PyInt_FromLong(GL_CLIP_PLANE2); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 180; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CLIP_PLANE2, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 180; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":181 */
  __pyx_1 = PyInt_FromLong(GL_CLIP_PLANE3); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 181; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CLIP_PLANE3, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 181; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":182 */
  __pyx_1 = PyInt_FromLong(GL_CLIP_PLANE4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 182; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CLIP_PLANE4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 182; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":183 */
  __pyx_1 = PyInt_FromLong(GL_CLIP_PLANE5); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 183; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CLIP_PLANE5, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 183; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":184 */
  __pyx_1 = PyInt_FromLong(GL_ACCUM_RED_BITS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 184; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ACCUM_RED_BITS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 184; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":185 */
  __pyx_1 = PyInt_FromLong(GL_ACCUM_GREEN_BITS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 185; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ACCUM_GREEN_BITS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 185; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":186 */
  __pyx_1 = PyInt_FromLong(GL_ACCUM_BLUE_BITS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 186; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ACCUM_BLUE_BITS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 186; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":187 */
  __pyx_1 = PyInt_FromLong(GL_ACCUM_ALPHA_BITS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 187; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ACCUM_ALPHA_BITS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 187; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":188 */
  __pyx_1 = PyInt_FromLong(GL_ACCUM_CLEAR_VALUE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 188; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ACCUM_CLEAR_VALUE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 188; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":189 */
  __pyx_1 = PyInt_FromLong(GL_ACCUM); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 189; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ACCUM, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 189; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":190 */
  __pyx_1 = PyInt_FromLong(GL_ADD); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 190; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ADD, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 190; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":191 */
  __pyx_1 = PyInt_FromLong(GL_LOAD); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 191; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LOAD, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 191; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":192 */
  __pyx_1 = PyInt_FromLong(GL_MULT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 192; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MULT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 192; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":193 */
  __pyx_1 = PyInt_FromLong(GL_RETURN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 193; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RETURN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 193; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":194 */
  __pyx_1 = PyInt_FromLong(GL_ALPHA_TEST); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 194; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ALPHA_TEST, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 194; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":195 */
  __pyx_1 = PyInt_FromLong(GL_ALPHA_TEST_REF); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 195; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ALPHA_TEST_REF, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 195; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":196 */
  __pyx_1 = PyInt_FromLong(GL_ALPHA_TEST_FUNC); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 196; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ALPHA_TEST_FUNC, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 196; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":197 */
  __pyx_1 = PyInt_FromLong(GL_BLEND); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 197; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_BLEND, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 197; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":198 */
  __pyx_1 = PyInt_FromLong(GL_BLEND_SRC); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 198; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_BLEND_SRC, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 198; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":199 */
  __pyx_1 = PyInt_FromLong(GL_BLEND_DST); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 199; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_BLEND_DST, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 199; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":200 */
  __pyx_1 = PyInt_FromLong(GL_ZERO); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 200; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ZERO, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 200; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":201 */
  __pyx_1 = PyInt_FromLong(GL_ONE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 201; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ONE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 201; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":202 */
  __pyx_1 = PyInt_FromLong(GL_SRC_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 202; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SRC_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 202; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":203 */
  __pyx_1 = PyInt_FromLong(GL_ONE_MINUS_SRC_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 203; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ONE_MINUS_SRC_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 203; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":204 */
  __pyx_1 = PyInt_FromLong(GL_SRC_ALPHA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 204; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SRC_ALPHA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 204; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":205 */
  __pyx_1 = PyInt_FromLong(GL_ONE_MINUS_SRC_ALPHA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 205; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ONE_MINUS_SRC_ALPHA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 205; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":206 */
  __pyx_1 = PyInt_FromLong(GL_DST_ALPHA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 206; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DST_ALPHA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 206; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":207 */
  __pyx_1 = PyInt_FromLong(GL_ONE_MINUS_DST_ALPHA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 207; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ONE_MINUS_DST_ALPHA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 207; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":208 */
  __pyx_1 = PyInt_FromLong(GL_DST_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 208; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DST_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 208; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":209 */
  __pyx_1 = PyInt_FromLong(GL_ONE_MINUS_DST_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 209; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ONE_MINUS_DST_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 209; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":210 */
  __pyx_1 = PyInt_FromLong(GL_SRC_ALPHA_SATURATE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 210; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SRC_ALPHA_SATURATE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 210; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":211 */
  __pyx_1 = PyInt_FromLong(GL_CONSTANT_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 211; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CONSTANT_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 211; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":212 */
  __pyx_1 = PyInt_FromLong(GL_ONE_MINUS_CONSTANT_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 212; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ONE_MINUS_CONSTANT_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 212; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":213 */
  __pyx_1 = PyInt_FromLong(GL_CONSTANT_ALPHA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 213; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CONSTANT_ALPHA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 213; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":214 */
  __pyx_1 = PyInt_FromLong(GL_ONE_MINUS_CONSTANT_ALPHA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 214; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ONE_MINUS_CONSTANT_ALPHA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 214; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":215 */
  __pyx_1 = PyInt_FromLong(GL_FEEDBACK); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 215; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FEEDBACK, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 215; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":216 */
  __pyx_1 = PyInt_FromLong(GL_RENDER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 216; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RENDER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 216; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":217 */
  __pyx_1 = PyInt_FromLong(GL_SELECT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 217; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SELECT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 217; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":218 */
  __pyx_1 = PyInt_FromLong(GL_2D); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 218; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_2D, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 218; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":219 */
  __pyx_1 = PyInt_FromLong(GL_3D); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 219; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_3D, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 219; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":220 */
  __pyx_1 = PyInt_FromLong(GL_3D_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 220; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_3D_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 220; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":221 */
  __pyx_1 = PyInt_FromLong(GL_3D_COLOR_TEXTURE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 221; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_3D_COLOR_TEXTURE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 221; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":222 */
  __pyx_1 = PyInt_FromLong(GL_4D_COLOR_TEXTURE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 222; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_4D_COLOR_TEXTURE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 222; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":223 */
  __pyx_1 = PyInt_FromLong(GL_POINT_TOKEN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 223; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POINT_TOKEN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 223; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":224 */
  __pyx_1 = PyInt_FromLong(GL_LINE_TOKEN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 224; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINE_TOKEN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 224; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":225 */
  __pyx_1 = PyInt_FromLong(GL_LINE_RESET_TOKEN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 225; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINE_RESET_TOKEN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 225; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":226 */
  __pyx_1 = PyInt_FromLong(GL_POLYGON_TOKEN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 226; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POLYGON_TOKEN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 226; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":227 */
  __pyx_1 = PyInt_FromLong(GL_BITMAP_TOKEN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 227; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_BITMAP_TOKEN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 227; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":228 */
  __pyx_1 = PyInt_FromLong(GL_DRAW_PIXEL_TOKEN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 228; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DRAW_PIXEL_TOKEN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 228; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":229 */
  __pyx_1 = PyInt_FromLong(GL_COPY_PIXEL_TOKEN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 229; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COPY_PIXEL_TOKEN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 229; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":230 */
  __pyx_1 = PyInt_FromLong(GL_PASS_THROUGH_TOKEN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 230; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PASS_THROUGH_TOKEN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 230; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":231 */
  __pyx_1 = PyInt_FromLong(GL_FEEDBACK_BUFFER_POINTER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 231; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FEEDBACK_BUFFER_POINTER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 231; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":232 */
  __pyx_1 = PyInt_FromLong(GL_FEEDBACK_BUFFER_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 232; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FEEDBACK_BUFFER_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 232; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":233 */
  __pyx_1 = PyInt_FromLong(GL_FEEDBACK_BUFFER_TYPE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 233; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FEEDBACK_BUFFER_TYPE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 233; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":234 */
  __pyx_1 = PyInt_FromLong(GL_SELECTION_BUFFER_POINTER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 234; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SELECTION_BUFFER_POINTER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 234; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":235 */
  __pyx_1 = PyInt_FromLong(GL_SELECTION_BUFFER_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 235; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SELECTION_BUFFER_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 235; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":236 */
  __pyx_1 = PyInt_FromLong(GL_FOG); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 236; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FOG, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 236; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":237 */
  __pyx_1 = PyInt_FromLong(GL_FOG_MODE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 237; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FOG_MODE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 237; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":238 */
  __pyx_1 = PyInt_FromLong(GL_FOG_DENSITY); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 238; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FOG_DENSITY, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 238; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":239 */
  __pyx_1 = PyInt_FromLong(GL_FOG_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 239; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FOG_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 239; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":240 */
  __pyx_1 = PyInt_FromLong(GL_FOG_INDEX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 240; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FOG_INDEX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 240; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":241 */
  __pyx_1 = PyInt_FromLong(GL_FOG_START); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 241; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FOG_START, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 241; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":242 */
  __pyx_1 = PyInt_FromLong(GL_FOG_END); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 242; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FOG_END, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 242; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":243 */
  __pyx_1 = PyInt_FromLong(GL_LINEAR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 243; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINEAR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 243; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":244 */
  __pyx_1 = PyInt_FromLong(GL_EXP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 244; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_EXP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 244; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":245 */
  __pyx_1 = PyInt_FromLong(GL_EXP2); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 245; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_EXP2, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 245; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":246 */
  __pyx_1 = PyInt_FromLong(GL_LOGIC_OP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 246; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LOGIC_OP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 246; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":247 */
  __pyx_1 = PyInt_FromLong(GL_INDEX_LOGIC_OP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 247; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INDEX_LOGIC_OP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 247; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":248 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_LOGIC_OP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 248; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_LOGIC_OP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 248; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":249 */
  __pyx_1 = PyInt_FromLong(GL_LOGIC_OP_MODE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 249; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LOGIC_OP_MODE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 249; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":250 */
  __pyx_1 = PyInt_FromLong(GL_CLEAR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 250; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CLEAR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 250; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":251 */
  __pyx_1 = PyInt_FromLong(GL_SET); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 251; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SET, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 251; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":252 */
  __pyx_1 = PyInt_FromLong(GL_COPY); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 252; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COPY, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 252; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":253 */
  __pyx_1 = PyInt_FromLong(GL_COPY_INVERTED); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 253; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COPY_INVERTED, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 253; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":254 */
  __pyx_1 = PyInt_FromLong(GL_NOOP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 254; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NOOP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 254; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":255 */
  __pyx_1 = PyInt_FromLong(GL_INVERT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 255; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INVERT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 255; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":256 */
  __pyx_1 = PyInt_FromLong(GL_AND); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 256; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_AND, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 256; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":257 */
  __pyx_1 = PyInt_FromLong(GL_NAND); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 257; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NAND, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 257; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":258 */
  __pyx_1 = PyInt_FromLong(GL_OR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 258; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_OR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 258; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":259 */
  __pyx_1 = PyInt_FromLong(GL_NOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 259; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 259; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":260 */
  __pyx_1 = PyInt_FromLong(GL_XOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 260; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_XOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 260; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":261 */
  __pyx_1 = PyInt_FromLong(GL_EQUIV); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 261; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_EQUIV, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 261; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":262 */
  __pyx_1 = PyInt_FromLong(GL_AND_REVERSE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 262; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_AND_REVERSE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 262; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":263 */
  __pyx_1 = PyInt_FromLong(GL_AND_INVERTED); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 263; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_AND_INVERTED, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 263; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":264 */
  __pyx_1 = PyInt_FromLong(GL_OR_REVERSE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 264; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_OR_REVERSE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 264; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":265 */
  __pyx_1 = PyInt_FromLong(GL_OR_INVERTED); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 265; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_OR_INVERTED, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 265; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":266 */
  __pyx_1 = PyInt_FromLong(GL_STENCIL_TEST); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 266; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_STENCIL_TEST, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 266; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":267 */
  __pyx_1 = PyInt_FromLong(GL_STENCIL_WRITEMASK); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 267; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_STENCIL_WRITEMASK, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 267; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":268 */
  __pyx_1 = PyInt_FromLong(GL_STENCIL_BITS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 268; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_STENCIL_BITS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 268; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":269 */
  __pyx_1 = PyInt_FromLong(GL_STENCIL_FUNC); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 269; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_STENCIL_FUNC, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 269; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":270 */
  __pyx_1 = PyInt_FromLong(GL_STENCIL_VALUE_MASK); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 270; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_STENCIL_VALUE_MASK, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 270; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":271 */
  __pyx_1 = PyInt_FromLong(GL_STENCIL_REF); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 271; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_STENCIL_REF, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 271; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":272 */
  __pyx_1 = PyInt_FromLong(GL_STENCIL_FAIL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 272; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_STENCIL_FAIL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 272; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":273 */
  __pyx_1 = PyInt_FromLong(GL_STENCIL_PASS_DEPTH_PASS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 273; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_STENCIL_PASS_DEPTH_PASS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 273; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":274 */
  __pyx_1 = PyInt_FromLong(GL_STENCIL_PASS_DEPTH_FAIL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 274; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_STENCIL_PASS_DEPTH_FAIL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 274; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":275 */
  __pyx_1 = PyInt_FromLong(GL_STENCIL_CLEAR_VALUE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 275; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_STENCIL_CLEAR_VALUE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 275; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":276 */
  __pyx_1 = PyInt_FromLong(GL_STENCIL_INDEX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 276; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_STENCIL_INDEX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 276; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":277 */
  __pyx_1 = PyInt_FromLong(GL_KEEP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 277; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_KEEP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 277; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":278 */
  __pyx_1 = PyInt_FromLong(GL_REPLACE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 278; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_REPLACE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 278; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":279 */
  __pyx_1 = PyInt_FromLong(GL_INCR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 279; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INCR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 279; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":280 */
  __pyx_1 = PyInt_FromLong(GL_DECR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 280; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DECR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 280; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":281 */
  __pyx_1 = PyInt_FromLong(GL_NONE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 281; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NONE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 281; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":282 */
  __pyx_1 = PyInt_FromLong(GL_LEFT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 282; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LEFT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 282; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":283 */
  __pyx_1 = PyInt_FromLong(GL_RIGHT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 283; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RIGHT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 283; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":284 */
  __pyx_1 = PyInt_FromLong(GL_FRONT_LEFT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 284; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FRONT_LEFT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 284; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":285 */
  __pyx_1 = PyInt_FromLong(GL_FRONT_RIGHT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 285; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FRONT_RIGHT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 285; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":286 */
  __pyx_1 = PyInt_FromLong(GL_BACK_LEFT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 286; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_BACK_LEFT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 286; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":287 */
  __pyx_1 = PyInt_FromLong(GL_BACK_RIGHT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 287; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_BACK_RIGHT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 287; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":288 */
  __pyx_1 = PyInt_FromLong(GL_AUX0); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 288; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_AUX0, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 288; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":289 */
  __pyx_1 = PyInt_FromLong(GL_AUX1); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 289; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_AUX1, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 289; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":290 */
  __pyx_1 = PyInt_FromLong(GL_AUX2); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 290; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_AUX2, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 290; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":291 */
  __pyx_1 = PyInt_FromLong(GL_AUX3); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 291; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_AUX3, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 291; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":292 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_INDEX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 292; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_INDEX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 292; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":293 */
  __pyx_1 = PyInt_FromLong(GL_RED); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 293; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RED, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 293; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":294 */
  __pyx_1 = PyInt_FromLong(GL_GREEN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 294; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_GREEN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 294; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":295 */
  __pyx_1 = PyInt_FromLong(GL_BLUE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 295; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_BLUE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 295; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":296 */
  __pyx_1 = PyInt_FromLong(GL_ALPHA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 296; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ALPHA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 296; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":297 */
  __pyx_1 = PyInt_FromLong(GL_LUMINANCE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 297; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LUMINANCE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 297; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":298 */
  __pyx_1 = PyInt_FromLong(GL_LUMINANCE_ALPHA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 298; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LUMINANCE_ALPHA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 298; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":299 */
  __pyx_1 = PyInt_FromLong(GL_ALPHA_BITS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 299; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ALPHA_BITS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 299; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":300 */
  __pyx_1 = PyInt_FromLong(GL_RED_BITS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 300; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RED_BITS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 300; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":301 */
  __pyx_1 = PyInt_FromLong(GL_GREEN_BITS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 301; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_GREEN_BITS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 301; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":302 */
  __pyx_1 = PyInt_FromLong(GL_BLUE_BITS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 302; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_BLUE_BITS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 302; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":303 */
  __pyx_1 = PyInt_FromLong(GL_INDEX_BITS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 303; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INDEX_BITS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 303; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":304 */
  __pyx_1 = PyInt_FromLong(GL_SUBPIXEL_BITS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 304; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SUBPIXEL_BITS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 304; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":305 */
  __pyx_1 = PyInt_FromLong(GL_AUX_BUFFERS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 305; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_AUX_BUFFERS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 305; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":306 */
  __pyx_1 = PyInt_FromLong(GL_READ_BUFFER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 306; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_READ_BUFFER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 306; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":307 */
  __pyx_1 = PyInt_FromLong(GL_DRAW_BUFFER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 307; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DRAW_BUFFER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 307; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":308 */
  __pyx_1 = PyInt_FromLong(GL_DOUBLEBUFFER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 308; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DOUBLEBUFFER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 308; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":309 */
  __pyx_1 = PyInt_FromLong(GL_STEREO); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 309; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_STEREO, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 309; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":310 */
  __pyx_1 = PyInt_FromLong(GL_BITMAP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 310; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_BITMAP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 310; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":311 */
  __pyx_1 = PyInt_FromLong(GL_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 311; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 311; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":312 */
  __pyx_1 = PyInt_FromLong(GL_DEPTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 312; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DEPTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 312; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":313 */
  __pyx_1 = PyInt_FromLong(GL_STENCIL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 313; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_STENCIL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 313; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":314 */
  __pyx_1 = PyInt_FromLong(GL_DITHER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 314; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DITHER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 314; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":315 */
  __pyx_1 = PyInt_FromLong(GL_RGB); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 315; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RGB, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 315; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":316 */
  __pyx_1 = PyInt_FromLong(GL_RGBA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 316; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RGBA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 316; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":317 */
  __pyx_1 = PyInt_FromLong(GL_MAX_LIST_NESTING); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 317; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_LIST_NESTING, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 317; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":318 */
  __pyx_1 = PyInt_FromLong(GL_MAX_ATTRIB_STACK_DEPTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 318; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_ATTRIB_STACK_DEPTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 318; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":319 */
  __pyx_1 = PyInt_FromLong(GL_MAX_MODELVIEW_STACK_DEPTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 319; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_MODELVIEW_STACK_DEPTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 319; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":320 */
  __pyx_1 = PyInt_FromLong(GL_MAX_NAME_STACK_DEPTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 320; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_NAME_STACK_DEPTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 320; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":321 */
  __pyx_1 = PyInt_FromLong(GL_MAX_PROJECTION_STACK_DEPTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 321; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_PROJECTION_STACK_DEPTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 321; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":322 */
  __pyx_1 = PyInt_FromLong(GL_MAX_TEXTURE_STACK_DEPTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 322; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_TEXTURE_STACK_DEPTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 322; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":323 */
  __pyx_1 = PyInt_FromLong(GL_MAX_EVAL_ORDER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 323; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_EVAL_ORDER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 323; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":324 */
  __pyx_1 = PyInt_FromLong(GL_MAX_LIGHTS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 324; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_LIGHTS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 324; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":325 */
  __pyx_1 = PyInt_FromLong(GL_MAX_CLIP_PLANES); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 325; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_CLIP_PLANES, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 325; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":326 */
  __pyx_1 = PyInt_FromLong(GL_MAX_TEXTURE_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 326; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_TEXTURE_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 326; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":327 */
  __pyx_1 = PyInt_FromLong(GL_MAX_PIXEL_MAP_TABLE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 327; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_PIXEL_MAP_TABLE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 327; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":328 */
  __pyx_1 = PyInt_FromLong(GL_MAX_VIEWPORT_DIMS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 328; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_VIEWPORT_DIMS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 328; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":329 */
  __pyx_1 = PyInt_FromLong(GL_MAX_CLIENT_ATTRIB_STACK_DEPTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 329; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_CLIENT_ATTRIB_STACK_DEPTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 329; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":330 */
  __pyx_1 = PyInt_FromLong(GL_ATTRIB_STACK_DEPTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 330; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ATTRIB_STACK_DEPTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 330; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":331 */
  __pyx_1 = PyInt_FromLong(GL_CLIENT_ATTRIB_STACK_DEPTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 331; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CLIENT_ATTRIB_STACK_DEPTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 331; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":332 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_CLEAR_VALUE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 332; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_CLEAR_VALUE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 332; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":333 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_WRITEMASK); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 333; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_WRITEMASK, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 333; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":334 */
  __pyx_1 = PyInt_FromLong(GL_CURRENT_INDEX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 334; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CURRENT_INDEX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 334; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":335 */
  __pyx_1 = PyInt_FromLong(GL_CURRENT_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 335; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CURRENT_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 335; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":336 */
  __pyx_1 = PyInt_FromLong(GL_CURRENT_NORMAL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 336; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CURRENT_NORMAL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 336; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":337 */
  __pyx_1 = PyInt_FromLong(GL_CURRENT_RASTER_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 337; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CURRENT_RASTER_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 337; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":338 */
  __pyx_1 = PyInt_FromLong(GL_CURRENT_RASTER_DISTANCE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 338; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CURRENT_RASTER_DISTANCE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 338; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":339 */
  __pyx_1 = PyInt_FromLong(GL_CURRENT_RASTER_INDEX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 339; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CURRENT_RASTER_INDEX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 339; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":340 */
  __pyx_1 = PyInt_FromLong(GL_CURRENT_RASTER_POSITION); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 340; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CURRENT_RASTER_POSITION, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 340; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":341 */
  __pyx_1 = PyInt_FromLong(GL_CURRENT_RASTER_TEXTURE_COORDS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 341; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CURRENT_RASTER_TEXTURE_COORDS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 341; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":342 */
  __pyx_1 = PyInt_FromLong(GL_CURRENT_RASTER_POSITION_VALID); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 342; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CURRENT_RASTER_POSITION_VALID, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 342; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":343 */
  __pyx_1 = PyInt_FromLong(GL_CURRENT_TEXTURE_COORDS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 343; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CURRENT_TEXTURE_COORDS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 343; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":344 */
  __pyx_1 = PyInt_FromLong(GL_INDEX_CLEAR_VALUE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 344; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INDEX_CLEAR_VALUE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 344; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":345 */
  __pyx_1 = PyInt_FromLong(GL_INDEX_MODE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 345; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INDEX_MODE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 345; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":346 */
  __pyx_1 = PyInt_FromLong(GL_INDEX_WRITEMASK); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 346; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INDEX_WRITEMASK, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 346; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":347 */
  __pyx_1 = PyInt_FromLong(GL_MODELVIEW_MATRIX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 347; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MODELVIEW_MATRIX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 347; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":348 */
  __pyx_1 = PyInt_FromLong(GL_MODELVIEW_STACK_DEPTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 348; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MODELVIEW_STACK_DEPTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 348; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":349 */
  __pyx_1 = PyInt_FromLong(GL_NAME_STACK_DEPTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 349; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NAME_STACK_DEPTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 349; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":350 */
  __pyx_1 = PyInt_FromLong(GL_PROJECTION_MATRIX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 350; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PROJECTION_MATRIX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 350; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":351 */
  __pyx_1 = PyInt_FromLong(GL_PROJECTION_STACK_DEPTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 351; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PROJECTION_STACK_DEPTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 351; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":352 */
  __pyx_1 = PyInt_FromLong(GL_RENDER_MODE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 352; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RENDER_MODE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 352; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":353 */
  __pyx_1 = PyInt_FromLong(GL_RGBA_MODE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 353; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RGBA_MODE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 353; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":354 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_MATRIX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 354; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_MATRIX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 354; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":355 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_STACK_DEPTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 355; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_STACK_DEPTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 355; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":356 */
  __pyx_1 = PyInt_FromLong(GL_VIEWPORT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 356; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_VIEWPORT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 356; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":357 */
  __pyx_1 = PyInt_FromLong(GL_AUTO_NORMAL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 357; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_AUTO_NORMAL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 357; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":358 */
  __pyx_1 = PyInt_FromLong(GL_MAP1_COLOR_4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 358; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP1_COLOR_4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 358; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":359 */
  __pyx_1 = PyInt_FromLong(GL_MAP1_GRID_DOMAIN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 359; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP1_GRID_DOMAIN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 359; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":360 */
  __pyx_1 = PyInt_FromLong(GL_MAP1_GRID_SEGMENTS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 360; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP1_GRID_SEGMENTS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 360; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":361 */
  __pyx_1 = PyInt_FromLong(GL_MAP1_INDEX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 361; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP1_INDEX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 361; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":362 */
  __pyx_1 = PyInt_FromLong(GL_MAP1_NORMAL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 362; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP1_NORMAL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 362; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":363 */
  __pyx_1 = PyInt_FromLong(GL_MAP1_TEXTURE_COORD_1); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 363; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP1_TEXTURE_COORD_1, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 363; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":364 */
  __pyx_1 = PyInt_FromLong(GL_MAP1_TEXTURE_COORD_2); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 364; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP1_TEXTURE_COORD_2, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 364; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":365 */
  __pyx_1 = PyInt_FromLong(GL_MAP1_TEXTURE_COORD_3); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 365; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP1_TEXTURE_COORD_3, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 365; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":366 */
  __pyx_1 = PyInt_FromLong(GL_MAP1_TEXTURE_COORD_4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 366; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP1_TEXTURE_COORD_4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 366; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":367 */
  __pyx_1 = PyInt_FromLong(GL_MAP1_VERTEX_3); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 367; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP1_VERTEX_3, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 367; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":368 */
  __pyx_1 = PyInt_FromLong(GL_MAP1_VERTEX_4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 368; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP1_VERTEX_4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 368; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":369 */
  __pyx_1 = PyInt_FromLong(GL_MAP2_COLOR_4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 369; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP2_COLOR_4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 369; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":370 */
  __pyx_1 = PyInt_FromLong(GL_MAP2_GRID_DOMAIN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 370; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP2_GRID_DOMAIN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 370; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":371 */
  __pyx_1 = PyInt_FromLong(GL_MAP2_GRID_SEGMENTS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 371; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP2_GRID_SEGMENTS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 371; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":372 */
  __pyx_1 = PyInt_FromLong(GL_MAP2_INDEX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 372; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP2_INDEX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 372; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":373 */
  __pyx_1 = PyInt_FromLong(GL_MAP2_NORMAL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 373; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP2_NORMAL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 373; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":374 */
  __pyx_1 = PyInt_FromLong(GL_MAP2_TEXTURE_COORD_1); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 374; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP2_TEXTURE_COORD_1, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 374; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":375 */
  __pyx_1 = PyInt_FromLong(GL_MAP2_TEXTURE_COORD_2); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 375; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP2_TEXTURE_COORD_2, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 375; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":376 */
  __pyx_1 = PyInt_FromLong(GL_MAP2_TEXTURE_COORD_3); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 376; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP2_TEXTURE_COORD_3, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 376; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":377 */
  __pyx_1 = PyInt_FromLong(GL_MAP2_TEXTURE_COORD_4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 377; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP2_TEXTURE_COORD_4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 377; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":378 */
  __pyx_1 = PyInt_FromLong(GL_MAP2_VERTEX_3); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 378; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP2_VERTEX_3, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 378; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":379 */
  __pyx_1 = PyInt_FromLong(GL_MAP2_VERTEX_4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 379; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP2_VERTEX_4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 379; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":380 */
  __pyx_1 = PyInt_FromLong(GL_COEFF); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 380; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COEFF, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 380; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":381 */
  __pyx_1 = PyInt_FromLong(GL_DOMAIN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 381; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DOMAIN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 381; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":382 */
  __pyx_1 = PyInt_FromLong(GL_ORDER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 382; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ORDER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 382; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":383 */
  __pyx_1 = PyInt_FromLong(GL_FOG_HINT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 383; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FOG_HINT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 383; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":384 */
  __pyx_1 = PyInt_FromLong(GL_LINE_SMOOTH_HINT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 384; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINE_SMOOTH_HINT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 384; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":385 */
  __pyx_1 = PyInt_FromLong(GL_PERSPECTIVE_CORRECTION_HINT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 385; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PERSPECTIVE_CORRECTION_HINT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 385; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":386 */
  __pyx_1 = PyInt_FromLong(GL_POINT_SMOOTH_HINT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 386; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POINT_SMOOTH_HINT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 386; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":387 */
  __pyx_1 = PyInt_FromLong(GL_POLYGON_SMOOTH_HINT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 387; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POLYGON_SMOOTH_HINT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 387; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":388 */
  __pyx_1 = PyInt_FromLong(GL_DONT_CARE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 388; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DONT_CARE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 388; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":389 */
  __pyx_1 = PyInt_FromLong(GL_FASTEST); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 389; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FASTEST, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 389; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":390 */
  __pyx_1 = PyInt_FromLong(GL_NICEST); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 390; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NICEST, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 390; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":391 */
  __pyx_1 = PyInt_FromLong(GL_SCISSOR_TEST); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 391; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SCISSOR_TEST, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 391; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":392 */
  __pyx_1 = PyInt_FromLong(GL_SCISSOR_BOX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 392; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SCISSOR_BOX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 392; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":393 */
  __pyx_1 = PyInt_FromLong(GL_MAP_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 393; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 393; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":394 */
  __pyx_1 = PyInt_FromLong(GL_MAP_STENCIL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 394; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAP_STENCIL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 394; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":395 */
  __pyx_1 = PyInt_FromLong(GL_INDEX_SHIFT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 395; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INDEX_SHIFT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 395; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":396 */
  __pyx_1 = PyInt_FromLong(GL_INDEX_OFFSET); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 396; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INDEX_OFFSET, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 396; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":397 */
  __pyx_1 = PyInt_FromLong(GL_RED_SCALE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 397; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RED_SCALE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 397; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":398 */
  __pyx_1 = PyInt_FromLong(GL_RED_BIAS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 398; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RED_BIAS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 398; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":399 */
  __pyx_1 = PyInt_FromLong(GL_GREEN_SCALE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 399; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_GREEN_SCALE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 399; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":400 */
  __pyx_1 = PyInt_FromLong(GL_GREEN_BIAS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 400; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_GREEN_BIAS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 400; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":401 */
  __pyx_1 = PyInt_FromLong(GL_BLUE_SCALE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 401; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_BLUE_SCALE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 401; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":402 */
  __pyx_1 = PyInt_FromLong(GL_BLUE_BIAS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 402; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_BLUE_BIAS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 402; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":403 */
  __pyx_1 = PyInt_FromLong(GL_ALPHA_SCALE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 403; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ALPHA_SCALE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 403; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":404 */
  __pyx_1 = PyInt_FromLong(GL_ALPHA_BIAS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 404; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ALPHA_BIAS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 404; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":405 */
  __pyx_1 = PyInt_FromLong(GL_DEPTH_SCALE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 405; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DEPTH_SCALE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 405; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":406 */
  __pyx_1 = PyInt_FromLong(GL_DEPTH_BIAS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 406; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DEPTH_BIAS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 406; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":407 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_S_TO_S_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 407; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_S_TO_S_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 407; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":408 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_I_TO_I_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 408; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_I_TO_I_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 408; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":409 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_I_TO_R_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 409; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_I_TO_R_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 409; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":410 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_I_TO_G_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 410; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_I_TO_G_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 410; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":411 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_I_TO_B_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 411; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_I_TO_B_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 411; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":412 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_I_TO_A_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 412; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_I_TO_A_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 412; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":413 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_R_TO_R_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 413; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_R_TO_R_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 413; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":414 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_G_TO_G_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 414; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_G_TO_G_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 414; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":415 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_B_TO_B_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 415; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_B_TO_B_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 415; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":416 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_A_TO_A_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 416; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_A_TO_A_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 416; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":417 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_S_TO_S); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 417; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_S_TO_S, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 417; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":418 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_I_TO_I); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 418; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_I_TO_I, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 418; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":419 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_I_TO_R); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 419; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_I_TO_R, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 419; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":420 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_I_TO_G); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 420; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_I_TO_G, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 420; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":421 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_I_TO_B); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 421; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_I_TO_B, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 421; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":422 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_I_TO_A); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 422; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_I_TO_A, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 422; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":423 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_R_TO_R); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 423; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_R_TO_R, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 423; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":424 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_G_TO_G); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 424; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_G_TO_G, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 424; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":425 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_B_TO_B); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 425; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_B_TO_B, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 425; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":426 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MAP_A_TO_A); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 426; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MAP_A_TO_A, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 426; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":427 */
  __pyx_1 = PyInt_FromLong(GL_PACK_ALIGNMENT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 427; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PACK_ALIGNMENT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 427; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":428 */
  __pyx_1 = PyInt_FromLong(GL_PACK_LSB_FIRST); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 428; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PACK_LSB_FIRST, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 428; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":429 */
  __pyx_1 = PyInt_FromLong(GL_PACK_ROW_LENGTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 429; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PACK_ROW_LENGTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 429; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":430 */
  __pyx_1 = PyInt_FromLong(GL_PACK_SKIP_PIXELS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 430; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PACK_SKIP_PIXELS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 430; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":431 */
  __pyx_1 = PyInt_FromLong(GL_PACK_SKIP_ROWS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 431; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PACK_SKIP_ROWS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 431; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":432 */
  __pyx_1 = PyInt_FromLong(GL_PACK_SWAP_BYTES); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 432; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PACK_SWAP_BYTES, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 432; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":433 */
  __pyx_1 = PyInt_FromLong(GL_UNPACK_ALIGNMENT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 433; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNPACK_ALIGNMENT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 433; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":434 */
  __pyx_1 = PyInt_FromLong(GL_UNPACK_LSB_FIRST); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 434; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNPACK_LSB_FIRST, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 434; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":435 */
  __pyx_1 = PyInt_FromLong(GL_UNPACK_ROW_LENGTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 435; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNPACK_ROW_LENGTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 435; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":436 */
  __pyx_1 = PyInt_FromLong(GL_UNPACK_SKIP_PIXELS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 436; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNPACK_SKIP_PIXELS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 436; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":437 */
  __pyx_1 = PyInt_FromLong(GL_UNPACK_SKIP_ROWS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 437; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNPACK_SKIP_ROWS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 437; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":438 */
  __pyx_1 = PyInt_FromLong(GL_UNPACK_SWAP_BYTES); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 438; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNPACK_SWAP_BYTES, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 438; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":439 */
  __pyx_1 = PyInt_FromLong(GL_ZOOM_X); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 439; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ZOOM_X, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 439; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":440 */
  __pyx_1 = PyInt_FromLong(GL_ZOOM_Y); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 440; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ZOOM_Y, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 440; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":441 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_ENV); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 441; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_ENV, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 441; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":442 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_ENV_MODE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 442; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_ENV_MODE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 442; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":443 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_1D); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 443; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_1D, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 443; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":444 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_2D); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 444; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_2D, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 444; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":445 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_WRAP_S); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 445; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_WRAP_S, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 445; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":446 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_WRAP_T); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 446; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_WRAP_T, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 446; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":447 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_MAG_FILTER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 447; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_MAG_FILTER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 447; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":448 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_MIN_FILTER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 448; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_MIN_FILTER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 448; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":449 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_ENV_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 449; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_ENV_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 449; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":450 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_GEN_S); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 450; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_GEN_S, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 450; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":451 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_GEN_T); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 451; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_GEN_T, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 451; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":452 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_GEN_MODE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 452; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_GEN_MODE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 452; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":453 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_BORDER_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 453; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_BORDER_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 453; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":454 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_WIDTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 454; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_WIDTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 454; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":455 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_HEIGHT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 455; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_HEIGHT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 455; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":456 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_BORDER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 456; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_BORDER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 456; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":457 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_COMPONENTS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 457; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_COMPONENTS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 457; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":458 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_RED_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 458; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_RED_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 458; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":459 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_GREEN_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 459; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_GREEN_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 459; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":460 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_BLUE_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 460; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_BLUE_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 460; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":461 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_ALPHA_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 461; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_ALPHA_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 461; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":462 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_LUMINANCE_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 462; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_LUMINANCE_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 462; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":463 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_INTENSITY_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 463; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_INTENSITY_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 463; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":464 */
  __pyx_1 = PyInt_FromLong(GL_NEAREST_MIPMAP_NEAREST); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 464; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NEAREST_MIPMAP_NEAREST, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 464; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":465 */
  __pyx_1 = PyInt_FromLong(GL_NEAREST_MIPMAP_LINEAR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 465; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NEAREST_MIPMAP_LINEAR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 465; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":466 */
  __pyx_1 = PyInt_FromLong(GL_LINEAR_MIPMAP_NEAREST); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 466; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINEAR_MIPMAP_NEAREST, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 466; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":467 */
  __pyx_1 = PyInt_FromLong(GL_LINEAR_MIPMAP_LINEAR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 467; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINEAR_MIPMAP_LINEAR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 467; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":468 */
  __pyx_1 = PyInt_FromLong(GL_OBJECT_LINEAR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 468; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_OBJECT_LINEAR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 468; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":469 */
  __pyx_1 = PyInt_FromLong(GL_OBJECT_PLANE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 469; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_OBJECT_PLANE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 469; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":470 */
  __pyx_1 = PyInt_FromLong(GL_EYE_LINEAR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 470; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_EYE_LINEAR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 470; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":471 */
  __pyx_1 = PyInt_FromLong(GL_EYE_PLANE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 471; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_EYE_PLANE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 471; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":472 */
  __pyx_1 = PyInt_FromLong(GL_SPHERE_MAP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 472; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SPHERE_MAP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 472; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":473 */
  __pyx_1 = PyInt_FromLong(GL_DECAL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 473; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DECAL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 473; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":474 */
  __pyx_1 = PyInt_FromLong(GL_MODULATE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 474; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MODULATE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 474; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":475 */
  __pyx_1 = PyInt_FromLong(GL_NEAREST); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 475; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NEAREST, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 475; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":476 */
  __pyx_1 = PyInt_FromLong(GL_REPEAT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 476; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_REPEAT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 476; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":477 */
  __pyx_1 = PyInt_FromLong(GL_CLAMP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 477; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CLAMP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 477; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":478 */
  __pyx_1 = PyInt_FromLong(GL_S); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 478; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_S, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 478; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":479 */
  __pyx_1 = PyInt_FromLong(GL_T); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 479; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_T, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 479; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":480 */
  __pyx_1 = PyInt_FromLong(GL_R); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 480; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_R, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 480; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":481 */
  __pyx_1 = PyInt_FromLong(GL_Q); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 481; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_Q, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 481; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":482 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_GEN_R); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 482; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_GEN_R, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 482; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":483 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_GEN_Q); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 483; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_GEN_Q, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 483; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":484 */
  __pyx_1 = PyInt_FromLong(GL_VENDOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 484; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_VENDOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 484; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":485 */
  __pyx_1 = PyInt_FromLong(GL_RENDERER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 485; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RENDERER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 485; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":486 */
  __pyx_1 = PyInt_FromLong(GL_VERSION); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 486; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_VERSION, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 486; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":487 */
  __pyx_1 = PyInt_FromLong(GL_EXTENSIONS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 487; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_EXTENSIONS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 487; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":488 */
  __pyx_1 = PyInt_FromLong(GL_NO_ERROR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 488; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NO_ERROR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 488; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":489 */
  __pyx_1 = PyInt_FromLong(GL_INVALID_VALUE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 489; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INVALID_VALUE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 489; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":490 */
  __pyx_1 = PyInt_FromLong(GL_INVALID_ENUM); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 490; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INVALID_ENUM, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 490; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":491 */
  __pyx_1 = PyInt_FromLong(GL_INVALID_OPERATION); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 491; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INVALID_OPERATION, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 491; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":492 */
  __pyx_1 = PyInt_FromLong(GL_STACK_OVERFLOW); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 492; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_STACK_OVERFLOW, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 492; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":493 */
  __pyx_1 = PyInt_FromLong(GL_STACK_UNDERFLOW); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 493; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_STACK_UNDERFLOW, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 493; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":494 */
  __pyx_1 = PyInt_FromLong(GL_OUT_OF_MEMORY); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 494; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_OUT_OF_MEMORY, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 494; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":495 */
  __pyx_1 = PyInt_FromLong(GL_CURRENT_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 495; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CURRENT_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 495; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":496 */
  __pyx_1 = PyInt_FromLong(GL_POINT_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 496; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POINT_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 496; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":497 */
  __pyx_1 = PyInt_FromLong(GL_LINE_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 497; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LINE_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 497; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":498 */
  __pyx_1 = PyInt_FromLong(GL_POLYGON_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 498; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POLYGON_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 498; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":499 */
  __pyx_1 = PyInt_FromLong(GL_POLYGON_STIPPLE_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 499; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POLYGON_STIPPLE_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 499; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":500 */
  __pyx_1 = PyInt_FromLong(GL_PIXEL_MODE_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 500; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PIXEL_MODE_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 500; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":501 */
  __pyx_1 = PyInt_FromLong(GL_LIGHTING_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 501; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LIGHTING_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 501; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":502 */
  __pyx_1 = PyInt_FromLong(GL_FOG_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 502; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FOG_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 502; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":503 */
  __pyx_1 = PyInt_FromLong(GL_DEPTH_BUFFER_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 503; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DEPTH_BUFFER_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 503; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":504 */
  __pyx_1 = PyInt_FromLong(GL_ACCUM_BUFFER_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 504; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ACCUM_BUFFER_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 504; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":505 */
  __pyx_1 = PyInt_FromLong(GL_STENCIL_BUFFER_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 505; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_STENCIL_BUFFER_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 505; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":506 */
  __pyx_1 = PyInt_FromLong(GL_VIEWPORT_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 506; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_VIEWPORT_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 506; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":507 */
  __pyx_1 = PyInt_FromLong(GL_TRANSFORM_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 507; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TRANSFORM_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 507; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":508 */
  __pyx_1 = PyInt_FromLong(GL_ENABLE_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 508; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ENABLE_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 508; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":509 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_BUFFER_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 509; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_BUFFER_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 509; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":510 */
  __pyx_1 = PyInt_FromLong(GL_HINT_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 510; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_HINT_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 510; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":511 */
  __pyx_1 = PyInt_FromLong(GL_EVAL_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 511; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_EVAL_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 511; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":512 */
  __pyx_1 = PyInt_FromLong(GL_LIST_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 512; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LIST_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 512; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":513 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 513; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 513; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":514 */
  __pyx_1 = PyInt_FromLong(GL_SCISSOR_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 514; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SCISSOR_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 514; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":515 */
  __pyx_1 = PyInt_FromLong(GL_ALL_ATTRIB_BITS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 515; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ALL_ATTRIB_BITS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 515; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":516 */
  __pyx_1 = PyInt_FromLong(GL_PROXY_TEXTURE_1D); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 516; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PROXY_TEXTURE_1D, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 516; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":517 */
  __pyx_1 = PyInt_FromLong(GL_PROXY_TEXTURE_2D); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 517; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PROXY_TEXTURE_2D, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 517; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":518 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_PRIORITY); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 518; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_PRIORITY, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 518; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":519 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_RESIDENT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 519; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_RESIDENT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 519; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":520 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_BINDING_1D); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 520; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_BINDING_1D, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 520; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":521 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_BINDING_2D); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 521; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_BINDING_2D, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 521; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":522 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_INTERNAL_FORMAT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 522; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_INTERNAL_FORMAT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 522; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":523 */
  __pyx_1 = PyInt_FromLong(GL_ALPHA4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 523; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ALPHA4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 523; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":524 */
  __pyx_1 = PyInt_FromLong(GL_ALPHA8); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 524; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ALPHA8, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 524; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":525 */
  __pyx_1 = PyInt_FromLong(GL_ALPHA12); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 525; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ALPHA12, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 525; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":526 */
  __pyx_1 = PyInt_FromLong(GL_ALPHA16); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 526; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ALPHA16, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 526; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":527 */
  __pyx_1 = PyInt_FromLong(GL_LUMINANCE4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 527; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LUMINANCE4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 527; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":528 */
  __pyx_1 = PyInt_FromLong(GL_LUMINANCE8); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 528; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LUMINANCE8, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 528; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":529 */
  __pyx_1 = PyInt_FromLong(GL_LUMINANCE12); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 529; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LUMINANCE12, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 529; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":530 */
  __pyx_1 = PyInt_FromLong(GL_LUMINANCE16); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 530; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LUMINANCE16, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 530; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":531 */
  __pyx_1 = PyInt_FromLong(GL_LUMINANCE4_ALPHA4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 531; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LUMINANCE4_ALPHA4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 531; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":532 */
  __pyx_1 = PyInt_FromLong(GL_LUMINANCE6_ALPHA2); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 532; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LUMINANCE6_ALPHA2, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 532; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":533 */
  __pyx_1 = PyInt_FromLong(GL_LUMINANCE8_ALPHA8); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 533; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LUMINANCE8_ALPHA8, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 533; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":534 */
  __pyx_1 = PyInt_FromLong(GL_LUMINANCE12_ALPHA4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 534; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LUMINANCE12_ALPHA4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 534; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":535 */
  __pyx_1 = PyInt_FromLong(GL_LUMINANCE12_ALPHA12); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 535; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LUMINANCE12_ALPHA12, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 535; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":536 */
  __pyx_1 = PyInt_FromLong(GL_LUMINANCE16_ALPHA16); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 536; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LUMINANCE16_ALPHA16, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 536; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":537 */
  __pyx_1 = PyInt_FromLong(GL_INTENSITY); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 537; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INTENSITY, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 537; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":538 */
  __pyx_1 = PyInt_FromLong(GL_INTENSITY4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 538; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INTENSITY4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 538; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":539 */
  __pyx_1 = PyInt_FromLong(GL_INTENSITY8); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 539; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INTENSITY8, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 539; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":540 */
  __pyx_1 = PyInt_FromLong(GL_INTENSITY12); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 540; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INTENSITY12, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 540; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":541 */
  __pyx_1 = PyInt_FromLong(GL_INTENSITY16); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 541; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INTENSITY16, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 541; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":542 */
  __pyx_1 = PyInt_FromLong(GL_R3_G3_B2); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 542; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_R3_G3_B2, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 542; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":543 */
  __pyx_1 = PyInt_FromLong(GL_RGB4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 543; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RGB4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 543; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":544 */
  __pyx_1 = PyInt_FromLong(GL_RGB5); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 544; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RGB5, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 544; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":545 */
  __pyx_1 = PyInt_FromLong(GL_RGB8); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 545; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RGB8, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 545; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":546 */
  __pyx_1 = PyInt_FromLong(GL_RGB10); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 546; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RGB10, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 546; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":547 */
  __pyx_1 = PyInt_FromLong(GL_RGB12); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 547; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RGB12, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 547; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":548 */
  __pyx_1 = PyInt_FromLong(GL_RGB16); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 548; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RGB16, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 548; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":549 */
  __pyx_1 = PyInt_FromLong(GL_RGBA2); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 549; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RGBA2, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 549; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":550 */
  __pyx_1 = PyInt_FromLong(GL_RGBA4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 550; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RGBA4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 550; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":551 */
  __pyx_1 = PyInt_FromLong(GL_RGB5_A1); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 551; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RGB5_A1, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 551; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":552 */
  __pyx_1 = PyInt_FromLong(GL_RGBA8); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 552; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RGBA8, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 552; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":553 */
  __pyx_1 = PyInt_FromLong(GL_RGB10_A2); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 553; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RGB10_A2, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 553; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":554 */
  __pyx_1 = PyInt_FromLong(GL_RGBA12); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 554; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RGBA12, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 554; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":555 */
  __pyx_1 = PyInt_FromLong(GL_RGBA16); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 555; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RGBA16, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 555; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":556 */
  __pyx_1 = PyInt_FromLong(GL_CLIENT_PIXEL_STORE_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 556; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CLIENT_PIXEL_STORE_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 556; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":557 */
  __pyx_1 = PyInt_FromLong(GL_CLIENT_VERTEX_ARRAY_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 557; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CLIENT_VERTEX_ARRAY_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 557; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":559 */
  __pyx_1 = PyInt_FromLong(GL_CLIENT_ALL_ATTRIB_BITS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 559; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CLIENT_ALL_ATTRIB_BITS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 559; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":560 */
  __pyx_1 = PyInt_FromLong(GL_RESCALE_NORMAL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 560; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RESCALE_NORMAL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 560; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":561 */
  __pyx_1 = PyInt_FromLong(GL_CLAMP_TO_EDGE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 561; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CLAMP_TO_EDGE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 561; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":562 */
  __pyx_1 = PyInt_FromLong(GL_MAX_ELEMENTS_VERTICES); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 562; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_ELEMENTS_VERTICES, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 562; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":563 */
  __pyx_1 = PyInt_FromLong(GL_MAX_ELEMENTS_INDICES); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 563; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_ELEMENTS_INDICES, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 563; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":564 */
  __pyx_1 = PyInt_FromLong(GL_BGR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 564; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_BGR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 564; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":565 */
  __pyx_1 = PyInt_FromLong(GL_BGRA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 565; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_BGRA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 565; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":566 */
  __pyx_1 = PyInt_FromLong(GL_UNSIGNED_BYTE_3_3_2); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 566; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNSIGNED_BYTE_3_3_2, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 566; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":567 */
  __pyx_1 = PyInt_FromLong(GL_UNSIGNED_BYTE_2_3_3_REV); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 567; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNSIGNED_BYTE_2_3_3_REV, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 567; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":568 */
  __pyx_1 = PyInt_FromLong(GL_UNSIGNED_SHORT_5_6_5); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 568; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNSIGNED_SHORT_5_6_5, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 568; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":569 */
  __pyx_1 = PyInt_FromLong(GL_UNSIGNED_SHORT_5_6_5_REV); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 569; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNSIGNED_SHORT_5_6_5_REV, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 569; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":570 */
  __pyx_1 = PyInt_FromLong(GL_UNSIGNED_SHORT_4_4_4_4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 570; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNSIGNED_SHORT_4_4_4_4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 570; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":571 */
  __pyx_1 = PyInt_FromLong(GL_UNSIGNED_SHORT_4_4_4_4_REV); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 571; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNSIGNED_SHORT_4_4_4_4_REV, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 571; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":572 */
  __pyx_1 = PyInt_FromLong(GL_UNSIGNED_SHORT_5_5_5_1); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 572; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNSIGNED_SHORT_5_5_5_1, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 572; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":573 */
  __pyx_1 = PyInt_FromLong(GL_UNSIGNED_SHORT_1_5_5_5_REV); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 573; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNSIGNED_SHORT_1_5_5_5_REV, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 573; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":574 */
  __pyx_1 = PyInt_FromLong(GL_UNSIGNED_INT_8_8_8_8); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 574; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNSIGNED_INT_8_8_8_8, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 574; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":575 */
  __pyx_1 = PyInt_FromLong(GL_UNSIGNED_INT_8_8_8_8_REV); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 575; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNSIGNED_INT_8_8_8_8_REV, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 575; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":576 */
  __pyx_1 = PyInt_FromLong(GL_UNSIGNED_INT_10_10_10_2); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 576; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNSIGNED_INT_10_10_10_2, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 576; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":577 */
  __pyx_1 = PyInt_FromLong(GL_UNSIGNED_INT_2_10_10_10_REV); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 577; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNSIGNED_INT_2_10_10_10_REV, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 577; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":578 */
  __pyx_1 = PyInt_FromLong(GL_LIGHT_MODEL_COLOR_CONTROL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 578; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_LIGHT_MODEL_COLOR_CONTROL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 578; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":579 */
  __pyx_1 = PyInt_FromLong(GL_SINGLE_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 579; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SINGLE_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 579; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":580 */
  __pyx_1 = PyInt_FromLong(GL_SEPARATE_SPECULAR_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 580; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SEPARATE_SPECULAR_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 580; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":581 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_MIN_LOD); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 581; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_MIN_LOD, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 581; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":582 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_MAX_LOD); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 582; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_MAX_LOD, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 582; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":583 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_BASE_LEVEL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 583; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_BASE_LEVEL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 583; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":584 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_MAX_LEVEL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 584; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_MAX_LEVEL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 584; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":585 */
  __pyx_1 = PyInt_FromLong(GL_SMOOTH_POINT_SIZE_RANGE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 585; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SMOOTH_POINT_SIZE_RANGE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 585; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":586 */
  __pyx_1 = PyInt_FromLong(GL_SMOOTH_POINT_SIZE_GRANULARITY); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 586; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SMOOTH_POINT_SIZE_GRANULARITY, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 586; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":587 */
  __pyx_1 = PyInt_FromLong(GL_SMOOTH_LINE_WIDTH_RANGE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 587; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SMOOTH_LINE_WIDTH_RANGE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 587; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":588 */
  __pyx_1 = PyInt_FromLong(GL_SMOOTH_LINE_WIDTH_GRANULARITY); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 588; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SMOOTH_LINE_WIDTH_GRANULARITY, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 588; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":589 */
  __pyx_1 = PyInt_FromLong(GL_ALIASED_POINT_SIZE_RANGE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 589; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ALIASED_POINT_SIZE_RANGE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 589; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":590 */
  __pyx_1 = PyInt_FromLong(GL_ALIASED_LINE_WIDTH_RANGE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 590; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ALIASED_LINE_WIDTH_RANGE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 590; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":591 */
  __pyx_1 = PyInt_FromLong(GL_PACK_SKIP_IMAGES); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 591; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PACK_SKIP_IMAGES, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 591; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":592 */
  __pyx_1 = PyInt_FromLong(GL_PACK_IMAGE_HEIGHT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 592; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PACK_IMAGE_HEIGHT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 592; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":593 */
  __pyx_1 = PyInt_FromLong(GL_UNPACK_SKIP_IMAGES); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 593; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNPACK_SKIP_IMAGES, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 593; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":594 */
  __pyx_1 = PyInt_FromLong(GL_UNPACK_IMAGE_HEIGHT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 594; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_UNPACK_IMAGE_HEIGHT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 594; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":595 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_3D); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 595; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_3D, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 595; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":596 */
  __pyx_1 = PyInt_FromLong(GL_PROXY_TEXTURE_3D); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 596; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PROXY_TEXTURE_3D, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 596; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":597 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_DEPTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 597; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_DEPTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 597; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":598 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_WRAP_R); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 598; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_WRAP_R, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 598; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":599 */
  __pyx_1 = PyInt_FromLong(GL_MAX_3D_TEXTURE_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 599; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_3D_TEXTURE_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 599; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":600 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_BINDING_3D); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 600; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_BINDING_3D, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 600; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":601 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_TABLE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 601; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_TABLE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 601; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":602 */
  __pyx_1 = PyInt_FromLong(GL_POST_CONVOLUTION_COLOR_TABLE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 602; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POST_CONVOLUTION_COLOR_TABLE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 602; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":603 */
  __pyx_1 = PyInt_FromLong(GL_POST_COLOR_MATRIX_COLOR_TABLE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 603; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POST_COLOR_MATRIX_COLOR_TABLE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 603; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":604 */
  __pyx_1 = PyInt_FromLong(GL_PROXY_COLOR_TABLE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 604; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PROXY_COLOR_TABLE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 604; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":605 */
  __pyx_1 = PyInt_FromLong(GL_PROXY_POST_CONVOLUTION_COLOR_TABLE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 605; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PROXY_POST_CONVOLUTION_COLOR_TABLE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 605; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":606 */
  __pyx_1 = PyInt_FromLong(GL_PROXY_POST_COLOR_MATRIX_COLOR_TABLE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 606; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PROXY_POST_COLOR_MATRIX_COLOR_TABLE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 606; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":607 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_TABLE_SCALE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 607; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_TABLE_SCALE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 607; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":608 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_TABLE_BIAS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 608; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_TABLE_BIAS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 608; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":609 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_TABLE_FORMAT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 609; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_TABLE_FORMAT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 609; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":610 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_TABLE_WIDTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 610; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_TABLE_WIDTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 610; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":611 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_TABLE_RED_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 611; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_TABLE_RED_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 611; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":612 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_TABLE_GREEN_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 612; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_TABLE_GREEN_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 612; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":613 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_TABLE_BLUE_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 613; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_TABLE_BLUE_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 613; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":614 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_TABLE_ALPHA_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 614; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_TABLE_ALPHA_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 614; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":615 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_TABLE_LUMINANCE_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 615; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_TABLE_LUMINANCE_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 615; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":616 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_TABLE_INTENSITY_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 616; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_TABLE_INTENSITY_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 616; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":617 */
  __pyx_1 = PyInt_FromLong(GL_CONVOLUTION_1D); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 617; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CONVOLUTION_1D, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 617; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":618 */
  __pyx_1 = PyInt_FromLong(GL_CONVOLUTION_2D); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 618; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CONVOLUTION_2D, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 618; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":619 */
  __pyx_1 = PyInt_FromLong(GL_SEPARABLE_2D); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 619; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SEPARABLE_2D, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 619; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":620 */
  __pyx_1 = PyInt_FromLong(GL_CONVOLUTION_BORDER_MODE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 620; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CONVOLUTION_BORDER_MODE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 620; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":621 */
  __pyx_1 = PyInt_FromLong(GL_CONVOLUTION_FILTER_SCALE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 621; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CONVOLUTION_FILTER_SCALE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 621; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":622 */
  __pyx_1 = PyInt_FromLong(GL_CONVOLUTION_FILTER_BIAS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 622; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CONVOLUTION_FILTER_BIAS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 622; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":623 */
  __pyx_1 = PyInt_FromLong(GL_REDUCE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 623; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_REDUCE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 623; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":624 */
  __pyx_1 = PyInt_FromLong(GL_CONVOLUTION_FORMAT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 624; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CONVOLUTION_FORMAT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 624; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":625 */
  __pyx_1 = PyInt_FromLong(GL_CONVOLUTION_WIDTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 625; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CONVOLUTION_WIDTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 625; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":626 */
  __pyx_1 = PyInt_FromLong(GL_CONVOLUTION_HEIGHT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 626; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CONVOLUTION_HEIGHT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 626; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":627 */
  __pyx_1 = PyInt_FromLong(GL_MAX_CONVOLUTION_WIDTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 627; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_CONVOLUTION_WIDTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 627; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":628 */
  __pyx_1 = PyInt_FromLong(GL_MAX_CONVOLUTION_HEIGHT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 628; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_CONVOLUTION_HEIGHT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 628; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":629 */
  __pyx_1 = PyInt_FromLong(GL_POST_CONVOLUTION_RED_SCALE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 629; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POST_CONVOLUTION_RED_SCALE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 629; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":630 */
  __pyx_1 = PyInt_FromLong(GL_POST_CONVOLUTION_GREEN_SCALE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 630; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POST_CONVOLUTION_GREEN_SCALE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 630; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":631 */
  __pyx_1 = PyInt_FromLong(GL_POST_CONVOLUTION_BLUE_SCALE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 631; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POST_CONVOLUTION_BLUE_SCALE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 631; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":632 */
  __pyx_1 = PyInt_FromLong(GL_POST_CONVOLUTION_ALPHA_SCALE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 632; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POST_CONVOLUTION_ALPHA_SCALE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 632; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":633 */
  __pyx_1 = PyInt_FromLong(GL_POST_CONVOLUTION_RED_BIAS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 633; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POST_CONVOLUTION_RED_BIAS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 633; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":634 */
  __pyx_1 = PyInt_FromLong(GL_POST_CONVOLUTION_GREEN_BIAS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 634; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POST_CONVOLUTION_GREEN_BIAS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 634; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":635 */
  __pyx_1 = PyInt_FromLong(GL_POST_CONVOLUTION_BLUE_BIAS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 635; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POST_CONVOLUTION_BLUE_BIAS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 635; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":636 */
  __pyx_1 = PyInt_FromLong(GL_POST_CONVOLUTION_ALPHA_BIAS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 636; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POST_CONVOLUTION_ALPHA_BIAS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 636; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":637 */
  __pyx_1 = PyInt_FromLong(GL_CONSTANT_BORDER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 637; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CONSTANT_BORDER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 637; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":638 */
  __pyx_1 = PyInt_FromLong(GL_REPLICATE_BORDER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 638; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_REPLICATE_BORDER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 638; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":639 */
  __pyx_1 = PyInt_FromLong(GL_CONVOLUTION_BORDER_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 639; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CONVOLUTION_BORDER_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 639; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":640 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_MATRIX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 640; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_MATRIX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 640; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":641 */
  __pyx_1 = PyInt_FromLong(GL_COLOR_MATRIX_STACK_DEPTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 641; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COLOR_MATRIX_STACK_DEPTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 641; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":642 */
  __pyx_1 = PyInt_FromLong(GL_MAX_COLOR_MATRIX_STACK_DEPTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 642; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_COLOR_MATRIX_STACK_DEPTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 642; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":643 */
  __pyx_1 = PyInt_FromLong(GL_POST_COLOR_MATRIX_RED_SCALE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 643; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POST_COLOR_MATRIX_RED_SCALE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 643; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":644 */
  __pyx_1 = PyInt_FromLong(GL_POST_COLOR_MATRIX_GREEN_SCALE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 644; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POST_COLOR_MATRIX_GREEN_SCALE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 644; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":645 */
  __pyx_1 = PyInt_FromLong(GL_POST_COLOR_MATRIX_BLUE_SCALE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 645; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POST_COLOR_MATRIX_BLUE_SCALE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 645; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":646 */
  __pyx_1 = PyInt_FromLong(GL_POST_COLOR_MATRIX_ALPHA_SCALE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 646; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POST_COLOR_MATRIX_ALPHA_SCALE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 646; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":647 */
  __pyx_1 = PyInt_FromLong(GL_POST_COLOR_MATRIX_RED_BIAS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 647; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POST_COLOR_MATRIX_RED_BIAS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 647; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":648 */
  __pyx_1 = PyInt_FromLong(GL_POST_COLOR_MATRIX_GREEN_BIAS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 648; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POST_COLOR_MATRIX_GREEN_BIAS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 648; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":649 */
  __pyx_1 = PyInt_FromLong(GL_POST_COLOR_MATRIX_BLUE_BIAS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 649; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POST_COLOR_MATRIX_BLUE_BIAS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 649; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":650 */
  __pyx_1 = PyInt_FromLong(GL_POST_COLOR_MATRIX_ALPHA_BIAS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 650; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_POST_COLOR_MATRIX_ALPHA_BIAS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 650; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":651 */
  __pyx_1 = PyInt_FromLong(GL_HISTOGRAM); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 651; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_HISTOGRAM, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 651; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":652 */
  __pyx_1 = PyInt_FromLong(GL_PROXY_HISTOGRAM); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 652; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PROXY_HISTOGRAM, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 652; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":653 */
  __pyx_1 = PyInt_FromLong(GL_HISTOGRAM_WIDTH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 653; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_HISTOGRAM_WIDTH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 653; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":654 */
  __pyx_1 = PyInt_FromLong(GL_HISTOGRAM_FORMAT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 654; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_HISTOGRAM_FORMAT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 654; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":655 */
  __pyx_1 = PyInt_FromLong(GL_HISTOGRAM_RED_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 655; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_HISTOGRAM_RED_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 655; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":656 */
  __pyx_1 = PyInt_FromLong(GL_HISTOGRAM_GREEN_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 656; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_HISTOGRAM_GREEN_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 656; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":657 */
  __pyx_1 = PyInt_FromLong(GL_HISTOGRAM_BLUE_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 657; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_HISTOGRAM_BLUE_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 657; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":658 */
  __pyx_1 = PyInt_FromLong(GL_HISTOGRAM_ALPHA_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 658; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_HISTOGRAM_ALPHA_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 658; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":659 */
  __pyx_1 = PyInt_FromLong(GL_HISTOGRAM_LUMINANCE_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 659; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_HISTOGRAM_LUMINANCE_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 659; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":660 */
  __pyx_1 = PyInt_FromLong(GL_HISTOGRAM_SINK); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 660; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_HISTOGRAM_SINK, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 660; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":661 */
  __pyx_1 = PyInt_FromLong(GL_MINMAX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 661; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MINMAX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 661; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":662 */
  __pyx_1 = PyInt_FromLong(GL_MINMAX_FORMAT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 662; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MINMAX_FORMAT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 662; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":663 */
  __pyx_1 = PyInt_FromLong(GL_MINMAX_SINK); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 663; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MINMAX_SINK, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 663; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":664 */
  __pyx_1 = PyInt_FromLong(GL_TABLE_TOO_LARGE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 664; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TABLE_TOO_LARGE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 664; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":665 */
  __pyx_1 = PyInt_FromLong(GL_BLEND_EQUATION); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 665; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_BLEND_EQUATION, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 665; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":666 */
  __pyx_1 = PyInt_FromLong(GL_MIN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 666; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MIN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 666; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":667 */
  __pyx_1 = PyInt_FromLong(GL_MAX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 667; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 667; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":668 */
  __pyx_1 = PyInt_FromLong(GL_FUNC_ADD); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 668; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FUNC_ADD, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 668; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":669 */
  __pyx_1 = PyInt_FromLong(GL_FUNC_SUBTRACT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 669; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FUNC_SUBTRACT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 669; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":670 */
  __pyx_1 = PyInt_FromLong(GL_FUNC_REVERSE_SUBTRACT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 670; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_FUNC_REVERSE_SUBTRACT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 670; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":671 */
  __pyx_1 = PyInt_FromLong(GL_BLEND_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 671; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_BLEND_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 671; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":672 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE0); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 672; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE0, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 672; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":673 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE1); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 673; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE1, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 673; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":674 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE2); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 674; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE2, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 674; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":675 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE3); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 675; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE3, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 675; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":676 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 676; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 676; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":677 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE5); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 677; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE5, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 677; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":678 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE6); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 678; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE6, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 678; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":679 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE7); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 679; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE7, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 679; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":680 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE8); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 680; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE8, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 680; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":681 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE9); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 681; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE9, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 681; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":682 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE10); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 682; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE10, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 682; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":683 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE11); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 683; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE11, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 683; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":684 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE12); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 684; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE12, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 684; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":685 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE13); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 685; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE13, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 685; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":686 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE14); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 686; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE14, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 686; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":687 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE15); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 687; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE15, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 687; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":688 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE16); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 688; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE16, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 688; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":689 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE17); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 689; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE17, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 689; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":690 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE18); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 690; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE18, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 690; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":691 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE19); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 691; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE19, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 691; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":692 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE20); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 692; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE20, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 692; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":693 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE21); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 693; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE21, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 693; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":694 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE22); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 694; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE22, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 694; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":695 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE23); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 695; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE23, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 695; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":696 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE24); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 696; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE24, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 696; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":697 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE25); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 697; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE25, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 697; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":698 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE26); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 698; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE26, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 698; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":699 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE27); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 699; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE27, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 699; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":700 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE28); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 700; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE28, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 700; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":701 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE29); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 701; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE29, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 701; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":702 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE30); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 702; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE30, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 702; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":703 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE31); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 703; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE31, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 703; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":704 */
  __pyx_1 = PyInt_FromLong(GL_ACTIVE_TEXTURE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 704; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ACTIVE_TEXTURE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 704; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":705 */
  __pyx_1 = PyInt_FromLong(GL_CLIENT_ACTIVE_TEXTURE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 705; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CLIENT_ACTIVE_TEXTURE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 705; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":706 */
  __pyx_1 = PyInt_FromLong(GL_MAX_TEXTURE_UNITS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 706; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_TEXTURE_UNITS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 706; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":707 */
  __pyx_1 = PyInt_FromLong(GL_NORMAL_MAP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 707; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NORMAL_MAP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 707; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":708 */
  __pyx_1 = PyInt_FromLong(GL_REFLECTION_MAP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 708; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_REFLECTION_MAP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 708; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":709 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_CUBE_MAP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 709; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_CUBE_MAP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 709; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":710 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_BINDING_CUBE_MAP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 710; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_BINDING_CUBE_MAP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 710; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":711 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_CUBE_MAP_POSITIVE_X); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 711; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_CUBE_MAP_POSITIVE_X, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 711; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":712 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_CUBE_MAP_NEGATIVE_X); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 712; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_CUBE_MAP_NEGATIVE_X, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 712; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":713 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_CUBE_MAP_POSITIVE_Y); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 713; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_CUBE_MAP_POSITIVE_Y, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 713; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":714 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 714; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 714; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":715 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_CUBE_MAP_POSITIVE_Z); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 715; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_CUBE_MAP_POSITIVE_Z, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 715; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":716 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 716; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 716; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":717 */
  __pyx_1 = PyInt_FromLong(GL_PROXY_TEXTURE_CUBE_MAP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 717; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PROXY_TEXTURE_CUBE_MAP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 717; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":718 */
  __pyx_1 = PyInt_FromLong(GL_MAX_CUBE_MAP_TEXTURE_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 718; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MAX_CUBE_MAP_TEXTURE_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 718; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":719 */
  __pyx_1 = PyInt_FromLong(GL_COMPRESSED_ALPHA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 719; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COMPRESSED_ALPHA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 719; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":720 */
  __pyx_1 = PyInt_FromLong(GL_COMPRESSED_LUMINANCE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 720; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COMPRESSED_LUMINANCE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 720; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":721 */
  __pyx_1 = PyInt_FromLong(GL_COMPRESSED_LUMINANCE_ALPHA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 721; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COMPRESSED_LUMINANCE_ALPHA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 721; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":722 */
  __pyx_1 = PyInt_FromLong(GL_COMPRESSED_INTENSITY); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 722; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COMPRESSED_INTENSITY, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 722; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":723 */
  __pyx_1 = PyInt_FromLong(GL_COMPRESSED_RGB); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 723; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COMPRESSED_RGB, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 723; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":724 */
  __pyx_1 = PyInt_FromLong(GL_COMPRESSED_RGBA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 724; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COMPRESSED_RGBA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 724; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":725 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_COMPRESSION_HINT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 725; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_COMPRESSION_HINT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 725; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":726 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_COMPRESSED_IMAGE_SIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 726; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_COMPRESSED_IMAGE_SIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 726; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":727 */
  __pyx_1 = PyInt_FromLong(GL_TEXTURE_COMPRESSED); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 727; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TEXTURE_COMPRESSED, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 727; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":728 */
  __pyx_1 = PyInt_FromLong(GL_NUM_COMPRESSED_TEXTURE_FORMATS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 728; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_NUM_COMPRESSED_TEXTURE_FORMATS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 728; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":729 */
  __pyx_1 = PyInt_FromLong(GL_COMPRESSED_TEXTURE_FORMATS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 729; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COMPRESSED_TEXTURE_FORMATS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 729; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":730 */
  __pyx_1 = PyInt_FromLong(GL_MULTISAMPLE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 730; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MULTISAMPLE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 730; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":731 */
  __pyx_1 = PyInt_FromLong(GL_SAMPLE_ALPHA_TO_COVERAGE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 731; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SAMPLE_ALPHA_TO_COVERAGE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 731; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":732 */
  __pyx_1 = PyInt_FromLong(GL_SAMPLE_ALPHA_TO_ONE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 732; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SAMPLE_ALPHA_TO_ONE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 732; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":733 */
  __pyx_1 = PyInt_FromLong(GL_SAMPLE_COVERAGE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 733; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SAMPLE_COVERAGE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 733; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":734 */
  __pyx_1 = PyInt_FromLong(GL_SAMPLE_BUFFERS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 734; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SAMPLE_BUFFERS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 734; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":735 */
  __pyx_1 = PyInt_FromLong(GL_SAMPLES); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 735; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SAMPLES, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 735; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":736 */
  __pyx_1 = PyInt_FromLong(GL_SAMPLE_COVERAGE_VALUE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 736; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SAMPLE_COVERAGE_VALUE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 736; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":737 */
  __pyx_1 = PyInt_FromLong(GL_SAMPLE_COVERAGE_INVERT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 737; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SAMPLE_COVERAGE_INVERT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 737; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":738 */
  __pyx_1 = PyInt_FromLong(GL_MULTISAMPLE_BIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 738; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_MULTISAMPLE_BIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 738; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":739 */
  __pyx_1 = PyInt_FromLong(GL_TRANSPOSE_MODELVIEW_MATRIX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 739; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TRANSPOSE_MODELVIEW_MATRIX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 739; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":740 */
  __pyx_1 = PyInt_FromLong(GL_TRANSPOSE_PROJECTION_MATRIX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 740; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TRANSPOSE_PROJECTION_MATRIX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 740; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":741 */
  __pyx_1 = PyInt_FromLong(GL_TRANSPOSE_TEXTURE_MATRIX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 741; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TRANSPOSE_TEXTURE_MATRIX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 741; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":742 */
  __pyx_1 = PyInt_FromLong(GL_TRANSPOSE_COLOR_MATRIX); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 742; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_TRANSPOSE_COLOR_MATRIX, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 742; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":743 */
  __pyx_1 = PyInt_FromLong(GL_COMBINE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 743; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COMBINE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 743; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":744 */
  __pyx_1 = PyInt_FromLong(GL_COMBINE_RGB); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 744; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COMBINE_RGB, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 744; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":745 */
  __pyx_1 = PyInt_FromLong(GL_COMBINE_ALPHA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 745; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_COMBINE_ALPHA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 745; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":746 */
  __pyx_1 = PyInt_FromLong(GL_SOURCE0_RGB); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 746; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SOURCE0_RGB, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 746; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":747 */
  __pyx_1 = PyInt_FromLong(GL_SOURCE1_RGB); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 747; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SOURCE1_RGB, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 747; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":748 */
  __pyx_1 = PyInt_FromLong(GL_SOURCE2_RGB); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 748; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SOURCE2_RGB, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 748; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":749 */
  __pyx_1 = PyInt_FromLong(GL_SOURCE0_ALPHA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 749; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SOURCE0_ALPHA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 749; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":750 */
  __pyx_1 = PyInt_FromLong(GL_SOURCE1_ALPHA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 750; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SOURCE1_ALPHA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 750; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":751 */
  __pyx_1 = PyInt_FromLong(GL_SOURCE2_ALPHA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 751; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SOURCE2_ALPHA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 751; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":752 */
  __pyx_1 = PyInt_FromLong(GL_OPERAND0_RGB); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 752; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_OPERAND0_RGB, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 752; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":753 */
  __pyx_1 = PyInt_FromLong(GL_OPERAND1_RGB); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 753; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_OPERAND1_RGB, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 753; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":754 */
  __pyx_1 = PyInt_FromLong(GL_OPERAND2_RGB); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 754; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_OPERAND2_RGB, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 754; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":755 */
  __pyx_1 = PyInt_FromLong(GL_OPERAND0_ALPHA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 755; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_OPERAND0_ALPHA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 755; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":756 */
  __pyx_1 = PyInt_FromLong(GL_OPERAND1_ALPHA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 756; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_OPERAND1_ALPHA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 756; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":757 */
  __pyx_1 = PyInt_FromLong(GL_OPERAND2_ALPHA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 757; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_OPERAND2_ALPHA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 757; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":758 */
  __pyx_1 = PyInt_FromLong(GL_RGB_SCALE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 758; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_RGB_SCALE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 758; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":759 */
  __pyx_1 = PyInt_FromLong(GL_ADD_SIGNED); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 759; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_ADD_SIGNED, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 759; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":760 */
  __pyx_1 = PyInt_FromLong(GL_INTERPOLATE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 760; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_INTERPOLATE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 760; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":761 */
  __pyx_1 = PyInt_FromLong(GL_SUBTRACT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 761; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_SUBTRACT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 761; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":762 */
  __pyx_1 = PyInt_FromLong(GL_CONSTANT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 762; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CONSTANT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 762; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":763 */
  __pyx_1 = PyInt_FromLong(GL_PRIMARY_COLOR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 763; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PRIMARY_COLOR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 763; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":764 */
  __pyx_1 = PyInt_FromLong(GL_PREVIOUS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 764; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_PREVIOUS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 764; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":765 */
  __pyx_1 = PyInt_FromLong(GL_DOT3_RGB); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 765; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DOT3_RGB, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 765; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":766 */
  __pyx_1 = PyInt_FromLong(GL_DOT3_RGBA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 766; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_DOT3_RGBA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 766; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":767 */
  __pyx_1 = PyInt_FromLong(GL_CLAMP_TO_BORDER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 767; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_GL_CLAMP_TO_BORDER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 767; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/opengl.pyx":820 */
  __pyx_1 = __Pyx_GetName(__pyx_m, __pyx_n_GL_COMPILE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 820; goto __pyx_L1;}
  __pyx_2 = PyInt_AsLong(__pyx_1); if (PyErr_Occurred()) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 820; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;
  __pyx_k1 = __pyx_2;

  /* "/home/jiba/src/soya/opengl.pyx":885 */
  return;
  __pyx_L1:;
  Py_XDECREF(__pyx_1);
  __Pyx_AddTraceback("opengl");
}

static char *__pyx_filenames[] = {
  "opengl.pyx",
};

/* Runtime support code */

static void __pyx_init_filenames(void) {
  __pyx_f = __pyx_filenames;
}

static PyObject *__Pyx_GetName(PyObject *dict, PyObject *name) {
    PyObject *result;
    result = PyObject_GetAttr(dict, name);
    if (!result)
        PyErr_SetObject(PyExc_NameError, name);
    return result;
}

static int __Pyx_InternStrings(__Pyx_InternTabEntry *t) {
    while (t->p) {
        *t->p = PyString_InternFromString(t->s);
        if (!*t->p)
            return -1;
        ++t;
    }
    return 0;
}

#include "compile.h"
#include "frameobject.h"
#include "traceback.h"

static void __Pyx_AddTraceback(char *funcname) {
    PyObject *py_srcfile = 0;
    PyObject *py_funcname = 0;
    PyObject *py_globals = 0;
    PyObject *empty_tuple = 0;
    PyObject *empty_string = 0;
    PyCodeObject *py_code = 0;
    PyFrameObject *py_frame = 0;
    
    py_srcfile = PyString_FromString(__pyx_filename);
    if (!py_srcfile) goto bad;
    py_funcname = PyString_FromString(funcname);
    if (!py_funcname) goto bad;
    py_globals = PyModule_GetDict(__pyx_m);
    if (!py_globals) goto bad;
    empty_tuple = PyTuple_New(0);
    if (!empty_tuple) goto bad;
    empty_string = PyString_FromString("");
    if (!empty_string) goto bad;
    py_code = PyCode_New(
        0,            /*int argcount,*/
        0,            /*int nlocals,*/
        0,            /*int stacksize,*/
        0,            /*int flags,*/
        empty_string, /*PyObject *code,*/
        empty_tuple,  /*PyObject *consts,*/
        empty_tuple,  /*PyObject *names,*/
        empty_tuple,  /*PyObject *varnames,*/
        empty_tuple,  /*PyObject *freevars,*/
        empty_tuple,  /*PyObject *cellvars,*/
        py_srcfile,   /*PyObject *filename,*/
        py_funcname,  /*PyObject *name,*/
        __pyx_lineno,   /*int firstlineno,*/
        empty_string  /*PyObject *lnotab*/
    );
    if (!py_code) goto bad;
    py_frame = PyFrame_New(
        PyThreadState_Get(), /*PyThreadState *tstate,*/
        py_code,             /*PyCodeObject *code,*/
        py_globals,          /*PyObject *globals,*/
        0                    /*PyObject *locals*/
    );
    if (!py_frame) goto bad;
    py_frame->f_lineno = __pyx_lineno;
    PyTraceBack_Here(py_frame);
bad:
    Py_XDECREF(py_srcfile);
    Py_XDECREF(py_funcname);
    Py_XDECREF(empty_tuple);
    Py_XDECREF(empty_string);
    Py_XDECREF(py_code);
    Py_XDECREF(py_frame);
}
