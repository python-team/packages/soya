/* 0.9.7.2 on Fri Aug 29 13:18:19 2008 */

#define PY_SSIZE_T_CLEAN
#include "Python.h"
#include "structmember.h"
#ifndef PY_LONG_LONG
  #define PY_LONG_LONG LONG_LONG
#endif
#if PY_VERSION_HEX < 0x02050000
  typedef int Py_ssize_t;
  #define PY_SSIZE_T_MAX INT_MAX
  #define PY_SSIZE_T_MIN INT_MIN
  #define PyInt_FromSsize_t(z) PyInt_FromLong(z)
  #define PyInt_AsSsize_t(o)	PyInt_AsLong(o)
#endif
#ifndef WIN32
  #ifndef __stdcall
    #define __stdcall
  #endif
  #ifndef __cdecl
    #define __cdecl
  #endif
#endif
#ifdef __cplusplus
#define __PYX_EXTERN_C extern "C"
#else
#define __PYX_EXTERN_C extern
#endif
#include <math.h>
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "stdint.h"
#include "SDL/SDL_endian.h"
#include "SDL/SDL.h"


typedef struct {PyObject **p; char *s;} __Pyx_InternTabEntry; /*proto*/
typedef struct {PyObject **p; char *s; long n;} __Pyx_StringTabEntry; /*proto*/

static PyObject *__pyx_m;
static PyObject *__pyx_b;
static int __pyx_lineno;
static char *__pyx_filename;
static char **__pyx_f;

static int __Pyx_InternStrings(__Pyx_InternTabEntry *t); /*proto*/

static void __Pyx_AddTraceback(char *funcname); /*proto*/

/* Declarations from sdlconst */



/* Implementation of sdlconst */

static PyObject *__pyx_n_SDL_ALLEVENTS;
static PyObject *__pyx_n_NOEVENT;
static PyObject *__pyx_n_ACTIVEEVENT;
static PyObject *__pyx_n_KEYDOWN;
static PyObject *__pyx_n_KEYUP;
static PyObject *__pyx_n_MOUSEMOTION;
static PyObject *__pyx_n_MOUSEBUTTONDOWN;
static PyObject *__pyx_n_MOUSEBUTTONUP;
static PyObject *__pyx_n_JOYAXISMOTION;
static PyObject *__pyx_n_JOYBALLMOTION;
static PyObject *__pyx_n_JOYHATMOTION;
static PyObject *__pyx_n_JOYBUTTONDOWN;
static PyObject *__pyx_n_JOYBUTTONUP;
static PyObject *__pyx_n_QUIT;
static PyObject *__pyx_n_SYSWMEVENT;
static PyObject *__pyx_n_EVENT_RESERVEDA;
static PyObject *__pyx_n_EVENT_RESERVEDB;
static PyObject *__pyx_n_VIDEORESIZE;
static PyObject *__pyx_n_VIDEOEXPOSE;
static PyObject *__pyx_n_USEREVENT;
static PyObject *__pyx_n_NUMEVENTS;
static PyObject *__pyx_n_MOD_NONE;
static PyObject *__pyx_n_MOD_LSHIFT;
static PyObject *__pyx_n_MOD_RSHIFT;
static PyObject *__pyx_n_MOD_SHIFT;
static PyObject *__pyx_n_MOD_LCTRL;
static PyObject *__pyx_n_MOD_RCTRL;
static PyObject *__pyx_n_MOD_CTRL;
static PyObject *__pyx_n_MOD_LALT;
static PyObject *__pyx_n_MOD_RALT;
static PyObject *__pyx_n_MOD_ALT;
static PyObject *__pyx_n_MOD_LMETA;
static PyObject *__pyx_n_MOD_RMETA;
static PyObject *__pyx_n_MOD_META;
static PyObject *__pyx_n_MOD_NUM;
static PyObject *__pyx_n_MOD_CAPS;
static PyObject *__pyx_n_MOD_MODE;
static PyObject *__pyx_n_MOD_RESERVED;
static PyObject *__pyx_n_K_UNKNOWN;
static PyObject *__pyx_n_K_FIRST;
static PyObject *__pyx_n_K_BACKSPACE;
static PyObject *__pyx_n_K_TAB;
static PyObject *__pyx_n_K_CLEAR;
static PyObject *__pyx_n_K_RETURN;
static PyObject *__pyx_n_K_PAUSE;
static PyObject *__pyx_n_K_ESCAPE;
static PyObject *__pyx_n_K_SPACE;
static PyObject *__pyx_n_K_EXCLAIM;
static PyObject *__pyx_n_K_QUOTEDBL;
static PyObject *__pyx_n_K_HASH;
static PyObject *__pyx_n_K_DOLLAR;
static PyObject *__pyx_n_K_AMPERSAND;
static PyObject *__pyx_n_K_QUOTE;
static PyObject *__pyx_n_K_LEFTPAREN;
static PyObject *__pyx_n_K_RIGHTPAREN;
static PyObject *__pyx_n_K_ASTERISK;
static PyObject *__pyx_n_K_PLUS;
static PyObject *__pyx_n_K_COMMA;
static PyObject *__pyx_n_K_MINUS;
static PyObject *__pyx_n_K_PERIOD;
static PyObject *__pyx_n_K_SLASH;
static PyObject *__pyx_n_K_0;
static PyObject *__pyx_n_K_1;
static PyObject *__pyx_n_K_2;
static PyObject *__pyx_n_K_3;
static PyObject *__pyx_n_K_4;
static PyObject *__pyx_n_K_5;
static PyObject *__pyx_n_K_6;
static PyObject *__pyx_n_K_7;
static PyObject *__pyx_n_K_8;
static PyObject *__pyx_n_K_9;
static PyObject *__pyx_n_K_COLON;
static PyObject *__pyx_n_K_SEMICOLON;
static PyObject *__pyx_n_K_LESS;
static PyObject *__pyx_n_K_EQUALS;
static PyObject *__pyx_n_K_GREATER;
static PyObject *__pyx_n_K_QUESTION;
static PyObject *__pyx_n_K_AT;
static PyObject *__pyx_n_K_LEFTBRACKET;
static PyObject *__pyx_n_K_BACKSLASH;
static PyObject *__pyx_n_K_RIGHTBRACKET;
static PyObject *__pyx_n_K_CARET;
static PyObject *__pyx_n_K_UNDERSCORE;
static PyObject *__pyx_n_K_BACKQUOTE;
static PyObject *__pyx_n_K_a;
static PyObject *__pyx_n_K_b;
static PyObject *__pyx_n_K_c;
static PyObject *__pyx_n_K_d;
static PyObject *__pyx_n_K_e;
static PyObject *__pyx_n_K_f;
static PyObject *__pyx_n_K_g;
static PyObject *__pyx_n_K_h;
static PyObject *__pyx_n_K_i;
static PyObject *__pyx_n_K_j;
static PyObject *__pyx_n_K_k;
static PyObject *__pyx_n_K_l;
static PyObject *__pyx_n_K_m;
static PyObject *__pyx_n_K_n;
static PyObject *__pyx_n_K_o;
static PyObject *__pyx_n_K_p;
static PyObject *__pyx_n_K_q;
static PyObject *__pyx_n_K_r;
static PyObject *__pyx_n_K_s;
static PyObject *__pyx_n_K_t;
static PyObject *__pyx_n_K_u;
static PyObject *__pyx_n_K_v;
static PyObject *__pyx_n_K_w;
static PyObject *__pyx_n_K_x;
static PyObject *__pyx_n_K_y;
static PyObject *__pyx_n_K_z;
static PyObject *__pyx_n_K_DELETE;
static PyObject *__pyx_n_K_WORLD_0;
static PyObject *__pyx_n_K_WORLD_1;
static PyObject *__pyx_n_K_WORLD_2;
static PyObject *__pyx_n_K_WORLD_3;
static PyObject *__pyx_n_K_WORLD_4;
static PyObject *__pyx_n_K_WORLD_5;
static PyObject *__pyx_n_K_WORLD_6;
static PyObject *__pyx_n_K_WORLD_7;
static PyObject *__pyx_n_K_WORLD_8;
static PyObject *__pyx_n_K_WORLD_9;
static PyObject *__pyx_n_K_WORLD_10;
static PyObject *__pyx_n_K_WORLD_11;
static PyObject *__pyx_n_K_WORLD_12;
static PyObject *__pyx_n_K_WORLD_13;
static PyObject *__pyx_n_K_WORLD_14;
static PyObject *__pyx_n_K_WORLD_15;
static PyObject *__pyx_n_K_WORLD_16;
static PyObject *__pyx_n_K_WORLD_17;
static PyObject *__pyx_n_K_WORLD_18;
static PyObject *__pyx_n_K_WORLD_19;
static PyObject *__pyx_n_K_WORLD_20;
static PyObject *__pyx_n_K_WORLD_21;
static PyObject *__pyx_n_K_WORLD_22;
static PyObject *__pyx_n_K_WORLD_23;
static PyObject *__pyx_n_K_WORLD_24;
static PyObject *__pyx_n_K_WORLD_25;
static PyObject *__pyx_n_K_WORLD_26;
static PyObject *__pyx_n_K_WORLD_27;
static PyObject *__pyx_n_K_WORLD_28;
static PyObject *__pyx_n_K_WORLD_29;
static PyObject *__pyx_n_K_WORLD_30;
static PyObject *__pyx_n_K_WORLD_31;
static PyObject *__pyx_n_K_WORLD_32;
static PyObject *__pyx_n_K_WORLD_33;
static PyObject *__pyx_n_K_WORLD_34;
static PyObject *__pyx_n_K_WORLD_35;
static PyObject *__pyx_n_K_WORLD_36;
static PyObject *__pyx_n_K_WORLD_37;
static PyObject *__pyx_n_K_WORLD_38;
static PyObject *__pyx_n_K_WORLD_39;
static PyObject *__pyx_n_K_WORLD_40;
static PyObject *__pyx_n_K_WORLD_41;
static PyObject *__pyx_n_K_WORLD_42;
static PyObject *__pyx_n_K_WORLD_43;
static PyObject *__pyx_n_K_WORLD_44;
static PyObject *__pyx_n_K_WORLD_45;
static PyObject *__pyx_n_K_WORLD_46;
static PyObject *__pyx_n_K_WORLD_47;
static PyObject *__pyx_n_K_WORLD_48;
static PyObject *__pyx_n_K_WORLD_49;
static PyObject *__pyx_n_K_WORLD_50;
static PyObject *__pyx_n_K_WORLD_51;
static PyObject *__pyx_n_K_WORLD_52;
static PyObject *__pyx_n_K_WORLD_53;
static PyObject *__pyx_n_K_WORLD_54;
static PyObject *__pyx_n_K_WORLD_55;
static PyObject *__pyx_n_K_WORLD_56;
static PyObject *__pyx_n_K_WORLD_57;
static PyObject *__pyx_n_K_WORLD_58;
static PyObject *__pyx_n_K_WORLD_59;
static PyObject *__pyx_n_K_WORLD_60;
static PyObject *__pyx_n_K_WORLD_61;
static PyObject *__pyx_n_K_WORLD_62;
static PyObject *__pyx_n_K_WORLD_63;
static PyObject *__pyx_n_K_WORLD_64;
static PyObject *__pyx_n_K_WORLD_65;
static PyObject *__pyx_n_K_WORLD_66;
static PyObject *__pyx_n_K_WORLD_67;
static PyObject *__pyx_n_K_WORLD_68;
static PyObject *__pyx_n_K_WORLD_69;
static PyObject *__pyx_n_K_WORLD_70;
static PyObject *__pyx_n_K_WORLD_71;
static PyObject *__pyx_n_K_WORLD_72;
static PyObject *__pyx_n_K_WORLD_73;
static PyObject *__pyx_n_K_WORLD_74;
static PyObject *__pyx_n_K_WORLD_75;
static PyObject *__pyx_n_K_WORLD_76;
static PyObject *__pyx_n_K_WORLD_77;
static PyObject *__pyx_n_K_WORLD_78;
static PyObject *__pyx_n_K_WORLD_79;
static PyObject *__pyx_n_K_WORLD_80;
static PyObject *__pyx_n_K_WORLD_81;
static PyObject *__pyx_n_K_WORLD_82;
static PyObject *__pyx_n_K_WORLD_83;
static PyObject *__pyx_n_K_WORLD_84;
static PyObject *__pyx_n_K_WORLD_85;
static PyObject *__pyx_n_K_WORLD_86;
static PyObject *__pyx_n_K_WORLD_87;
static PyObject *__pyx_n_K_WORLD_88;
static PyObject *__pyx_n_K_WORLD_89;
static PyObject *__pyx_n_K_WORLD_90;
static PyObject *__pyx_n_K_WORLD_91;
static PyObject *__pyx_n_K_WORLD_92;
static PyObject *__pyx_n_K_WORLD_93;
static PyObject *__pyx_n_K_WORLD_94;
static PyObject *__pyx_n_K_WORLD_95;
static PyObject *__pyx_n_K_KP0;
static PyObject *__pyx_n_K_KP1;
static PyObject *__pyx_n_K_KP2;
static PyObject *__pyx_n_K_KP3;
static PyObject *__pyx_n_K_KP4;
static PyObject *__pyx_n_K_KP5;
static PyObject *__pyx_n_K_KP6;
static PyObject *__pyx_n_K_KP7;
static PyObject *__pyx_n_K_KP8;
static PyObject *__pyx_n_K_KP9;
static PyObject *__pyx_n_K_KP_PERIOD;
static PyObject *__pyx_n_K_KP_DIVIDE;
static PyObject *__pyx_n_K_KP_MULTIPLY;
static PyObject *__pyx_n_K_KP_MINUS;
static PyObject *__pyx_n_K_KP_PLUS;
static PyObject *__pyx_n_K_KP_ENTER;
static PyObject *__pyx_n_K_KP_EQUALS;
static PyObject *__pyx_n_K_UP;
static PyObject *__pyx_n_K_DOWN;
static PyObject *__pyx_n_K_RIGHT;
static PyObject *__pyx_n_K_LEFT;
static PyObject *__pyx_n_K_INSERT;
static PyObject *__pyx_n_K_HOME;
static PyObject *__pyx_n_K_END;
static PyObject *__pyx_n_K_PAGEUP;
static PyObject *__pyx_n_K_PAGEDOWN;
static PyObject *__pyx_n_K_F1;
static PyObject *__pyx_n_K_F2;
static PyObject *__pyx_n_K_F3;
static PyObject *__pyx_n_K_F4;
static PyObject *__pyx_n_K_F5;
static PyObject *__pyx_n_K_F6;
static PyObject *__pyx_n_K_F7;
static PyObject *__pyx_n_K_F8;
static PyObject *__pyx_n_K_F9;
static PyObject *__pyx_n_K_F10;
static PyObject *__pyx_n_K_F11;
static PyObject *__pyx_n_K_F12;
static PyObject *__pyx_n_K_F13;
static PyObject *__pyx_n_K_F14;
static PyObject *__pyx_n_K_F15;
static PyObject *__pyx_n_K_NUMLOCK;
static PyObject *__pyx_n_K_CAPSLOCK;
static PyObject *__pyx_n_K_SCROLLOCK;
static PyObject *__pyx_n_K_RSHIFT;
static PyObject *__pyx_n_K_LSHIFT;
static PyObject *__pyx_n_K_RCTRL;
static PyObject *__pyx_n_K_LCTRL;
static PyObject *__pyx_n_K_RALT;
static PyObject *__pyx_n_K_LALT;
static PyObject *__pyx_n_K_RMETA;
static PyObject *__pyx_n_K_LMETA;
static PyObject *__pyx_n_K_LSUPER;
static PyObject *__pyx_n_K_RSUPER;
static PyObject *__pyx_n_K_MODE;
static PyObject *__pyx_n_K_COMPOSE;
static PyObject *__pyx_n_K_HELP;
static PyObject *__pyx_n_K_PRINT;
static PyObject *__pyx_n_K_SYSREQ;
static PyObject *__pyx_n_K_BREAK;
static PyObject *__pyx_n_K_MENU;
static PyObject *__pyx_n_K_POWER;
static PyObject *__pyx_n_K_EURO;
static PyObject *__pyx_n_K_UNDO;
static PyObject *__pyx_n_BUTTON_LEFT;
static PyObject *__pyx_n_BUTTON_MIDDLE;
static PyObject *__pyx_n_BUTTON_RIGHT;
static PyObject *__pyx_n_BUTTON_WHEELUP;
static PyObject *__pyx_n_BUTTON_WHEELDOWN;

static __Pyx_InternTabEntry __pyx_intern_tab[] = {
  {&__pyx_n_ACTIVEEVENT, "ACTIVEEVENT"},
  {&__pyx_n_BUTTON_LEFT, "BUTTON_LEFT"},
  {&__pyx_n_BUTTON_MIDDLE, "BUTTON_MIDDLE"},
  {&__pyx_n_BUTTON_RIGHT, "BUTTON_RIGHT"},
  {&__pyx_n_BUTTON_WHEELDOWN, "BUTTON_WHEELDOWN"},
  {&__pyx_n_BUTTON_WHEELUP, "BUTTON_WHEELUP"},
  {&__pyx_n_EVENT_RESERVEDA, "EVENT_RESERVEDA"},
  {&__pyx_n_EVENT_RESERVEDB, "EVENT_RESERVEDB"},
  {&__pyx_n_JOYAXISMOTION, "JOYAXISMOTION"},
  {&__pyx_n_JOYBALLMOTION, "JOYBALLMOTION"},
  {&__pyx_n_JOYBUTTONDOWN, "JOYBUTTONDOWN"},
  {&__pyx_n_JOYBUTTONUP, "JOYBUTTONUP"},
  {&__pyx_n_JOYHATMOTION, "JOYHATMOTION"},
  {&__pyx_n_KEYDOWN, "KEYDOWN"},
  {&__pyx_n_KEYUP, "KEYUP"},
  {&__pyx_n_K_0, "K_0"},
  {&__pyx_n_K_1, "K_1"},
  {&__pyx_n_K_2, "K_2"},
  {&__pyx_n_K_3, "K_3"},
  {&__pyx_n_K_4, "K_4"},
  {&__pyx_n_K_5, "K_5"},
  {&__pyx_n_K_6, "K_6"},
  {&__pyx_n_K_7, "K_7"},
  {&__pyx_n_K_8, "K_8"},
  {&__pyx_n_K_9, "K_9"},
  {&__pyx_n_K_AMPERSAND, "K_AMPERSAND"},
  {&__pyx_n_K_ASTERISK, "K_ASTERISK"},
  {&__pyx_n_K_AT, "K_AT"},
  {&__pyx_n_K_BACKQUOTE, "K_BACKQUOTE"},
  {&__pyx_n_K_BACKSLASH, "K_BACKSLASH"},
  {&__pyx_n_K_BACKSPACE, "K_BACKSPACE"},
  {&__pyx_n_K_BREAK, "K_BREAK"},
  {&__pyx_n_K_CAPSLOCK, "K_CAPSLOCK"},
  {&__pyx_n_K_CARET, "K_CARET"},
  {&__pyx_n_K_CLEAR, "K_CLEAR"},
  {&__pyx_n_K_COLON, "K_COLON"},
  {&__pyx_n_K_COMMA, "K_COMMA"},
  {&__pyx_n_K_COMPOSE, "K_COMPOSE"},
  {&__pyx_n_K_DELETE, "K_DELETE"},
  {&__pyx_n_K_DOLLAR, "K_DOLLAR"},
  {&__pyx_n_K_DOWN, "K_DOWN"},
  {&__pyx_n_K_END, "K_END"},
  {&__pyx_n_K_EQUALS, "K_EQUALS"},
  {&__pyx_n_K_ESCAPE, "K_ESCAPE"},
  {&__pyx_n_K_EURO, "K_EURO"},
  {&__pyx_n_K_EXCLAIM, "K_EXCLAIM"},
  {&__pyx_n_K_F1, "K_F1"},
  {&__pyx_n_K_F10, "K_F10"},
  {&__pyx_n_K_F11, "K_F11"},
  {&__pyx_n_K_F12, "K_F12"},
  {&__pyx_n_K_F13, "K_F13"},
  {&__pyx_n_K_F14, "K_F14"},
  {&__pyx_n_K_F15, "K_F15"},
  {&__pyx_n_K_F2, "K_F2"},
  {&__pyx_n_K_F3, "K_F3"},
  {&__pyx_n_K_F4, "K_F4"},
  {&__pyx_n_K_F5, "K_F5"},
  {&__pyx_n_K_F6, "K_F6"},
  {&__pyx_n_K_F7, "K_F7"},
  {&__pyx_n_K_F8, "K_F8"},
  {&__pyx_n_K_F9, "K_F9"},
  {&__pyx_n_K_FIRST, "K_FIRST"},
  {&__pyx_n_K_GREATER, "K_GREATER"},
  {&__pyx_n_K_HASH, "K_HASH"},
  {&__pyx_n_K_HELP, "K_HELP"},
  {&__pyx_n_K_HOME, "K_HOME"},
  {&__pyx_n_K_INSERT, "K_INSERT"},
  {&__pyx_n_K_KP0, "K_KP0"},
  {&__pyx_n_K_KP1, "K_KP1"},
  {&__pyx_n_K_KP2, "K_KP2"},
  {&__pyx_n_K_KP3, "K_KP3"},
  {&__pyx_n_K_KP4, "K_KP4"},
  {&__pyx_n_K_KP5, "K_KP5"},
  {&__pyx_n_K_KP6, "K_KP6"},
  {&__pyx_n_K_KP7, "K_KP7"},
  {&__pyx_n_K_KP8, "K_KP8"},
  {&__pyx_n_K_KP9, "K_KP9"},
  {&__pyx_n_K_KP_DIVIDE, "K_KP_DIVIDE"},
  {&__pyx_n_K_KP_ENTER, "K_KP_ENTER"},
  {&__pyx_n_K_KP_EQUALS, "K_KP_EQUALS"},
  {&__pyx_n_K_KP_MINUS, "K_KP_MINUS"},
  {&__pyx_n_K_KP_MULTIPLY, "K_KP_MULTIPLY"},
  {&__pyx_n_K_KP_PERIOD, "K_KP_PERIOD"},
  {&__pyx_n_K_KP_PLUS, "K_KP_PLUS"},
  {&__pyx_n_K_LALT, "K_LALT"},
  {&__pyx_n_K_LCTRL, "K_LCTRL"},
  {&__pyx_n_K_LEFT, "K_LEFT"},
  {&__pyx_n_K_LEFTBRACKET, "K_LEFTBRACKET"},
  {&__pyx_n_K_LEFTPAREN, "K_LEFTPAREN"},
  {&__pyx_n_K_LESS, "K_LESS"},
  {&__pyx_n_K_LMETA, "K_LMETA"},
  {&__pyx_n_K_LSHIFT, "K_LSHIFT"},
  {&__pyx_n_K_LSUPER, "K_LSUPER"},
  {&__pyx_n_K_MENU, "K_MENU"},
  {&__pyx_n_K_MINUS, "K_MINUS"},
  {&__pyx_n_K_MODE, "K_MODE"},
  {&__pyx_n_K_NUMLOCK, "K_NUMLOCK"},
  {&__pyx_n_K_PAGEDOWN, "K_PAGEDOWN"},
  {&__pyx_n_K_PAGEUP, "K_PAGEUP"},
  {&__pyx_n_K_PAUSE, "K_PAUSE"},
  {&__pyx_n_K_PERIOD, "K_PERIOD"},
  {&__pyx_n_K_PLUS, "K_PLUS"},
  {&__pyx_n_K_POWER, "K_POWER"},
  {&__pyx_n_K_PRINT, "K_PRINT"},
  {&__pyx_n_K_QUESTION, "K_QUESTION"},
  {&__pyx_n_K_QUOTE, "K_QUOTE"},
  {&__pyx_n_K_QUOTEDBL, "K_QUOTEDBL"},
  {&__pyx_n_K_RALT, "K_RALT"},
  {&__pyx_n_K_RCTRL, "K_RCTRL"},
  {&__pyx_n_K_RETURN, "K_RETURN"},
  {&__pyx_n_K_RIGHT, "K_RIGHT"},
  {&__pyx_n_K_RIGHTBRACKET, "K_RIGHTBRACKET"},
  {&__pyx_n_K_RIGHTPAREN, "K_RIGHTPAREN"},
  {&__pyx_n_K_RMETA, "K_RMETA"},
  {&__pyx_n_K_RSHIFT, "K_RSHIFT"},
  {&__pyx_n_K_RSUPER, "K_RSUPER"},
  {&__pyx_n_K_SCROLLOCK, "K_SCROLLOCK"},
  {&__pyx_n_K_SEMICOLON, "K_SEMICOLON"},
  {&__pyx_n_K_SLASH, "K_SLASH"},
  {&__pyx_n_K_SPACE, "K_SPACE"},
  {&__pyx_n_K_SYSREQ, "K_SYSREQ"},
  {&__pyx_n_K_TAB, "K_TAB"},
  {&__pyx_n_K_UNDERSCORE, "K_UNDERSCORE"},
  {&__pyx_n_K_UNDO, "K_UNDO"},
  {&__pyx_n_K_UNKNOWN, "K_UNKNOWN"},
  {&__pyx_n_K_UP, "K_UP"},
  {&__pyx_n_K_WORLD_0, "K_WORLD_0"},
  {&__pyx_n_K_WORLD_1, "K_WORLD_1"},
  {&__pyx_n_K_WORLD_10, "K_WORLD_10"},
  {&__pyx_n_K_WORLD_11, "K_WORLD_11"},
  {&__pyx_n_K_WORLD_12, "K_WORLD_12"},
  {&__pyx_n_K_WORLD_13, "K_WORLD_13"},
  {&__pyx_n_K_WORLD_14, "K_WORLD_14"},
  {&__pyx_n_K_WORLD_15, "K_WORLD_15"},
  {&__pyx_n_K_WORLD_16, "K_WORLD_16"},
  {&__pyx_n_K_WORLD_17, "K_WORLD_17"},
  {&__pyx_n_K_WORLD_18, "K_WORLD_18"},
  {&__pyx_n_K_WORLD_19, "K_WORLD_19"},
  {&__pyx_n_K_WORLD_2, "K_WORLD_2"},
  {&__pyx_n_K_WORLD_20, "K_WORLD_20"},
  {&__pyx_n_K_WORLD_21, "K_WORLD_21"},
  {&__pyx_n_K_WORLD_22, "K_WORLD_22"},
  {&__pyx_n_K_WORLD_23, "K_WORLD_23"},
  {&__pyx_n_K_WORLD_24, "K_WORLD_24"},
  {&__pyx_n_K_WORLD_25, "K_WORLD_25"},
  {&__pyx_n_K_WORLD_26, "K_WORLD_26"},
  {&__pyx_n_K_WORLD_27, "K_WORLD_27"},
  {&__pyx_n_K_WORLD_28, "K_WORLD_28"},
  {&__pyx_n_K_WORLD_29, "K_WORLD_29"},
  {&__pyx_n_K_WORLD_3, "K_WORLD_3"},
  {&__pyx_n_K_WORLD_30, "K_WORLD_30"},
  {&__pyx_n_K_WORLD_31, "K_WORLD_31"},
  {&__pyx_n_K_WORLD_32, "K_WORLD_32"},
  {&__pyx_n_K_WORLD_33, "K_WORLD_33"},
  {&__pyx_n_K_WORLD_34, "K_WORLD_34"},
  {&__pyx_n_K_WORLD_35, "K_WORLD_35"},
  {&__pyx_n_K_WORLD_36, "K_WORLD_36"},
  {&__pyx_n_K_WORLD_37, "K_WORLD_37"},
  {&__pyx_n_K_WORLD_38, "K_WORLD_38"},
  {&__pyx_n_K_WORLD_39, "K_WORLD_39"},
  {&__pyx_n_K_WORLD_4, "K_WORLD_4"},
  {&__pyx_n_K_WORLD_40, "K_WORLD_40"},
  {&__pyx_n_K_WORLD_41, "K_WORLD_41"},
  {&__pyx_n_K_WORLD_42, "K_WORLD_42"},
  {&__pyx_n_K_WORLD_43, "K_WORLD_43"},
  {&__pyx_n_K_WORLD_44, "K_WORLD_44"},
  {&__pyx_n_K_WORLD_45, "K_WORLD_45"},
  {&__pyx_n_K_WORLD_46, "K_WORLD_46"},
  {&__pyx_n_K_WORLD_47, "K_WORLD_47"},
  {&__pyx_n_K_WORLD_48, "K_WORLD_48"},
  {&__pyx_n_K_WORLD_49, "K_WORLD_49"},
  {&__pyx_n_K_WORLD_5, "K_WORLD_5"},
  {&__pyx_n_K_WORLD_50, "K_WORLD_50"},
  {&__pyx_n_K_WORLD_51, "K_WORLD_51"},
  {&__pyx_n_K_WORLD_52, "K_WORLD_52"},
  {&__pyx_n_K_WORLD_53, "K_WORLD_53"},
  {&__pyx_n_K_WORLD_54, "K_WORLD_54"},
  {&__pyx_n_K_WORLD_55, "K_WORLD_55"},
  {&__pyx_n_K_WORLD_56, "K_WORLD_56"},
  {&__pyx_n_K_WORLD_57, "K_WORLD_57"},
  {&__pyx_n_K_WORLD_58, "K_WORLD_58"},
  {&__pyx_n_K_WORLD_59, "K_WORLD_59"},
  {&__pyx_n_K_WORLD_6, "K_WORLD_6"},
  {&__pyx_n_K_WORLD_60, "K_WORLD_60"},
  {&__pyx_n_K_WORLD_61, "K_WORLD_61"},
  {&__pyx_n_K_WORLD_62, "K_WORLD_62"},
  {&__pyx_n_K_WORLD_63, "K_WORLD_63"},
  {&__pyx_n_K_WORLD_64, "K_WORLD_64"},
  {&__pyx_n_K_WORLD_65, "K_WORLD_65"},
  {&__pyx_n_K_WORLD_66, "K_WORLD_66"},
  {&__pyx_n_K_WORLD_67, "K_WORLD_67"},
  {&__pyx_n_K_WORLD_68, "K_WORLD_68"},
  {&__pyx_n_K_WORLD_69, "K_WORLD_69"},
  {&__pyx_n_K_WORLD_7, "K_WORLD_7"},
  {&__pyx_n_K_WORLD_70, "K_WORLD_70"},
  {&__pyx_n_K_WORLD_71, "K_WORLD_71"},
  {&__pyx_n_K_WORLD_72, "K_WORLD_72"},
  {&__pyx_n_K_WORLD_73, "K_WORLD_73"},
  {&__pyx_n_K_WORLD_74, "K_WORLD_74"},
  {&__pyx_n_K_WORLD_75, "K_WORLD_75"},
  {&__pyx_n_K_WORLD_76, "K_WORLD_76"},
  {&__pyx_n_K_WORLD_77, "K_WORLD_77"},
  {&__pyx_n_K_WORLD_78, "K_WORLD_78"},
  {&__pyx_n_K_WORLD_79, "K_WORLD_79"},
  {&__pyx_n_K_WORLD_8, "K_WORLD_8"},
  {&__pyx_n_K_WORLD_80, "K_WORLD_80"},
  {&__pyx_n_K_WORLD_81, "K_WORLD_81"},
  {&__pyx_n_K_WORLD_82, "K_WORLD_82"},
  {&__pyx_n_K_WORLD_83, "K_WORLD_83"},
  {&__pyx_n_K_WORLD_84, "K_WORLD_84"},
  {&__pyx_n_K_WORLD_85, "K_WORLD_85"},
  {&__pyx_n_K_WORLD_86, "K_WORLD_86"},
  {&__pyx_n_K_WORLD_87, "K_WORLD_87"},
  {&__pyx_n_K_WORLD_88, "K_WORLD_88"},
  {&__pyx_n_K_WORLD_89, "K_WORLD_89"},
  {&__pyx_n_K_WORLD_9, "K_WORLD_9"},
  {&__pyx_n_K_WORLD_90, "K_WORLD_90"},
  {&__pyx_n_K_WORLD_91, "K_WORLD_91"},
  {&__pyx_n_K_WORLD_92, "K_WORLD_92"},
  {&__pyx_n_K_WORLD_93, "K_WORLD_93"},
  {&__pyx_n_K_WORLD_94, "K_WORLD_94"},
  {&__pyx_n_K_WORLD_95, "K_WORLD_95"},
  {&__pyx_n_K_a, "K_a"},
  {&__pyx_n_K_b, "K_b"},
  {&__pyx_n_K_c, "K_c"},
  {&__pyx_n_K_d, "K_d"},
  {&__pyx_n_K_e, "K_e"},
  {&__pyx_n_K_f, "K_f"},
  {&__pyx_n_K_g, "K_g"},
  {&__pyx_n_K_h, "K_h"},
  {&__pyx_n_K_i, "K_i"},
  {&__pyx_n_K_j, "K_j"},
  {&__pyx_n_K_k, "K_k"},
  {&__pyx_n_K_l, "K_l"},
  {&__pyx_n_K_m, "K_m"},
  {&__pyx_n_K_n, "K_n"},
  {&__pyx_n_K_o, "K_o"},
  {&__pyx_n_K_p, "K_p"},
  {&__pyx_n_K_q, "K_q"},
  {&__pyx_n_K_r, "K_r"},
  {&__pyx_n_K_s, "K_s"},
  {&__pyx_n_K_t, "K_t"},
  {&__pyx_n_K_u, "K_u"},
  {&__pyx_n_K_v, "K_v"},
  {&__pyx_n_K_w, "K_w"},
  {&__pyx_n_K_x, "K_x"},
  {&__pyx_n_K_y, "K_y"},
  {&__pyx_n_K_z, "K_z"},
  {&__pyx_n_MOD_ALT, "MOD_ALT"},
  {&__pyx_n_MOD_CAPS, "MOD_CAPS"},
  {&__pyx_n_MOD_CTRL, "MOD_CTRL"},
  {&__pyx_n_MOD_LALT, "MOD_LALT"},
  {&__pyx_n_MOD_LCTRL, "MOD_LCTRL"},
  {&__pyx_n_MOD_LMETA, "MOD_LMETA"},
  {&__pyx_n_MOD_LSHIFT, "MOD_LSHIFT"},
  {&__pyx_n_MOD_META, "MOD_META"},
  {&__pyx_n_MOD_MODE, "MOD_MODE"},
  {&__pyx_n_MOD_NONE, "MOD_NONE"},
  {&__pyx_n_MOD_NUM, "MOD_NUM"},
  {&__pyx_n_MOD_RALT, "MOD_RALT"},
  {&__pyx_n_MOD_RCTRL, "MOD_RCTRL"},
  {&__pyx_n_MOD_RESERVED, "MOD_RESERVED"},
  {&__pyx_n_MOD_RMETA, "MOD_RMETA"},
  {&__pyx_n_MOD_RSHIFT, "MOD_RSHIFT"},
  {&__pyx_n_MOD_SHIFT, "MOD_SHIFT"},
  {&__pyx_n_MOUSEBUTTONDOWN, "MOUSEBUTTONDOWN"},
  {&__pyx_n_MOUSEBUTTONUP, "MOUSEBUTTONUP"},
  {&__pyx_n_MOUSEMOTION, "MOUSEMOTION"},
  {&__pyx_n_NOEVENT, "NOEVENT"},
  {&__pyx_n_NUMEVENTS, "NUMEVENTS"},
  {&__pyx_n_QUIT, "QUIT"},
  {&__pyx_n_SDL_ALLEVENTS, "SDL_ALLEVENTS"},
  {&__pyx_n_SYSWMEVENT, "SYSWMEVENT"},
  {&__pyx_n_USEREVENT, "USEREVENT"},
  {&__pyx_n_VIDEOEXPOSE, "VIDEOEXPOSE"},
  {&__pyx_n_VIDEORESIZE, "VIDEORESIZE"},
  {0, 0}
};

static struct PyMethodDef __pyx_methods[] = {
  {0, 0, 0, 0}
};

static void __pyx_init_filenames(void); /*proto*/

PyMODINIT_FUNC initsdlconst(void); /*proto*/
PyMODINIT_FUNC initsdlconst(void) {
  PyObject *__pyx_1 = 0;
  __pyx_init_filenames();
  __pyx_m = Py_InitModule4("sdlconst", __pyx_methods, 0, 0, PYTHON_API_VERSION);
  if (!__pyx_m) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 21; goto __pyx_L1;};
  Py_INCREF(__pyx_m);
  __pyx_b = PyImport_AddModule("__builtin__");
  if (!__pyx_b) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 21; goto __pyx_L1;};
  if (PyObject_SetAttrString(__pyx_m, "__builtins__", __pyx_b) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 21; goto __pyx_L1;};
  if (__Pyx_InternStrings(__pyx_intern_tab) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 21; goto __pyx_L1;};

  /* "/home/jiba/src/soya/sdl.pxd":25 */
  __pyx_1 = PyInt_FromLong(0xFFFFFFFF); if (!__pyx_1) {__pyx_filename = __pyx_f[1]; __pyx_lineno = 25; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_SDL_ALLEVENTS, __pyx_1) < 0) {__pyx_filename = __pyx_f[1]; __pyx_lineno = 25; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":25 */
  __pyx_1 = PyInt_FromLong(SDL_NOEVENT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 25; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_NOEVENT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 25; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":26 */
  __pyx_1 = PyInt_FromLong(SDL_ACTIVEEVENT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 26; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_ACTIVEEVENT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 26; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":27 */
  __pyx_1 = PyInt_FromLong(SDL_KEYDOWN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 27; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_KEYDOWN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 27; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":28 */
  __pyx_1 = PyInt_FromLong(SDL_KEYUP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 28; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_KEYUP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 28; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":29 */
  __pyx_1 = PyInt_FromLong(SDL_MOUSEMOTION); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 29; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOUSEMOTION, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 29; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":30 */
  __pyx_1 = PyInt_FromLong(SDL_MOUSEBUTTONDOWN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 30; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOUSEBUTTONDOWN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 30; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":31 */
  __pyx_1 = PyInt_FromLong(SDL_MOUSEBUTTONUP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 31; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOUSEBUTTONUP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 31; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":32 */
  __pyx_1 = PyInt_FromLong(SDL_JOYAXISMOTION); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 32; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_JOYAXISMOTION, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 32; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":33 */
  __pyx_1 = PyInt_FromLong(SDL_JOYBALLMOTION); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 33; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_JOYBALLMOTION, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 33; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":34 */
  __pyx_1 = PyInt_FromLong(SDL_JOYHATMOTION); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 34; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_JOYHATMOTION, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 34; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":35 */
  __pyx_1 = PyInt_FromLong(SDL_JOYBUTTONDOWN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 35; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_JOYBUTTONDOWN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 35; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":36 */
  __pyx_1 = PyInt_FromLong(SDL_JOYBUTTONUP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 36; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_JOYBUTTONUP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 36; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":37 */
  __pyx_1 = PyInt_FromLong(SDL_QUIT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 37; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_QUIT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 37; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":38 */
  __pyx_1 = PyInt_FromLong(SDL_SYSWMEVENT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 38; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_SYSWMEVENT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 38; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":39 */
  __pyx_1 = PyInt_FromLong(SDL_EVENT_RESERVEDA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 39; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_EVENT_RESERVEDA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 39; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":40 */
  __pyx_1 = PyInt_FromLong(SDL_EVENT_RESERVEDB); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 40; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_EVENT_RESERVEDB, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 40; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":41 */
  __pyx_1 = PyInt_FromLong(SDL_VIDEORESIZE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 41; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_VIDEORESIZE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 41; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":42 */
  __pyx_1 = PyInt_FromLong(SDL_VIDEOEXPOSE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 42; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_VIDEOEXPOSE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 42; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":43 */
  __pyx_1 = PyInt_FromLong(SDL_USEREVENT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 43; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_USEREVENT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 43; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":44 */
  __pyx_1 = PyInt_FromLong(SDL_NUMEVENTS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 44; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_NUMEVENTS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 44; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":46 */
  __pyx_1 = PyInt_FromLong(KMOD_NONE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 46; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOD_NONE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 46; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":47 */
  __pyx_1 = PyInt_FromLong(KMOD_LSHIFT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 47; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOD_LSHIFT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 47; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":48 */
  __pyx_1 = PyInt_FromLong(KMOD_RSHIFT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 48; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOD_RSHIFT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 48; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":49 */
  __pyx_1 = PyInt_FromLong((KMOD_LSHIFT | KMOD_RSHIFT)); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 49; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOD_SHIFT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 49; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":50 */
  __pyx_1 = PyInt_FromLong(KMOD_LCTRL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 50; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOD_LCTRL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 50; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":51 */
  __pyx_1 = PyInt_FromLong(KMOD_RCTRL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 51; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOD_RCTRL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 51; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":52 */
  __pyx_1 = PyInt_FromLong((KMOD_LCTRL | KMOD_RCTRL)); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 52; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOD_CTRL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 52; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":53 */
  __pyx_1 = PyInt_FromLong(KMOD_LALT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 53; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOD_LALT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 53; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":54 */
  __pyx_1 = PyInt_FromLong(KMOD_RALT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 54; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOD_RALT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 54; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":55 */
  __pyx_1 = PyInt_FromLong((KMOD_RALT | KMOD_LALT)); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 55; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOD_ALT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 55; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":56 */
  __pyx_1 = PyInt_FromLong(KMOD_LMETA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 56; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOD_LMETA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 56; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":57 */
  __pyx_1 = PyInt_FromLong(KMOD_RMETA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 57; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOD_RMETA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 57; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":58 */
  __pyx_1 = PyInt_FromLong((KMOD_LMETA | KMOD_RMETA)); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 58; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOD_META, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 58; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":59 */
  __pyx_1 = PyInt_FromLong(KMOD_NUM); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 59; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOD_NUM, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 59; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":60 */
  __pyx_1 = PyInt_FromLong(KMOD_CAPS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 60; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOD_CAPS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 60; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":61 */
  __pyx_1 = PyInt_FromLong(KMOD_MODE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 61; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOD_MODE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 61; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":62 */
  __pyx_1 = PyInt_FromLong(KMOD_RESERVED); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 62; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_MOD_RESERVED, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 62; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":64 */
  __pyx_1 = PyInt_FromLong(SDLK_UNKNOWN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 64; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_UNKNOWN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 64; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":65 */
  __pyx_1 = PyInt_FromLong(SDLK_FIRST); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 65; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_FIRST, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 65; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":66 */
  __pyx_1 = PyInt_FromLong(SDLK_BACKSPACE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 66; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_BACKSPACE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 66; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":67 */
  __pyx_1 = PyInt_FromLong(SDLK_TAB); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 67; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_TAB, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 67; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":68 */
  __pyx_1 = PyInt_FromLong(SDLK_CLEAR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 68; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_CLEAR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 68; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":69 */
  __pyx_1 = PyInt_FromLong(SDLK_RETURN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 69; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_RETURN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 69; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":70 */
  __pyx_1 = PyInt_FromLong(SDLK_PAUSE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 70; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_PAUSE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 70; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":71 */
  __pyx_1 = PyInt_FromLong(SDLK_ESCAPE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 71; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_ESCAPE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 71; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":72 */
  __pyx_1 = PyInt_FromLong(SDLK_SPACE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 72; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_SPACE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 72; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":73 */
  __pyx_1 = PyInt_FromLong(SDLK_EXCLAIM); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 73; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_EXCLAIM, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 73; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":74 */
  __pyx_1 = PyInt_FromLong(SDLK_QUOTEDBL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 74; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_QUOTEDBL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 74; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":75 */
  __pyx_1 = PyInt_FromLong(SDLK_HASH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 75; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_HASH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 75; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":76 */
  __pyx_1 = PyInt_FromLong(SDLK_DOLLAR); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 76; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_DOLLAR, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 76; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":77 */
  __pyx_1 = PyInt_FromLong(SDLK_AMPERSAND); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 77; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_AMPERSAND, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 77; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":78 */
  __pyx_1 = PyInt_FromLong(SDLK_QUOTE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 78; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_QUOTE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 78; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":79 */
  __pyx_1 = PyInt_FromLong(SDLK_LEFTPAREN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 79; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_LEFTPAREN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 79; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":80 */
  __pyx_1 = PyInt_FromLong(SDLK_RIGHTPAREN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 80; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_RIGHTPAREN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 80; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":81 */
  __pyx_1 = PyInt_FromLong(SDLK_ASTERISK); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 81; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_ASTERISK, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 81; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":82 */
  __pyx_1 = PyInt_FromLong(SDLK_PLUS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 82; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_PLUS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 82; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":83 */
  __pyx_1 = PyInt_FromLong(SDLK_COMMA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 83; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_COMMA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 83; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":84 */
  __pyx_1 = PyInt_FromLong(SDLK_MINUS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 84; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_MINUS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 84; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":85 */
  __pyx_1 = PyInt_FromLong(SDLK_PERIOD); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 85; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_PERIOD, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 85; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":86 */
  __pyx_1 = PyInt_FromLong(SDLK_SLASH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 86; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_SLASH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 86; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":87 */
  __pyx_1 = PyInt_FromLong(SDLK_0); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 87; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_0, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 87; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":88 */
  __pyx_1 = PyInt_FromLong(SDLK_1); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 88; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_1, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 88; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":89 */
  __pyx_1 = PyInt_FromLong(SDLK_2); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 89; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_2, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 89; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":90 */
  __pyx_1 = PyInt_FromLong(SDLK_3); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 90; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_3, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 90; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":91 */
  __pyx_1 = PyInt_FromLong(SDLK_4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 91; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 91; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":92 */
  __pyx_1 = PyInt_FromLong(SDLK_5); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 92; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_5, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 92; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":93 */
  __pyx_1 = PyInt_FromLong(SDLK_6); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 93; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_6, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 93; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":94 */
  __pyx_1 = PyInt_FromLong(SDLK_7); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 94; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_7, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 94; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":95 */
  __pyx_1 = PyInt_FromLong(SDLK_8); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 95; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_8, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 95; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":96 */
  __pyx_1 = PyInt_FromLong(SDLK_9); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 96; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_9, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 96; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":97 */
  __pyx_1 = PyInt_FromLong(SDLK_COLON); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 97; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_COLON, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 97; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":98 */
  __pyx_1 = PyInt_FromLong(SDLK_SEMICOLON); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 98; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_SEMICOLON, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 98; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":99 */
  __pyx_1 = PyInt_FromLong(SDLK_LESS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 99; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_LESS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 99; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":100 */
  __pyx_1 = PyInt_FromLong(SDLK_EQUALS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 100; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_EQUALS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 100; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":101 */
  __pyx_1 = PyInt_FromLong(SDLK_GREATER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 101; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_GREATER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 101; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":102 */
  __pyx_1 = PyInt_FromLong(SDLK_QUESTION); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 102; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_QUESTION, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 102; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":103 */
  __pyx_1 = PyInt_FromLong(SDLK_AT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 103; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_AT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 103; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":104 */
  __pyx_1 = PyInt_FromLong(SDLK_LEFTBRACKET); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 104; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_LEFTBRACKET, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 104; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":105 */
  __pyx_1 = PyInt_FromLong(SDLK_BACKSLASH); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 105; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_BACKSLASH, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 105; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":106 */
  __pyx_1 = PyInt_FromLong(SDLK_RIGHTBRACKET); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 106; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_RIGHTBRACKET, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 106; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":107 */
  __pyx_1 = PyInt_FromLong(SDLK_CARET); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 107; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_CARET, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 107; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":108 */
  __pyx_1 = PyInt_FromLong(SDLK_UNDERSCORE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 108; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_UNDERSCORE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 108; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":109 */
  __pyx_1 = PyInt_FromLong(SDLK_BACKQUOTE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 109; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_BACKQUOTE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 109; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":110 */
  __pyx_1 = PyInt_FromLong(SDLK_a); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 110; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_a, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 110; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":111 */
  __pyx_1 = PyInt_FromLong(SDLK_b); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 111; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_b, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 111; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":112 */
  __pyx_1 = PyInt_FromLong(SDLK_c); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 112; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_c, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 112; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":113 */
  __pyx_1 = PyInt_FromLong(SDLK_d); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 113; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_d, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 113; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":114 */
  __pyx_1 = PyInt_FromLong(SDLK_e); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 114; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_e, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 114; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":115 */
  __pyx_1 = PyInt_FromLong(SDLK_f); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 115; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_f, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 115; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":116 */
  __pyx_1 = PyInt_FromLong(SDLK_g); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 116; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_g, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 116; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":117 */
  __pyx_1 = PyInt_FromLong(SDLK_h); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 117; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_h, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 117; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":118 */
  __pyx_1 = PyInt_FromLong(SDLK_i); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 118; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_i, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 118; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":119 */
  __pyx_1 = PyInt_FromLong(SDLK_j); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 119; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_j, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 119; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":120 */
  __pyx_1 = PyInt_FromLong(SDLK_k); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 120; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_k, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 120; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":121 */
  __pyx_1 = PyInt_FromLong(SDLK_l); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 121; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_l, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 121; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":122 */
  __pyx_1 = PyInt_FromLong(SDLK_m); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 122; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_m, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 122; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":123 */
  __pyx_1 = PyInt_FromLong(SDLK_n); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 123; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_n, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 123; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":124 */
  __pyx_1 = PyInt_FromLong(SDLK_o); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 124; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_o, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 124; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":125 */
  __pyx_1 = PyInt_FromLong(SDLK_p); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 125; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_p, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 125; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":126 */
  __pyx_1 = PyInt_FromLong(SDLK_q); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 126; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_q, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 126; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":127 */
  __pyx_1 = PyInt_FromLong(SDLK_r); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 127; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_r, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 127; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":128 */
  __pyx_1 = PyInt_FromLong(SDLK_s); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 128; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_s, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 128; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":129 */
  __pyx_1 = PyInt_FromLong(SDLK_t); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 129; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_t, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 129; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":130 */
  __pyx_1 = PyInt_FromLong(SDLK_u); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 130; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_u, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 130; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":131 */
  __pyx_1 = PyInt_FromLong(SDLK_v); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 131; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_v, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 131; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":132 */
  __pyx_1 = PyInt_FromLong(SDLK_w); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 132; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_w, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 132; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":133 */
  __pyx_1 = PyInt_FromLong(SDLK_x); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 133; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_x, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 133; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":134 */
  __pyx_1 = PyInt_FromLong(SDLK_y); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 134; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_y, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 134; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":135 */
  __pyx_1 = PyInt_FromLong(SDLK_z); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 135; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_z, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 135; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":136 */
  __pyx_1 = PyInt_FromLong(SDLK_DELETE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 136; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_DELETE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 136; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":137 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_0); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 137; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_0, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 137; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":138 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_1); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 138; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_1, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 138; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":139 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_2); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 139; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_2, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 139; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":140 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_3); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 140; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_3, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 140; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":141 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 141; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 141; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":142 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_5); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 142; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_5, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 142; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":143 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_6); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 143; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_6, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 143; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":144 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_7); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 144; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_7, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 144; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":145 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_8); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 145; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_8, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 145; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":146 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_9); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 146; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_9, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 146; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":147 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_10); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 147; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_10, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 147; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":148 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_11); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 148; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_11, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 148; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":149 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_12); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 149; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_12, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 149; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":150 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_13); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 150; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_13, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 150; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":151 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_14); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 151; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_14, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 151; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":152 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_15); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 152; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_15, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 152; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":153 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_16); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 153; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_16, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 153; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":154 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_17); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 154; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_17, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 154; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":155 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_18); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 155; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_18, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 155; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":156 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_19); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 156; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_19, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 156; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":157 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_20); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 157; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_20, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 157; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":158 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_21); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 158; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_21, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 158; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":159 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_22); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 159; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_22, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 159; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":160 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_23); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 160; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_23, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 160; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":161 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_24); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 161; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_24, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 161; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":162 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_25); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 162; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_25, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 162; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":163 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_26); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 163; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_26, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 163; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":164 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_27); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 164; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_27, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 164; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":165 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_28); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 165; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_28, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 165; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":166 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_29); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 166; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_29, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 166; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":167 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_30); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 167; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_30, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 167; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":168 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_31); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 168; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_31, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 168; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":169 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_32); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 169; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_32, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 169; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":170 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_33); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 170; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_33, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 170; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":171 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_34); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 171; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_34, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 171; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":172 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_35); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 172; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_35, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 172; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":173 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_36); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 173; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_36, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 173; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":174 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_37); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 174; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_37, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 174; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":175 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_38); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 175; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_38, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 175; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":176 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_39); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 176; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_39, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 176; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":177 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_40); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 177; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_40, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 177; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":178 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_41); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 178; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_41, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 178; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":179 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_42); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 179; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_42, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 179; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":180 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_43); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 180; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_43, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 180; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":181 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_44); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 181; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_44, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 181; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":182 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_45); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 182; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_45, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 182; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":183 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_46); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 183; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_46, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 183; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":184 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_47); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 184; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_47, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 184; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":185 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_48); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 185; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_48, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 185; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":186 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_49); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 186; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_49, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 186; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":187 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_50); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 187; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_50, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 187; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":188 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_51); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 188; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_51, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 188; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":189 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_52); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 189; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_52, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 189; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":190 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_53); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 190; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_53, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 190; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":191 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_54); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 191; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_54, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 191; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":192 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_55); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 192; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_55, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 192; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":193 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_56); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 193; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_56, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 193; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":194 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_57); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 194; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_57, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 194; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":195 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_58); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 195; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_58, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 195; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":196 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_59); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 196; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_59, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 196; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":197 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_60); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 197; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_60, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 197; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":198 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_61); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 198; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_61, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 198; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":199 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_62); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 199; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_62, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 199; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":200 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_63); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 200; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_63, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 200; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":201 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_64); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 201; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_64, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 201; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":202 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_65); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 202; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_65, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 202; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":203 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_66); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 203; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_66, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 203; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":204 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_67); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 204; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_67, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 204; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":205 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_68); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 205; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_68, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 205; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":206 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_69); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 206; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_69, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 206; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":207 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_70); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 207; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_70, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 207; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":208 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_71); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 208; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_71, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 208; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":209 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_72); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 209; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_72, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 209; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":210 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_73); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 210; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_73, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 210; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":211 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_74); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 211; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_74, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 211; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":212 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_75); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 212; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_75, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 212; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":213 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_76); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 213; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_76, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 213; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":214 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_77); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 214; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_77, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 214; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":215 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_78); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 215; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_78, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 215; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":216 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_79); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 216; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_79, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 216; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":217 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_80); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 217; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_80, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 217; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":218 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_81); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 218; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_81, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 218; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":219 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_82); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 219; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_82, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 219; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":220 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_83); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 220; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_83, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 220; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":221 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_84); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 221; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_84, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 221; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":222 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_85); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 222; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_85, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 222; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":223 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_86); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 223; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_86, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 223; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":224 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_87); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 224; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_87, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 224; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":225 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_88); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 225; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_88, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 225; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":226 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_89); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 226; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_89, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 226; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":227 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_90); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 227; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_90, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 227; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":228 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_91); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 228; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_91, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 228; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":229 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_92); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 229; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_92, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 229; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":230 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_93); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 230; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_93, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 230; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":231 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_94); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 231; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_94, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 231; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":232 */
  __pyx_1 = PyInt_FromLong(SDLK_WORLD_95); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 232; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_WORLD_95, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 232; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":233 */
  __pyx_1 = PyInt_FromLong(SDLK_KP0); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 233; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_KP0, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 233; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":234 */
  __pyx_1 = PyInt_FromLong(SDLK_KP1); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 234; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_KP1, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 234; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":235 */
  __pyx_1 = PyInt_FromLong(SDLK_KP2); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 235; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_KP2, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 235; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":236 */
  __pyx_1 = PyInt_FromLong(SDLK_KP3); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 236; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_KP3, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 236; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":237 */
  __pyx_1 = PyInt_FromLong(SDLK_KP4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 237; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_KP4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 237; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":238 */
  __pyx_1 = PyInt_FromLong(SDLK_KP5); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 238; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_KP5, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 238; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":239 */
  __pyx_1 = PyInt_FromLong(SDLK_KP6); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 239; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_KP6, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 239; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":240 */
  __pyx_1 = PyInt_FromLong(SDLK_KP7); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 240; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_KP7, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 240; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":241 */
  __pyx_1 = PyInt_FromLong(SDLK_KP8); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 241; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_KP8, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 241; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":242 */
  __pyx_1 = PyInt_FromLong(SDLK_KP9); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 242; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_KP9, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 242; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":243 */
  __pyx_1 = PyInt_FromLong(SDLK_KP_PERIOD); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 243; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_KP_PERIOD, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 243; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":244 */
  __pyx_1 = PyInt_FromLong(SDLK_KP_DIVIDE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 244; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_KP_DIVIDE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 244; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":245 */
  __pyx_1 = PyInt_FromLong(SDLK_KP_MULTIPLY); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 245; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_KP_MULTIPLY, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 245; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":246 */
  __pyx_1 = PyInt_FromLong(SDLK_KP_MINUS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 246; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_KP_MINUS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 246; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":247 */
  __pyx_1 = PyInt_FromLong(SDLK_KP_PLUS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 247; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_KP_PLUS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 247; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":248 */
  __pyx_1 = PyInt_FromLong(SDLK_KP_ENTER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 248; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_KP_ENTER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 248; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":249 */
  __pyx_1 = PyInt_FromLong(SDLK_KP_EQUALS); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 249; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_KP_EQUALS, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 249; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":250 */
  __pyx_1 = PyInt_FromLong(SDLK_UP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 250; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_UP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 250; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":251 */
  __pyx_1 = PyInt_FromLong(SDLK_DOWN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 251; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_DOWN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 251; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":252 */
  __pyx_1 = PyInt_FromLong(SDLK_RIGHT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 252; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_RIGHT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 252; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":253 */
  __pyx_1 = PyInt_FromLong(SDLK_LEFT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 253; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_LEFT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 253; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":254 */
  __pyx_1 = PyInt_FromLong(SDLK_INSERT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 254; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_INSERT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 254; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":255 */
  __pyx_1 = PyInt_FromLong(SDLK_HOME); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 255; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_HOME, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 255; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":256 */
  __pyx_1 = PyInt_FromLong(SDLK_END); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 256; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_END, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 256; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":257 */
  __pyx_1 = PyInt_FromLong(SDLK_PAGEUP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 257; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_PAGEUP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 257; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":258 */
  __pyx_1 = PyInt_FromLong(SDLK_PAGEDOWN); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 258; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_PAGEDOWN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 258; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":259 */
  __pyx_1 = PyInt_FromLong(SDLK_F1); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 259; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_F1, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 259; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":260 */
  __pyx_1 = PyInt_FromLong(SDLK_F2); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 260; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_F2, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 260; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":261 */
  __pyx_1 = PyInt_FromLong(SDLK_F3); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 261; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_F3, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 261; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":262 */
  __pyx_1 = PyInt_FromLong(SDLK_F4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 262; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_F4, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 262; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":263 */
  __pyx_1 = PyInt_FromLong(SDLK_F5); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 263; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_F5, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 263; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":264 */
  __pyx_1 = PyInt_FromLong(SDLK_F6); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 264; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_F6, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 264; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":265 */
  __pyx_1 = PyInt_FromLong(SDLK_F7); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 265; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_F7, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 265; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":266 */
  __pyx_1 = PyInt_FromLong(SDLK_F8); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 266; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_F8, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 266; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":267 */
  __pyx_1 = PyInt_FromLong(SDLK_F9); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 267; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_F9, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 267; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":268 */
  __pyx_1 = PyInt_FromLong(SDLK_F10); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 268; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_F10, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 268; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":269 */
  __pyx_1 = PyInt_FromLong(SDLK_F11); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 269; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_F11, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 269; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":270 */
  __pyx_1 = PyInt_FromLong(SDLK_F12); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 270; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_F12, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 270; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":271 */
  __pyx_1 = PyInt_FromLong(SDLK_F13); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 271; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_F13, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 271; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":272 */
  __pyx_1 = PyInt_FromLong(SDLK_F14); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 272; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_F14, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 272; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":273 */
  __pyx_1 = PyInt_FromLong(SDLK_F15); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 273; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_F15, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 273; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":274 */
  __pyx_1 = PyInt_FromLong(SDLK_NUMLOCK); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 274; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_NUMLOCK, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 274; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":275 */
  __pyx_1 = PyInt_FromLong(SDLK_CAPSLOCK); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 275; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_CAPSLOCK, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 275; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":276 */
  __pyx_1 = PyInt_FromLong(SDLK_SCROLLOCK); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 276; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_SCROLLOCK, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 276; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":277 */
  __pyx_1 = PyInt_FromLong(SDLK_RSHIFT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 277; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_RSHIFT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 277; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":278 */
  __pyx_1 = PyInt_FromLong(SDLK_LSHIFT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 278; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_LSHIFT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 278; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":279 */
  __pyx_1 = PyInt_FromLong(SDLK_RCTRL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 279; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_RCTRL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 279; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":280 */
  __pyx_1 = PyInt_FromLong(SDLK_LCTRL); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 280; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_LCTRL, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 280; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":281 */
  __pyx_1 = PyInt_FromLong(SDLK_RALT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 281; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_RALT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 281; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":282 */
  __pyx_1 = PyInt_FromLong(SDLK_LALT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 282; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_LALT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 282; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":283 */
  __pyx_1 = PyInt_FromLong(SDLK_RMETA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 283; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_RMETA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 283; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":284 */
  __pyx_1 = PyInt_FromLong(SDLK_LMETA); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 284; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_LMETA, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 284; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":285 */
  __pyx_1 = PyInt_FromLong(SDLK_LSUPER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 285; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_LSUPER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 285; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":286 */
  __pyx_1 = PyInt_FromLong(SDLK_RSUPER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 286; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_RSUPER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 286; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":287 */
  __pyx_1 = PyInt_FromLong(SDLK_MODE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 287; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_MODE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 287; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":288 */
  __pyx_1 = PyInt_FromLong(SDLK_COMPOSE); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 288; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_COMPOSE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 288; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":289 */
  __pyx_1 = PyInt_FromLong(SDLK_HELP); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 289; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_HELP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 289; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":290 */
  __pyx_1 = PyInt_FromLong(SDLK_PRINT); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 290; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_PRINT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 290; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":291 */
  __pyx_1 = PyInt_FromLong(SDLK_SYSREQ); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 291; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_SYSREQ, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 291; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":292 */
  __pyx_1 = PyInt_FromLong(SDLK_BREAK); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 292; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_BREAK, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 292; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":293 */
  __pyx_1 = PyInt_FromLong(SDLK_MENU); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 293; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_MENU, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 293; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":294 */
  __pyx_1 = PyInt_FromLong(SDLK_POWER); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 294; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_POWER, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 294; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":295 */
  __pyx_1 = PyInt_FromLong(SDLK_EURO); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 295; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_EURO, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 295; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":296 */
  __pyx_1 = PyInt_FromLong(SDLK_UNDO); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 296; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_K_UNDO, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 296; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":300 */
  __pyx_1 = PyInt_FromLong(1); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 300; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_BUTTON_LEFT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 300; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":301 */
  __pyx_1 = PyInt_FromLong(2); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 301; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_BUTTON_MIDDLE, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 301; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":302 */
  __pyx_1 = PyInt_FromLong(3); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 302; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_BUTTON_RIGHT, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 302; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":303 */
  __pyx_1 = PyInt_FromLong(4); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 303; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_BUTTON_WHEELUP, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 303; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;

  /* "/home/jiba/src/soya/sdlconst.pyx":304 */
  __pyx_1 = PyInt_FromLong(5); if (!__pyx_1) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 304; goto __pyx_L1;}
  if (PyObject_SetAttr(__pyx_m, __pyx_n_BUTTON_WHEELDOWN, __pyx_1) < 0) {__pyx_filename = __pyx_f[0]; __pyx_lineno = 304; goto __pyx_L1;}
  Py_DECREF(__pyx_1); __pyx_1 = 0;
  return;
  __pyx_L1:;
  Py_XDECREF(__pyx_1);
  __Pyx_AddTraceback("sdlconst");
}

static char *__pyx_filenames[] = {
  "sdlconst.pyx",
  "sdl.pxd",
};

/* Runtime support code */

static void __pyx_init_filenames(void) {
  __pyx_f = __pyx_filenames;
}

static int __Pyx_InternStrings(__Pyx_InternTabEntry *t) {
    while (t->p) {
        *t->p = PyString_InternFromString(t->s);
        if (!*t->p)
            return -1;
        ++t;
    }
    return 0;
}

#include "compile.h"
#include "frameobject.h"
#include "traceback.h"

static void __Pyx_AddTraceback(char *funcname) {
    PyObject *py_srcfile = 0;
    PyObject *py_funcname = 0;
    PyObject *py_globals = 0;
    PyObject *empty_tuple = 0;
    PyObject *empty_string = 0;
    PyCodeObject *py_code = 0;
    PyFrameObject *py_frame = 0;
    
    py_srcfile = PyString_FromString(__pyx_filename);
    if (!py_srcfile) goto bad;
    py_funcname = PyString_FromString(funcname);
    if (!py_funcname) goto bad;
    py_globals = PyModule_GetDict(__pyx_m);
    if (!py_globals) goto bad;
    empty_tuple = PyTuple_New(0);
    if (!empty_tuple) goto bad;
    empty_string = PyString_FromString("");
    if (!empty_string) goto bad;
    py_code = PyCode_New(
        0,            /*int argcount,*/
        0,            /*int nlocals,*/
        0,            /*int stacksize,*/
        0,            /*int flags,*/
        empty_string, /*PyObject *code,*/
        empty_tuple,  /*PyObject *consts,*/
        empty_tuple,  /*PyObject *names,*/
        empty_tuple,  /*PyObject *varnames,*/
        empty_tuple,  /*PyObject *freevars,*/
        empty_tuple,  /*PyObject *cellvars,*/
        py_srcfile,   /*PyObject *filename,*/
        py_funcname,  /*PyObject *name,*/
        __pyx_lineno,   /*int firstlineno,*/
        empty_string  /*PyObject *lnotab*/
    );
    if (!py_code) goto bad;
    py_frame = PyFrame_New(
        PyThreadState_Get(), /*PyThreadState *tstate,*/
        py_code,             /*PyCodeObject *code,*/
        py_globals,          /*PyObject *globals,*/
        0                    /*PyObject *locals*/
    );
    if (!py_frame) goto bad;
    py_frame->f_lineno = __pyx_lineno;
    PyTraceBack_Here(py_frame);
bad:
    Py_XDECREF(py_srcfile);
    Py_XDECREF(py_funcname);
    Py_XDECREF(empty_tuple);
    Py_XDECREF(empty_string);
    Py_XDECREF(py_code);
    Py_XDECREF(py_frame);
}
